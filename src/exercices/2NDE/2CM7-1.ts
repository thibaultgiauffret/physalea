/**
 * 2CM7-1.ts
 * @author Thibault Giauffret
 * @date 2024-04-03
 */

// On importe tous les outils nécessaires
import { combinaisonListes } from '../../lib/outils/arrayOutils'
import { ecritureScientifique } from '../../lib/outils_phys/ecritures'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { createList } from '../../lib/format/lists'

// On définit le titre de l'exercice et les références
export const titre = 'Déterminer la masse d’une entité'
export const uuid = 'b8c14'
export const ref = '2CM7-1'
export const refs = {
  'fr-fr': ['2CM7-1']
}

// On définit les propriétés de l'interactivité
export const interactifReady = true
export const interactifType = 'mathLive'

// Fonction principale
export default class MasseEntite extends Exercice {
  constructor () {
    super()
    this.titre = titre
    this.consigne = 'Pour les questions suivantes, on rappelle les masses de quelques éléments :'

    const list = createList({
      items: [`Carbone : $m_\\text{C} = ${ecritureScientifique(1.994e-23)}\\ \\text{g}$`,
      `Hydrogène : $m_\\text{H} = ${ecritureScientifique(1.674e-24)}\\ \\text{g}$`,
      `Oxygène : $m_\\text{O} = ${ecritureScientifique(2.657e-23)}\\ \\text{g}$`],
      style: 'fleches',
      classOptions: ''
    }) as unknown as HTMLElement
    this.consigne += list
    this.nbQuestions = 2
  }

  nouvelleVersion () {
    // On réinitialise les listes de questions et de corrections
    this.listeQuestions = []
    this.listeCorrections = []

    // On précise le nombre de type de questions disponibles (lien avec le switch plus bas)
    const typesDeQuestionsDisponibles = [1, 2]
    const listeTypeDeQuestions = combinaisonListes(
      typesDeQuestionsDisponibles,
      this.nbQuestions
    )

    // On crée les questions pour à partir des types de questions dans listeTypeDeQuestions
    for (let i = 0, texte, texteCorr, cpt = 0, n, m, formuleBrute; i < this.nbQuestions && cpt < 50;) {
      // En fonction du type de question, on crée la question adéquate
      switch (listeTypeDeQuestions[i]) {
        case 1:
          // CnH2n+2O : alcool
          n = randint(2, 6)
          m = (1.994e-23 * n + 1.674e-24 * (2 * n + 2) + 2.657e-23)
          formuleBrute = `\\text{C}_{${n}}\\text{H}_{${2 * n + 2}}\\text{O}`

          texte = `On considère une entité chimique de formule brute $${formuleBrute}$. Calculer sa masse $m$ en grammes.`

          texteCorr = `D'après la formule brute, on a ${n} atomes de carbone, ${2 * n + 2} atomes d'hydrogène et 1 atome d'oxygène. On a donc : `
          texteCorr += `\\[m = ${n} \\times m_{\\text{C}} + ${2 * n + 2} \\times m_{\\text{H}} + m_{\\text{O}}\\]<br>`
          texteCorr += 'A.N. :<br>'
          texteCorr += `\\[m = ${n} \\times ${ecritureScientifique(1.994e-23)} + ${2 * n + 2} \\times ${ecritureScientifique(1.674e-24)} + ${ecritureScientifique(2.657e-23)}\\]<br>`
          texteCorr += `\\[m = ${ecritureScientifique(m, 3)}\\ \\text{g}\\]`
          break
        case 2:
          // CnH2n+2 : Alcane
          n = randint(2, 5)
          m = (1.994e-23 * n + 1.674e-24 * (2 * n + 2))
          formuleBrute = `\\text{C}_{${n}}\\text{H}_{${2 * n + 2}}`

          texte = `On considère une entité chimique de formule brute $${formuleBrute}$. Calculer sa masse $m$ en grammes.`
          texteCorr = `D'après la formule brute, on a ${n} atomes de carbone et ${2 * n + 2} atomes d'hydrogène. On a donc : `
          texteCorr += `\\[m = ${n} \\times m_{\\text{C}} + ${2 * n + 2} \\times m_{\\text{H}}\\]<br>`
          texteCorr += 'A.N. :<br>'
          texteCorr += `\\[m = ${n} \\times ${ecritureScientifique(1.994e-23)} + ${2 * n + 2} \\times ${ecritureScientifique(1.674e-24)}\\]<br>`
          texteCorr += `\\[m = ${ecritureScientifique(m, 3)}\\ \\text{g}\\]`
          break
      }

      // On ajoute les champs de réponse interactifs
      texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteApres: 'g' })
      handleAnswers(this, i, { reponse: { value: m, compare: approximatelyCompare, options: { tolerance: 1e-25 } } })

      if (this.questionJamaisPosee(i, n!, m!, formuleBrute!)) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(texte!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(texteCorr!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }
    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
