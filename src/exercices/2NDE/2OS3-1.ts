/**
 * 2OS3-1
 * @author Thibault Giauffret
 * @date 2024-04-07
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { createComplexCircuit, createSimpleCircuit } from '../../lib/outils_phys/circuit_elec'
import { context } from '../../modules/context.js'

export const titre = 'Exploiter la loi des mailles et la loi des nœuds dans un circuit électrique'

export const uuid = 'b8c16'
export const ref = '2OS3-1'
export const refs = {
  'fr-fr': ['2OS3-1']
}
export const interactifReady = true
export const interactifType = 'mathLive'

export default class loiMaillesNoeuds extends Exercice {
  constructor () {
    super()
    this.titre = titre
    this.consigne = ''
    this.nbQuestions = 1
    this.sup = 1
    this.sup2 = 2
    this.besoinFormulaireNumerique = ['Choix des questions', 2, '1 : Loi des mailles (niveau 1)\n2 : Loi des mailles (niveau 2)\n3 : Loi des noeuds']
    this.besoinFormulaire2Numerique = ['\n\nNombre de dipoles récepteurs', 2, '1 : Niveau 1\n2 : Niveau 2\n3 : Niveau 3\n4 : Aléatoire']
  }

  nouvelleVersion () {
    this.listeQuestions = []
    this.listeCorrections = []

    this.consigne = ''

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['typeE1']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['typeE2']
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['typeE3']
    }

    let sousChoix
    if (this.sup2 === 1) {
      sousChoix = combinaisonListes(['dipole1'], this.nbQuestions) // pour choisir aléatoirement des questions dans chaque catégorie
    } else if (this.sup2 === 2) {
      sousChoix = combinaisonListes(['dipole2'], this.nbQuestions)
    } else if (this.sup2 === 3) {
      sousChoix = combinaisonListes(['dipole3'], this.nbQuestions)
    } else {
      sousChoix = combinaisonListes(['dipole1', 'dipole2', 'dipole3'], this.nbQuestions)
    }

    const listeTypeDeQuestions = combinaisonListes(
      typesDeQuestionsDisponibles!,
      this.nbQuestions
    )

    for (let i = 0, texte, texteCorr, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      console.log(i)
      let elements = []
      let intensities = []
      let nDipoles = 0
      let nDipoles2 = 0
      const tensions = []
      const tensions2 = []
      let tensionCentrale = 0
      let tensionGenerateur = 0
      let elementPosition = ''
      let resistorIndex = 0
      let circuit = ''
      let availablePositions = []
      let availablePositions2 = []
      let availableReceptors = []
      let receptor = ''
      let intensityIndex = 0
      let valeurCachee = 0

      switch (listeTypeDeQuestions[i]) {
        // Loi des mailles (niveau 1)
        case 'typeE1':
          // On crée un circuit simple avec une maille. On choisit le nombre de dipoles et les tensions
          switch (sousChoix[i]) {
            case 'dipole1':
              nDipoles = 1
              break
            case 'dipole2':
              nDipoles = 2
              break
            case 'dipole3':
              nDipoles = 3
              break
            default:
              nDipoles = randint(1, 3)
              break
          }

          availablePositions = ['left', 'right', 'top', 'bottom']
          availableReceptors = ['resistor', 'lamp']
          for (let j = 0; j < nDipoles; j++) {
            tensions.push(randint(1, 10))
          }

          // On calcule la tension totale
          tensionGenerateur = tensions.reduce((a, b) => a + b, 0)
          // On choisit une position pour le générateur (et on la retire des positions disponibles)
          elementPosition = availablePositions.splice(randint(0, availablePositions.length - 1), 1)[0]
          // On ajoute le générateur
          elements.push({ id: 0, type: 'generator', voltage: tensionGenerateur, pos: elementPosition })

          // On fait de même pour les résistances
          for (let j = 0; j < nDipoles; j++) {
            elementPosition = availablePositions.splice(randint(0, availablePositions.length - 1), 1)[0]
            // On choisit un récepteur au hasard
            const receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
            elements.push({ id: 0, type: receptor, voltage: tensions[j], pos: elementPosition })
          }

          // Pour les positions restantes, on ajoute des fils
          for (let j = 0; j < availablePositions.length; j++) {
            elements.push({ id: 0, type: 'wire', pos: availablePositions[j], voltage: 0 })
          }

          // On choisit un dipôle au hasard pour lequel on va demander la tension
          resistorIndex = randint(0, nDipoles - 1)
          valeurCachee = elements[resistorIndex].voltage
          elements[resistorIndex].voltage = 0

          texte = 'En appliquant la loi des mailles, déterminer la valeur de tension manquante dans le circuit suivant :<br>'
          // On crée le circuit
          circuit = createSimpleCircuit(elements)

          if (context.isHtml) {
            const span = document.createElement('div')
            span.innerHTML = circuit
            if (context.isDiaporama) {
              span.setAttribute('style', 'display: block; margin: auto; width:100%; max-width:600px; margin-bottom: 10px; margin-top: 10px;font-size: max(32px);')
            } else {
              span.setAttribute('style', 'display: block; margin: auto; width:100%; max-width:400px; margin-bottom: 10px; margin-top: 10px;font-size:25px;')
            }
            texte += `${span.outerHTML}`
          } else {
            texte += `${circuit}`
          }

          texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: 'U<sub>' + (resistorIndex + 1).toString() + '</sub> = ', texteApres: ' V' })
          handleAnswers(this, i, { reponse: { value: valeurCachee, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

          texteCorr = 'On commence par choisir un sens de parcours pour la maille. Ici, on choisit (par exemple) le sens horaire.<br>'
          texteCorr += 'Toutes les tensions indiquées par une flèche allant dans le sens du parcours sont comptées positivement, les autres sont comptées négativement.<br><br>'
          texteCorr += 'En appliquant la loi des mailles, la somme de ces valeurs de tension est nulle. On a alors :<br>'
          texteCorr += '\\['
          for (let j = 0; j < nDipoles + 1; j++) {
            if (elements[j].type === 'generator') {
              if (j === 0) {
                texteCorr += `U_{${j + 1}} `
              } else {
                texteCorr += `+ U_{${j + 1}} `
              }
            } else {
              texteCorr += `- U_{${j + 1}} `
            }
          }
          texteCorr += ' = 0\\]'
          texteCorr += 'A.N. :<br>'

          texteCorr += '\\['
          for (let j = 0; j < nDipoles + 1; j++) {
            if (elements[j].type === 'generator') {
              if (j === 0) {
                texteCorr += ''
              } else {
                texteCorr += '+ '
              }
            } else {
              texteCorr += '- '
            }

            if (j === resistorIndex) {
              texteCorr += `U_{${j + 1}}`
            } else {
              texteCorr += `${elements[j].voltage}`
            }
          }
          texteCorr += ' = 0\\]'
          texteCorr += 'd\'où :<br>'
          texteCorr += `\\[U_{${resistorIndex + 1}} = ${valeurCachee} \\text{ V}\\]`

          break

        // Loi des mailles (niveau 2)
        case 'typeE2':
          // On crée un circuit plus complexe avec deux noeuds. On choisit le nombre de dipoles et les tensions
          switch (sousChoix[i]) {
            case 'dipole1':
              nDipoles = 0
              nDipoles2 = 1
              break
            case 'dipole2':
              nDipoles = 1
              nDipoles2 = 1
              break
            case 'dipole3':
              nDipoles = 2
              nDipoles2 = 3
              break
            default:
              nDipoles = randint(1, 2)
              nDipoles2 = randint(1, 3)
              break
          }

          elements = []
          availablePositions = ['left', 'top', 'bottom']
          availablePositions2 = ['top2', 'bottom2', 'right2']
          availableReceptors = ['resistor', 'lamp']

          // On tire les tensions des dipoles de la branche de droite
          for (let j = 0; j < nDipoles2; j++) {
            tensions2.push(randint(1, 5))
          }
          tensionCentrale = tensions2.reduce((a, b) => a + b, 0)

          // On tire les tensions des dipoles de la branche de gauche
          for (let j = 0; j < nDipoles; j++) {
            tensions.push(randint(1, 10))
          }

          // On calcule la tension totale
          tensionGenerateur = tensions.reduce((a, b) => a + b, 0) + tensionCentrale

          // On choisit une position pour le générateur (et on la retire des positions disponibles)
          elementPosition = availablePositions.splice(randint(0, availablePositions.length - 1), 1)[0]
          elements.push({ id: 0, type: 'generator', voltage: tensionGenerateur, pos: elementPosition })

          // On ajoute le dipôle central
          elementPosition = 'right'
          receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
          elements.push({ id: 0, type: receptor, voltage: tensionCentrale, pos: elementPosition })

          // On fait de même pour les récepteurs de la branche de gauche...
          for (let j = 0; j < nDipoles; j++) {
            elementPosition = availablePositions.splice(randint(0, availablePositions.length - 1), 1)[0]
            // On choisit un récepteur au hasard
            const receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
            elements.push({ id: 0, type: receptor, voltage: tensions[j], pos: elementPosition })
          }

          // ... et les récepteurs de la branche de droite
          for (let j = 0; j < nDipoles2; j++) {
            elementPosition = availablePositions2.splice(randint(0, availablePositions2.length - 1), 1)[0]
            // On choisit un récepteur au hasard
            const receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
            elements.push({ id: 0, type: receptor, voltage: tensions2[j], pos: elementPosition })
          }

          // Pour les positions restantes, on ajoute des fils
          for (let j = 0; j < availablePositions.length; j++) {
            elements.push({ id: 0, type: 'wire', pos: availablePositions[j], voltage: 0 })
          }
          for (let j = 0; j < availablePositions2.length; j++) {
            elements.push({ id: 0, type: 'wire', pos: availablePositions2[j], voltage: 0 })
          }

          // On choisit un dipôle au hasard pour lequel on va demander la tension
          resistorIndex = randint(0, nDipoles + nDipoles2 - 1)
          valeurCachee = elements[resistorIndex].voltage
          elements[resistorIndex].voltage = 0

          texte = 'En appliquant la loi des mailles, déterminer la valeur de tension manquante dans le circuit suivant :<br>'
          // On crée le circuit
          circuit = createComplexCircuit(elements, [])

          if (context.isHtml) {
            const span = document.createElement('div')
            span.innerHTML = circuit
            if (context.isDiaporama) {
              span.setAttribute('style', 'display: block; margin: auto; width:100%; max-width:750px; margin-bottom: 10px; margin-top: 10px;font-size: max(30px);')
            } else {
              span.setAttribute('style', 'display: block; margin: auto; width:100%; max-width:500px; margin-bottom: 10px; margin-top: 10px;font-size:22px;')
            }
            texte += `${span.outerHTML}`
          } else {
            texte += `${circuit}`
          }

          texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: 'U<sub>' + (resistorIndex + 1).toString() + '</sub> = ', texteApres: ' V' })
          handleAnswers(this, i, { reponse: { value: valeurCachee, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

          texteCorr = `\\[U_{${resistorIndex + 1}} = ${valeurCachee} \\text{ V}\\]`

          break

        // Loi des noeuds
        case 'typeE3':
          // On crée un circuit plus complexe avec deux noeuds. On choisit le nombre de dipoles et les intensités
          switch (sousChoix[i]) {
            case 'dipole1':
              nDipoles = 0
              nDipoles2 = 1
              break
            case 'dipole2':
              nDipoles = 1
              nDipoles2 = 1
              break
            case 'dipole3':
              nDipoles = 2
              nDipoles2 = 3
              break
            default:
              nDipoles = randint(1, 2)
              nDipoles2 = randint(1, 3)
              break
          }

          elements = []
          availablePositions = ['left', 'top', 'bottom']
          availablePositions2 = ['top2', 'bottom2', 'right2']
          availableReceptors = ['resistor', 'lamp']

          // On choisit une position pour le générateur (et on la retire des positions disponibles)
          elementPosition = availablePositions.splice(randint(0, availablePositions.length - 1), 1)[0]
          elements.push({ id: 0, type: 'generator', voltage: 0, pos: elementPosition })

          // On ajoute le dipôle central
          elementPosition = 'right'
          receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
          elements.push({ id: 0, type: receptor, voltage: 0, pos: elementPosition })

          // On fait de même pour les récepteurs de la branche de gauche...
          for (let j = 0; j < nDipoles; j++) {
            elementPosition = availablePositions.splice(randint(0, availablePositions.length - 1), 1)[0]
            // On choisit un récepteur au hasard
            const receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
            elements.push({ id: 0, type: receptor, voltage: 0, pos: elementPosition })
          }

          // ... et la branche de droite
          for (let j = 0; j < nDipoles2; j++) {
            elementPosition = availablePositions2.splice(randint(0, availablePositions2.length - 1), 1)[0]
            // On choisit un récepteur au hasard
            const receptor = availableReceptors[randint(0, availableReceptors.length - 1)]
            elements.push({ id: 0, type: receptor, voltage: 0, pos: elementPosition })
          }

          // Pour les positions restantes, on ajoute des fils
          for (let j = 0; j < availablePositions.length; j++) {
            elements.push({ id: 0, type: 'wire', pos: availablePositions[j], voltage: 0 })
          }
          for (let j = 0; j < availablePositions2.length; j++) {
            elements.push({ id: 0, type: 'wire', pos: availablePositions2[j], voltage: 0 })
          }

          // On programme les intensités
          intensities = []
          intensities.push({ id: 0, value: randint(1, 300), pos: 'right' })
          intensities.push({ id: 0, value: randint(1, 300), pos: 'bottom' })
          intensities.push({ id: 0, value: intensities[0].value + intensities[1].value, pos: 'left' })
          intensities = intensities.reverse()

          // On choisit une des intensités et on remplace sa valeur par null
          intensityIndex = randint(0, intensities.length - 1)
          valeurCachee = intensities[intensityIndex].value
          intensities[intensityIndex].value = 0

          texte = 'En appliquant la loi des noeuds, déterminer la valeur d\'intensité du courant manquante dans le circuit suivant :<br>'
          // On crée le circuit
          circuit = createComplexCircuit(elements, intensities)

          if (context.isHtml) {
            const span = document.createElement('div')
            span.innerHTML = circuit
            if (context.isDiaporama) {
              span.setAttribute('style', 'display: block; margin: auto; width:100%; max-width:750px; margin-bottom: 10px; margin-top: 10px;font-size: max(30px);')
            } else {
              span.setAttribute('style', 'display: block; margin: auto; width:100%; max-width:500px; margin-bottom: 10px; margin-top: 10px;font-size:22px;')
            }
            texte += `${span.outerHTML}`
          } else {
            texte += `${circuit}`
          }

          texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: 'I<sub>' + (intensityIndex + 1).toString() + '</sub> = ', texteApres: ' mA' })
          handleAnswers(this, i, { reponse: { value: valeurCachee, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

          texteCorr = `\\[I_{${intensityIndex + 1}} = ${valeurCachee} \\text{ mA}\\]`

          break
      }

      if (this.questionJamaisPosee(i, listeTypeDeQuestions[i], valeurCachee, sousChoix[i])) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(texte!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(texteCorr!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }
    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
