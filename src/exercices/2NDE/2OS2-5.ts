/**
 * 2OS2-5.ts
 * @author Régis Ferreira Da Silva
 * @author Thibault Giauffret
 * @date 2024-10-26
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { chiffresSignificatifs } from '../../lib/outils_phys/ecritures'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { prenom } from '../../lib/outils/Personne'

// à remplacer
export const titre = 'Exploiter les lois de Snell-Descartes pour la réflexion et la réfraction.'
export const interactifReady = true
export const interactifType = 'mathLive'
// à remplacer
export const dateDePublication = '05/11/2024'
// à remplacer : pnpm getNewUuid pour récupérer l'UUID pour un nouvel exercice
export const uuid = 'd7e5b'
// à remplacer "test-2" par la référence dans le référentiel (disponible dans "src/json/allExercice.json")
export const refs = {
  'fr-fr': ['2OS2-5']
}

export default class LoiSnellDescartes extends Exercice {
  constructor () {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 2, "1 : Calcul de l'indice d'un milieu (novice)\n2 : Calcul de l'angle réfracté (confirmé)"]
  }

  nouvelleVersion () {
    this.listeQuestions = []
    this.listeCorrections = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['novice']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['confirme']
    }

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles, this.nbQuestions)

    for (let i = 0, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      const iincident = randint(20, 50)
      const nun = randint(121, 199) / 100
      const ndeux = randint(201, 299) / 100
      const irefracte = Math.asin(nun * Math.sin(iincident * Math.PI / 180) / ndeux) * 180 / Math.PI

      switch (listeTypeDeQuestions[i]) {
        case 'novice':
          // On énonce la question
          this.question = `Lors d'une séance de TP, ${prenom()} pointe une source lumineuse sur un dioptre avec un angle de $i_{\\text{incident}}=${chiffresSignificatifs(iincident, 3)}^\\circ$ avec la normale.<br>
            Le milieu incident a pour indice de réfraction $n_{\\text{incident}}=${nun}$. <br>
            Le rayon lumineux réfracté fait un angle de $i_{\\text{réfraction}}=${chiffresSignificatifs(irefracte, 3)}^\\circ$ avec la normale.<br><br>`
          this.question += "Déterminer la valeur de l'indice de réfraction du milieu où a lieu le phénomène de réfraction. Pour cela, vous utiliserez la deuxième loi de Snell-Descartes ($n_1\\times \\sin i_1=n_2\\times \\sin i_2$).<br><br>"
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: "La valeur de l'indice de réfraction du milieu où se produit la réfraction est  $n_{\\text{réfracté}}=$", texteApres: '.' })
          //   On génère la réponse
          this.reponse = ndeux
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare } })
          //   On écrit la correction
          this.correction = `En appliquant la deuxième loi de Snell-Descartes au cas présent, on peut écrire :<br><br>
           $n_{\\text{incident}}\\times \\sin i_{\\text{incident}}=n_{\\text{réfracté}}\\times \\sin i_{\\text{réfracté}}$<br><br> 
            Soit en remplaçant par les valeurs numériques :<br><br>
             $ ${chiffresSignificatifs(nun, 3)}\\times \\sin ${chiffresSignificatifs(iincident, 3)}=n_{\\text{réfracté}}\\times \\sin ${chiffresSignificatifs(irefracte, 3)}$<br><br>

            En divisant chaque membre de l'égalité précédente par $\\sin ${chiffresSignificatifs(irefracte, 3)}$, on en déduit que :<br><br>
            
            $ n_{\\text{réfracté}} = \\frac{${nun}\\times \\sin ${iincident}}{\\sin ${chiffresSignificatifs(irefracte, 3)}}$.<br><br>
             Soit : $n_{\\text{réfracté}} = ${chiffresSignificatifs(ndeux, 3)}$.`
          // fin question
          break
        case 'confirme':
          // On énonce la question
          this.question = `Lors d'une séance de TP, ${prenom()} pointe une source lumineuse sur un dioptre avec un angle $i_{\\text{incident}}=${chiffresSignificatifs(iincident, 3)}^\\circ$ avec la normale. <br>
            Le milieu incident a pour indice de réfraction $n_{\\text{incident}}=${chiffresSignificatifs(nun, 3)}$. <br>
            Le milieu où a lieu le phénomène de réfraction a pour indice $n_{\\text{réfraction}}=${chiffresSignificatifs(ndeux, 3)}$.<br><br>`
          // On énonce la question
          this.question += "Déterminer la valeur de l'angle réfracté en utilisant la deuxième loi de Snell-Descartes ($n_1\\times \\sin i_1=n_2\\times \\sin i_2$).<br><br>"
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: "La valeur de l'angle réfracté est $i_{\\text{réfracté}} = $", texteApres: '$^\\circ$.' })
          //   On génère la réponse
          this.reponse = irefracte
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare } })
          //   On écrit la correction
          this.correction = `En appliquant la deuxième loi de Snell-Descartes au cas présent, on peut écrire :<br><br>
           $n_{\\text{incident}}\\times \\sin i_{\\text{incident}}=n_{\\text{réfracté}}\\times \\sin i_{\\text{réfracté}}$<br><br>
            Soit en remplaçant par les valeurs numériques :<br><br>
            
            $${chiffresSignificatifs(nun, 3)}\\times \\sin ${chiffresSignificatifs(iincident, 3)}=${chiffresSignificatifs(ndeux, 3)}\\times \\sin i_{\\text{réfracté}}$ <br><br>

            En divisant chaque membre de l'égalité précédente par $${chiffresSignificatifs(ndeux, 3)}$, on en déduit que : <br><br>
            
            $ \\sin i_{\\text{réfracté}} = \\frac{${chiffresSignificatifs(nun, 3)}\\times \\sin ${chiffresSignificatifs(iincident, 3)}}{${chiffresSignificatifs(ndeux, 3)}}$<br><br>

            En prenant l'arcsinus de l'égalité précédente, on en déduit $i_{\\text{réfracté}}= \\arcsin\\left(\\frac{${chiffresSignificatifs(nun, 3)}\\times \\sin ${chiffresSignificatifs(iincident, 3)}}{${chiffresSignificatifs(ndeux, 3)}}\\right) = ${chiffresSignificatifs(irefracte, 3)}^\\circ$.`
          // fin question
          break
      }

      if (this.questionJamaisPosee(i)) { // Si la question n'a jamais été posée, on en créé une autre, tgL ne peut pas prendre la même valeur
        this.listeQuestions.push(this.question!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(this.correction!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }

    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
