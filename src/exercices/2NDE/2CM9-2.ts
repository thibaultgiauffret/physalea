/**
* @author Thibault Giauffret
* 2CM9-2.ts
*/

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { equationCombustionAlcane, equationCombustionAlcool } from '../../lib/outils_phys/equation_transfo_chimique'
import { propositionsQcm } from '../../lib/interactif/qcm.js'
import { context } from '../../modules/context.js'

export const titre = 'Identifier le réactif limitant à partir des quantités de matière des réactifs et de l\'équation de réaction.'
export const interactifReady = true
export const interactifType = 'qcm'
export const uuid = '9e871'
export const refs = {
  'fr-fr': ['2CM9-2']
}
export const dateDePublication = '10/5/2024'

export default class reactifLimitant extends Exercice {
  constructor () {
    super()
    this.consigne = ''
    this.nbQuestions = 1
    this.sup = 1
    this.besoinFormulaireNumerique = ['Types de question ', 1, '1 : Niveau 1\n2 : Niveau 2']
  }

  nouvelleVersion () {
    this.sup = parseInt(this.sup)
    this.listeQuestions = []
    this.listeCorrections = []
    let typesDeQuestionsDisponibles: (1 | 2)[] = []
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = [1]
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = [2]
    }
    let texte: string = ''
    let texteCorr: string = ''
    // ToDo : fix the type...
    let values: { equation: string; especes: string[]; coeffs: { id: string; value: number; text: string; result: string; hidden: boolean; }[]; }
    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles, this.nbQuestions)

    for (let i = 0, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      const alcool = randint(0, 1)
      const stoichio = randint(0, 2)
      const nAlcaneAlcool = randint(1, 20)
      let nO2 = 0
      let ReponseProportionnalite = ''
      let ReponseConditionsStoechio = ''
      let ReponseAlcaneLimitant = ''
      let ReponseO2Limitant = ''

      // On récupère les valeurs de l'équation de combustion d'un alcane ou d'un alcool
      if (alcool === 0) {
        values = equationCombustionAlcane(i, false) as { equation: string; especes: string[]; coeffs: { id: string; value: number; text: string; result: string; hidden: boolean; }[] }
      } else {
        values = equationCombustionAlcool(i, false) as { equation: string; especes: string[]; coeffs: { id: string; value: number; text: string; result: string; hidden: boolean; }[] }
      }
      if (values.equation === '') {
        continue
      }
      //   On tire au sort si on est dans les conditions stoichiométriques ou non
      if (stoichio !== 2) {
        const O2Limitant = randint(0, 1)
        if (O2Limitant === 0) {
          nO2 = (nAlcaneAlcool * values.coeffs[1].value / values.coeffs[0].value) * (1 - Math.random() * 0.5)
          nO2 = Math.round(nO2)
        } else {
          nO2 = (nAlcaneAlcool * values.coeffs[1].value / values.coeffs[0].value) * (1 + Math.random() * 0.5)
          nO2 = Math.round(nO2)
        }
        ReponseProportionnalite = 'non'
        ReponseConditionsStoechio = 'non'
        if (nO2 / values.coeffs[1].value > nAlcaneAlcool / values.coeffs[0].value) {
          ReponseO2Limitant = 'non'
          ReponseAlcaneLimitant = 'oui'
        } else {
          ReponseO2Limitant = 'oui'
          ReponseAlcaneLimitant = 'non'
        }
      } else {
        nO2 = values.coeffs[1].value * nAlcaneAlcool / values.coeffs[0].value
        ReponseProportionnalite = 'oui'
        ReponseO2Limitant = 'non'
        ReponseAlcaneLimitant = 'non'
        ReponseConditionsStoechio = 'oui'
      }

      texte = 'On considère l\'équation de réaction chimique suivante :<br>'
      texte += values.equation
      texte += '<br>'
      texte += 'Les quantités de matière introduites initialement sont : $n_i(' + values.especes[0] + ') = ' + nAlcaneAlcool + '$ mol et $n_i(' + values.especes[1] + ') = ' + nO2 + '$ mol.<br><br>'

      switch (listeTypeDeQuestions[i]) {
        case 1:
          texte += 'On consigne alors ces informations dans le tableau suivant :<br><br>'
          texte += `$\\def\\arraystretch{1.5}\\begin{array}{|l|c|c|}
            \\hline
            \\text{Réactifs} & ${values.especes[0]} & ${values.especes[1]} \\\\
            \\hline
            \\text{Nombres stoechiométriques} & ${values.coeffs[0].text} & ${values.coeffs[1].text} \\\\
            \\hline
            \\text{Quantité de matière (mol)} & ${nAlcaneAlcool} & ${nO2} \\\\
            \\hline
            \\end{array}
            $<br><br>`
          texte += 'Choisir la (ou les) proposition(s) adaptée(s) :<br><br>'
          texteCorr = ''

          if (this.interactif || context.isAmc) {
            this.autoCorrection[i] = {}
            this.autoCorrection[i].options = { ordered: true, vertical: true }
            this.autoCorrection[i].enonce = `${texte}\n`
            this.autoCorrection[i].propositions = [
              {
                texte: 'Le tableau fait apparaître une situation de proportionnalité',
                statut: ReponseProportionnalite !== 'non'
              },
              {
                texte: 'Le réactif limitant est $' + values.especes[0] + '$',
                statut: ReponseAlcaneLimitant !== 'non'
              },
              {
                texte: 'Le réactif limitant est $' + values.especes[1] + '$',
                statut: ReponseO2Limitant !== 'non'
              },
              {
                texte: 'Les réactifs sont en proportions stoechiométriques (pas de réactif limitant)',
                statut: ReponseConditionsStoechio !== 'non'
              }
            ]
            if (this.interactif) {
              texte += propositionsQcm(this, i).texte
            }
          } else {
            texte += '$\\square$ Le tableau fait apparaître une situation de proportionnalité<br>'
            texte += '$\\square$ Le réactif limitant est $' + values.especes[0] + '$<br>'
            texte += '$\\square$ Le réactif limitant est $' + values.especes[1] + '$<br>'
            texte += '$\\square$ Les réactifs sont en proportions stoechiométriques (pas de réactif limitant)<br>'
          }
          break
        case 2:
          texte += 'Choisir la (ou les) proposition(s) adaptée(s) :<br><br>'
          texteCorr = 'On consigne alors les informations de l\'énoncé dans le tableau suivant :<br><br>'
          texteCorr += `$\\def\\arraystretch{1.5}\\begin{array}{|l|c|c|}
            \\hline
            \\text{Réactifs} & ${values.especes[0]} & ${values.especes[1]} \\\\
            \\hline
            \\text{Nombres stoechiométriques} & ${values.coeffs[0].text} & ${values.coeffs[1].text} \\\\
            \\hline
            \\text{Quantité de matière (mol)} & ${nAlcaneAlcool} & ${nO2} \\\\
            \\hline
            \\end{array}
            $<br><br>`
          if (this.interactif || context.isAmc) {
            this.autoCorrection[i] = {}
            this.autoCorrection[i].options = { ordered: true, vertical: true }
            this.autoCorrection[i].enonce = `${texte}\n`
            this.autoCorrection[i].propositions = [
              {
                texte: 'Le réactif limitant est $' + values.especes[0] + '$',
                statut: ReponseAlcaneLimitant !== 'non'
              },
              {
                texte: 'Le réactif limitant est $' + values.especes[1] + '$',
                statut: ReponseO2Limitant !== 'non'
              },
              {
                texte: 'Les réactifs sont en proportions stoechiométriques (pas de réactif limitant)',
                statut: ReponseConditionsStoechio !== 'non'
              }
            ]
            if (this.interactif) {
              texte += propositionsQcm(this, i).texte
            }
          } else {
            texte += '$\\square$ Le réactif limitant est $' + values.especes[0] + '$<br>'
            texte += '$\\square$ Le réactif limitant est $' + values.especes[1] + '$<br>'
            texte += '$\\square$ Les réactifs sont en proportions stoechiométriques (pas de réactif limitant)<br>'
          }
          break
      }
      texte += '<br><br>'
      if (ReponseProportionnalite === 'oui') {
        texteCorr += 'Le tableau fait apparaître une situation de proportionnalité entre les quantités de matière des réactifs et les nombres stoechiométriques dans l\'équation de réaction chimique. Les réactifs sont introduits en proportions stoechiométriques car ils sont introduits dans les proportions équivalentes aux nombres stoechiométriques.<br>'
      } else {
        texteCorr += `Le tableau ne fait pas apparaître une situation de proportionnalité entre les quantités de matière des réactifs et les nombres stoechiométriques. Les réactifs ne sont pas introduits en proportions stoechiométriques.<br><br>

            On peut donc identifier le réactif limitant. On calcule pour cela le rapport entre les quantités de matière des réactifs et les nombres stoechiométriques dans l'équation de chimique :<br><br>
            $\\frac{n(${values.especes[0]})}{${values.coeffs[0].text}} = \\frac{${nAlcaneAlcool}}{${values.coeffs[0].value}} = ${nAlcaneAlcool / values.coeffs[0].value}$<br><br>
            $\\frac{n(${values.especes[1]})}{${values.coeffs[1].text}} = \\frac{${nO2}}{${values.coeffs[1].value}} = ${nO2 / values.coeffs[1].value}$<br><br>`

        let reponseText = ''
        if (ReponseO2Limitant === 'oui') {
          reponseText = values.especes[1]
        } else {
          reponseText = values.especes[0]
        }
        texteCorr += `On peut donc conclure que le réactif limitant est le réactif $${reponseText}$ car il a le plus petit rapport entre la quantité de matière et le nombre stoechiométrique.<br>`
      }

      if (this.questionJamaisPosee(i, texte)) {
        this.listeQuestions.push(texte)
        this.listeCorrections.push(texteCorr)
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }
}
