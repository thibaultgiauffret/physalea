/**
 * 2OS2-8 : Lentille mince convergente
 * @author Thibault Giauffret
 * @author Régis Ferreira Da Silva
 */
import Exercice from '../Exercice'
// Géométrie dynamique
import { Point, point, tracePoint } from '../../lib/2d/points.js'
import { grille } from '../../lib/2d/reperes.js'
import { demiDroite, segment } from '../../lib/2d/segmentsVecteurs.js'
import { labelPoint } from '../../lib/2d/textes'
import { mathalea2d } from '../../modules/2dGeneralites.js'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import Alea2iep from '../../modules/Alea2iep.js'
import Figure from 'apigeom'
import figureApigeom from '../../lib/figureApigeom'
import { verifSegment, verifDemiDroite } from '../../lib/outils_phys/optique/verifRayons'

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { chiffresSignificatifs } from '../../lib/outils_phys/ecritures'
import { propositionsQcm } from '../../lib/interactif/qcm.js'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { numAlpha } from '../../lib/outils/outilString.js'
import { createList } from '../../lib/format/lists.js'
import { texteEnCouleurEtGras } from '../../lib/outils/embellissements'

export const titre = 'Utiliser le modèle du rayon lumineux pour déterminer graphiquement la position, la taille et le sens de l’image réelle d’un objet plan réel donnée par une lentille mince convergente.'
export const interactifReady = true
export const interactifType = 'custom'
export const dateDePublication = '19/11/2024'
export const uuid = '498cf'
export const refs = {
  'fr-fr': ['2OS2-8']
}

// Début de l'exercice
export default class LentilleMinceConvergente extends Exercice {
  echelle: number | undefined
  A: Point | undefined
  B: Point | undefined
  C: Point | undefined
  D: Point | undefined
  F: Point | undefined
  Fprime: Point | undefined
  L: Point | undefined
  Aprime: Point | undefined
  Bprime: Point | undefined
  O: Point | undefined
  delta: Point | undefined
  To: any
  Tf: any
  Tc: any
  AxeOptique: Segment | undefined
  ABsegment: Segment | undefined
  Lsegment: Segment | undefined
  BC: Segment | undefined
  CFprime: Segment | undefined
  BD: Segment | undefined
  DBprime: Segment | undefined
  BBprime: Segment | undefined
  AprimeBprimeSegment: Segment | undefined
  Ainteractif: any
  Binteractif: any
  Aprimeinteractif: any
  Bprimeinteractif: any
  Cinteractif: any
  Dinteractif: any
  Finteractif: any
  Fprimeinteractif: any
  Ointeractif: any
  Lsegmentinteractif: any
  AxeOptiqueinteractif: any
  L1: any
  L2: any
  O1: any
  O2: any
  d: any
  reponse2: number | undefined
  reponse3: string | undefined
  constructor () {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.sup2 = false
    this.exoCustomResultat = true
    this.besoinFormulaireNumerique = ['Choix des questions', 5, "Construction guidée des rayons\nConstruction de l'image\nPosition, taille et sens de l'image\nConstruction, position, taille et sens de l'image"]
  }

  nouvelleVersion () {
    this.figures = []
    this.listeQuestions = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['rayons_decouverte']
      this.besoinFormulaire2CaseACocher = false
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['rayons']
      this.besoinFormulaire2CaseACocher = false
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['pos_taille_sens']
      this.besoinFormulaire2CaseACocher = ['Échelle simple (1 carreau = 1 cm)']
    } else {
      typesDeQuestionsDisponibles = ['tout']
      this.besoinFormulaire2CaseACocher = false
    }

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles || [], this.nbQuestions)

    // On boucle pour générer les questions. cpt est le nombre maximal d'essai pour obtenir des questions différentes.
    for (let i = 0, cpt = 0, fprime, OA, AB; i < this.nbQuestions && cpt < 50;) {
      // On calcule les grandeurs (O est le centre de la lentille, A est l'objet, Aprime est l'image)
      // La figure est centrée en O et est comprise entre -10 et 10 en x
      fprime = randint(3, 5) // Focale comprise entre 2 et 4
      OA = 9 // Objet à gauche de la lentille
      const OAprime = 1 / (1 / fprime + 1 / -OA) // Relation de conjugaison pour déterminer la position de l'image
      AB = randint(2, 6) // Taille de l'objet (fixe pour l'instant, à changer plus tard)
      this.echelle = randint(2, 6) // Echelle de la figure
      const AprimeBprime = AB * OAprime / OA // Taille de l'image
      const Xmax = Math.round(OAprime + 7)
      const Xmin = -10
      const Ymax = Math.round(Math.max(AB, AprimeBprime) + 3)
      const Ymin = -Ymax

      // On énonce la question
      this.question = ''
      switch (listeTypeDeQuestions[i]) {
        case 'rayons_decouverte': {
          this.question = 'Réaliser le tracé des trois rayons lumineux permettant de former l\'image B\' de l\'objet B à travers la lentille convergente $(L)$ :<br>'
          // Liste
          const list = createList({
            items: [`en ${texteEnCouleurEtGras('rouge', 'red')}  : le rayon issu de $B$ parallèle à l'axe optique ressort de la lentille en passant par le foyer image ;`,
              ` en ${texteEnCouleurEtGras('vert', 'green')} : le rayon issu de $B$ passant par le foyer objet ressort de la lentille parallèle à l'axe optique ;`,
              `en ${texteEnCouleurEtGras('bleu', 'blue')} : le rayon issu de $B$ passant par le centre optique ressort de la lentille sans être dévié.`
            ],
            style: 'fleches',
            classOptions: ''
          })
          this.question += list + '<br>'
          if (this.interactif) {
            this.question += '<i>On utilisera deux segments et trois demi-droites.</i><br><br>'
          }
          break
        }
        case 'rayons':
          this.question = 'Réaliser le tracé des trois rayons lumineux permettant de former l\'image B\' de l\'objet B à travers la lentille convergente $(L)$ puis tracer l\'image A\'B\'.<br>'
          if (this.interactif) {
            this.question += '<i>Pour les rayons, on utilisera deux segments et trois demi-droites. Pour l\'image, on utilisera un segment.</i><br><br>'
          }
          break
        case 'pos_taille_sens' :
          if (this.sup2) {
            this.echelle = 1
          }
          this.question = 'On réalise l\'image $A\'B\'$ d\'un objet $AB$ à travers une lentille convergente $(L)$. La situation est modélisée ci-dessous.<br> En vous appuyant sur l\'échelle donnée, répondre aux questions ci-dessous.<br>'
          break
        case 'tout':
          this.question = 'On souhaite réaliser l\'image $A\'B\'$ d\'un objet $AB$ à travers une lentille convergente $(L)$. La situation est modélisée ci-dessous. En vous appuyant sur une construction géométrique, répondre aux questions suivantes.<br>'
          if (this.interactif) {
            this.question += '<i>Pour la construction géométrique des rayons, on utilisera deux segments et trois demi-droites. Pour l\'image, on utilisera un segment.</i><br><br>'
          }
      }
      // -------------------------------------------------
      //   On y ajoute le champ de réponse (non interactif et interactif)
      // -------------------------------------------------
      const objetsEnonce = []
      const objetsCorrection = []

      // On crée les points
      this.L = point(0, Ymin + 1, '(L)', 'below')
      this.delta = point(Xmax - 1.5, 0, 'Axe optique', 'above left')
      this.A = point(-OA, 0, 'A', 'below left')
      this.B = point(-OA, this.A.y + AB, 'B', 'above left')
      this.Bprime = point(OAprime, -AprimeBprime, 'B\'', 'below left')
      this.O = point(0, 0, 'O', 'above left')
      this.F = point(-fprime, 0, 'F', 'above left')
      this.Fprime = point(fprime, 0, 'F\'', 'above right')

      // On ajoute les libellés de certains points
      this.To = tracePoint(this.A, this.B)
      this.To.style = 'x'
      this.To.epaisseur = 1
      this.To.tailleTikz = 0.3
      this.Tf = tracePoint(this.F, this.Fprime)
      this.Tf.style = '|'
      this.Tf.epaisseur = 2
      this.Tf.taille = 4
      this.Tf.tailleTikz = 0.3

      // On crée les segments et droites
      this.AxeOptique = segment(point(Xmin, 0), point(Xmax, 0), 'black')
      this.AxeOptique.epaisseur = 1.5
      this.AxeOptique.pointilles = 2
      this.ABsegment = segment(this.A, this.B, 'black')
      this.ABsegment.epaisseur = 2
      this.Lsegment = segment(0, Ymin + 1, 0, Ymax - 1, 'black', '<->')
      this.Lsegment.epaisseur = 2

      // On crée les points et segments pour les rayons lumineux (correction)
      this.C = point(0, this.B.y, 'C', 'above left')
      this.BC = segment(this.B, this.C, 'red')
      this.BC.epaisseur = 1.5
      this.CFprime = demiDroite(this.C, this.Fprime, 'red')
      this.CFprime.epaisseur = 1.5
      this.D = point(0, -AprimeBprime, 'D', 'below right')
      this.Aprime = point(OAprime, 0, 'A\'', 'above right')

      this.BD = segment(this.B, this.D, 'green')
      this.BD.epaisseur = 1.5
      this.DBprime = demiDroite(this.D, this.Bprime, 'green')
      this.DBprime.epaisseur = 1.5
      this.BBprime = demiDroite(this.B, this.Bprime, 'blue')
      this.BBprime.epaisseur = 1.5
      this.AprimeBprimeSegment = segment(this.Aprime, this.Bprime, 'black')
      this.AprimeBprimeSegment.epaisseur = 2

      this.Tc = tracePoint(this.C, this.D, this.Aprime, this.Bprime)

      // On ajoute les objets à la liste utilisée pour générer la figure d'énoncé et de correction
      objetsEnonce.push(labelPoint(this.A, this.B, this.O, this.F, this.L, this.delta, this.Fprime), this.ABsegment, this.AxeOptique, this.Lsegment, this.To, this.Tf)
      objetsCorrection.push(labelPoint(this.A, this.B, this.O, this.F, this.L, this.delta, this.Fprime, this.C, this.D, this.Aprime, this.Bprime), this.To, this.Tf, this.Tc, this.BC, this.CFprime, this.BD, this.DBprime, this.BBprime, this.AprimeBprimeSegment, this.ABsegment, this.Lsegment, this.AxeOptique)

      // Grille
      let sc
      const g = grille(Xmin, Ymin, Xmax, Ymax, 'gray', 0.7)

      objetsEnonce.push(g)
      objetsCorrection.push(g)

      // On crée les figures interactive et non interactive
      if (this.interactif && (this.sup === 1 || this.sup === 2 || this.sup === 4)) {
        // Figure interactive
        console.log('Création de la figure interactive')
        const figure = new Figure({ xMin: Xmin, yMin: Ymin, width: 350, height: 268, scale: 0.5 })
        figure.options.thickness = 2
        this.figures[i] = figure

        // On génère les points
        this.Ainteractif = figure.create('Point', { x: this.A.x, y: this.A.y, label: this.A.nom, isFree: false })
        this.Ainteractif.isDeletable = false
        this.Binteractif = figure.create('Point', { x: this.B.x, y: this.B.y, label: this.B.nom, isFree: false })
        this.Binteractif.isDeletable = false
        this.Cinteractif = figure.create('Point', { x: this.C.x, y: this.C.y, label: this.C.nom, isFree: false, isVisible: false })
        this.Cinteractif.isDeletable = false
        this.Dinteractif = figure.create('Point', { x: this.D.x, y: -AprimeBprime, label: this.D.nom, isFree: false, isVisible: false })
        this.Dinteractif.isDeletable = false
        console.log(`Dinteractifbis=${JSON.stringify(this.Dinteractif)}`)
        this.Aprimeinteractif = figure.create('Point', { x: this.Aprime.x, y: this.Aprime.y, label: this.Aprime.nom, isFree: false, isVisible: false })
        this.Bprimeinteractif = figure.create('Point', { x: this.Bprime.x, y: this.Bprime.y, label: this.Bprime.nom, isFree: false, isVisible: false })
        this.Finteractif = figure.create('Point', { x: this.F.x, y: this.F.y, label: this.F.nom, isFree: false })
        this.Finteractif.isDeletable = false
        this.Fprimeinteractif = figure.create('Point', { x: this.Fprime.x, y: this.Fprime.y, label: this.Fprime.nom, isFree: false })
        this.Fprimeinteractif.isDeletable = false
        this.Ointeractif = figure.create('Point', { x: this.O.x, y: this.O.y, label: this.O.nom, isFree: false })
        this.Ointeractif.isDeletable = false
        this.L1 = figure.create('Point', { x: this.O.x, y: Ymax - 2, label: this.O.nom, isVisible: false })
        this.L1.isDeletable = false
        this.L2 = figure.create('Point', { x: this.O.x, y: Ymin + 2, label: this.O.nom, isVisible: false })
        this.L2.isDeletable = false
        this.O1 = figure.create('Point', { x: -10, y: 0, label: this.O.nom, isVisible: false })
        this.O1.isDeletable = false
        this.O2 = figure.create('Point', { x: 10, y: 0, label: this.O.nom, isVisible: false })
        this.O2.isDeletable = false
        // Objet
        this.d = figure.create('Segment', { point1: this.Ainteractif, point2: this.Binteractif, isSelectable: false })
        this.d.color = 'black'
        this.d.thickness = 2
        this.d.isDashed = false
        // Lentille
        this.Lsegmentinteractif = figure.create('Segment', { point1: this.L1, point2: this.L2, label: 'L', shape: '<->', isSelectable: false })
        this.Lsegmentinteractif.color = 'black'
        this.Lsegmentinteractif.thickness = 2
        this.Lsegmentinteractif.isDashed = false
        // Axe optique
        this.AxeOptiqueinteractif = figure.create('Segment', { point1: this.O1, point2: this.O2, label: 'Axe optique', isDashed: true, isSelectable: false })
        this.AxeOptiqueinteractif.color = 'black'
        this.AxeOptiqueinteractif.thickness = 1.5

        figure.setToolbar({ tools: ['SEGMENT', 'RAY', 'DRAG', 'REMOVE'] })
        const emplacementPourFigure = figureApigeom({ exercice: this, i, figure })
        this.question += emplacementPourFigure
        figure.create('Grid', {
          axeX: false,
          axeY: false,
          labelX: false,
          labelY: false
        })
      } else {
        if (this.sup === 3) { // Dans le cas où on demande la position, la taille et le sens de l'image, on trace tous les rayons
          this.question += '<br>' + mathalea2d(
            {
              xmin: Xmin,
              ymin: Ymin,
              xmax: Xmax,
              ymax: Ymax,
              pixelsParCm: 20,
              scale: sc
            },
            objetsCorrection
          )
        } else {
          this.question += '<br>' + mathalea2d(
            {
              xmin: Xmin,
              ymin: Ymin,
              xmax: Xmax,
              ymax: Ymax,
              pixelsParCm: 20,
              scale: sc
            },
            objetsEnonce
          )
        }
      }

      // On ajoute les champs de réponse supplémentaires
      //   On génère la réponse
      this.reponse = OAprime * this.echelle
      this.reponse2 = AprimeBprime * this.echelle
      this.reponse3 = 'oppose'
      if (this.sup === 3 || this.sup === 4) {
        this.question += `<br>Sur la représentation graphique ci-dessus, un carreau a une largeur de ${chiffresSignificatifs(this.echelle, 2)} cm.<br>`
        if (this.interactif) {
          this.question += ajouteChampTexteMathLive(this, 4 * i + 2, 'inline largeur01', { texteAvant: `<br>${numAlpha(0)} Position de l'image $A'$ par rapport à la lentille $(L)$ :<br><br> $OA' =$`, texteApres: ' cm' })
          this.question += ajouteChampTexteMathLive(this, 4 * i + 3, 'inline largeur01', { texteAvant: `<br>${numAlpha(1)} Taille de l'image<br><br> $A'B' =$`, texteApres: ' cm' })
          this.question += `<br>${numAlpha(2)} Sens de l'image $A'B'$ par rapport à l'objet $AB$ : `
          this.autoCorrection[4 * i + 4] = {}
          this.autoCorrection[4 * i + 4].options = { ordered: true }
          this.autoCorrection[4 * i + 4].propositions = [
            {
              texte: 'Même sens',
              statut: this.reponse3 !== 'oppose'
            },
            {
              texte: 'Sens opposé',
              statut: this.reponse3 !== 'meme'
            }
          ]
          this.question += propositionsQcm(this, 4 * i + 4).texte
          handleAnswers(this, 4 * i + 2, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare } })// options: { tolerance: 1e-1 }
          handleAnswers(this, 4 * i + 3, { reponse: { value: this.reponse2.toString(), compare: approximatelyCompare } })// options: { tolerance: 1e-1 }
        } else {
          this.question += `<br>${numAlpha(0)} Déterminer graphiquement la position $OA'$ de l'image $A'$ par rapport à la lentille $(L)$`
          this.question += `<br>${numAlpha(1)} Déterminer graphiquement la taille de l'image $A'B'$ de l'objet $AB$`
          this.question += `<br>${numAlpha(2)} Déterminer le sens de l'image $A'B'$ par rapport à l'objet $AB$`
        }
      }

      // -------------------------------------------------
      //   On écrit la correction
      // -------------------------------------------------

      // On ajoute les points et les segments à la liste utilisée pour générer la figure de correction

      this.correction = ''
      if (this.sup === 2) {
        this.correction += 'On réalise le tracé des trois rayons lumineux suivants :<br>'
        const list = createList({
          items: ["un rayon issu de $B$ parallèle à l'axe optique ressort de la lentille $(L)$ en passant par le foyer image $F'$ ;",
            "un rayon issu de $B$ passant par le foyer objet $F$ ressort de la lentille $(L)$ parallèle à l'axe optique ;",
            'un rayon issu de $B$ passant par le centre optique $O$ ressort de la lentille $(L)$ sans être dévié.'
          ],
          style: 'fleches',
          classOptions: ''
        })
        this.correction += list + '<br>'
      }

      if (this.sup === 1 || this.sup === 2 || this.sup === 4) {
        this.correction += mathalea2d(
          {
            xmin: Xmin,
            ymin: Ymin,
            xmax: Xmax,
            ymax: Ymax,
            pixelsParCm: 20,
            scale: 0.5
          },
          objetsCorrection
        )

        // On crée l'animation
        const anim = new Alea2iep()
        anim.equerreZoom(150, {})
        anim.recadre(Xmin, Ymax)
        // Situation de départ
        anim.tempo = 0.001
        anim.couleur = 'black'
        anim.pointsCreer(this.A, this.B)
        anim.traitRapide(this.A, this.B)
        anim.pointsCreer(this.O, this.F, this.Fprime)
        anim.traitRapide({ x: 0, y: Ymin + 1 }, { x: 0, y: Ymax - 1 }, { couleur: 'black', epaisseur: 2 })
        anim.traitRapide({ x: Xmin, y: 0 }, { x: Xmax, y: 0 }, { couleur: 'black', epaisseur: 1.5, pointilles: 2 })
        anim.crayonDeplacer(this.B, {})

        // Début de l'animation
        anim.tempo = 0.01
        anim.crayonMontrer(this.B)

        anim.regleSegment(this.B, this.C, { couleur: 'red' })
        anim.regleDemiDroiteOriginePoint(this.C, this.Fprime, { couleur: 'red' })
        anim.regleSegment(this.D, this.B, { couleur: 'green' })
        anim.regleDemiDroiteOriginePoint(this.D, this.Bprime, { couleur: 'green' })
        anim.crayonDeplacer(this.B, {})
        anim.regleSegment(this.B, this.O, { couleur: 'blue' })
        anim.regleProlongerSegment(this.B, this.O, { couleur: 'blue', longueur: 10 })
        anim.pointCreer(this.Aprime)
        anim.pointCreer(this.Bprime)
        anim.regleSegment(this.Aprime, this.Bprime, { couleur: 'black' })
        anim.crayonMasquer({})
        anim.regleMasquer({})

        this.correction += anim.htmlBouton(this.numeroExercice, i)
      }

      if (this.sup === 3 || this.sup === 4) {
        this.correction += `${numAlpha(0)} On mesure graphiquement ${Math.round(OAprime * 10) / 10} carreaux entre $O$ et $A'$.<br>
       Or, un carreau a une largeur de ${this.echelle} cm.<br>
       On peut donc établir le tableau de proportionnalité suivant :<br><br>

        $
        \\def\\arraystretch{1.5}
        \\begin{array}{|c|c|}
        \\hline
        1 \\text{ carreau} & ${chiffresSignificatifs(this.echelle, 2)}\\text{ cm} \\\\
        \\hline
        ${Math.round(OAprime * 10) / 10} \\text{ carreau(x)} & OA' \\\\
        \\hline
        \\end{array}$
        <br><br>
       En effectuant un produit en croix : $OA' = \\cfrac{${Math.round(OAprime * 10) / 10} \\, \\text{carreaux} \\times ${chiffresSignificatifs(this.echelle, 2)} \\,\\text{cm} }{1 \\,\\text{carreau} } = ${chiffresSignificatifs(this.reponse, 2)} \\, \\text{cm}$.<br>
        
        L'image $A'$ est donc située à  $OA' = ${chiffresSignificatifs(this.reponse, 2)} \\, \\text{cm}$ à droite de la lentille $(L)$.<br><br>`

        this.correction += `${numAlpha(1)} On mesure graphiquement ${chiffresSignificatifs(AprimeBprime, 2)} carreaux entre $A'$ et $B'$.<br>
        Comme précédemment, on peut établir le tableau de proportionnalité suivant :<br><br>

        $
        \\def\\arraystretch{1.5}
        \\begin{array}{|c|c|}
        \\hline
        1 \\text{ carreau} & ${chiffresSignificatifs(this.echelle, 2)}\\text{ cm} \\\\
        \\hline
        ${chiffresSignificatifs(AprimeBprime, 2)} \\text{ carreau(x)} & A'B' \\\\
        \\hline
        \\end{array}$
        <br><br>
         En effectuant un produit en croix : $A'B' = \\cfrac{${chiffresSignificatifs(AprimeBprime, 2)} \\, \\text{carreaux} \\times ${chiffresSignificatifs(this.echelle, 2)} \\,\\text{cm} }{1 \\,\\text{carreau} } = ${chiffresSignificatifs(this.reponse2, 2)} \\, \\text{cm}$.<br>
        
        La taille de l'image $A'B'$ vaut donc $ ${chiffresSignificatifs(this.reponse2, 2)} \\, \\text{cm}$.<br>`
        this.correction += `${numAlpha(2)} L'image $A'B'$ est de sens opposé à l'objet $AB$.<br>`
      }

      //   Si l'exercice généré est unique (i.e. la valeur de this.reponse n'est jamais sortie), alors on l'ajoute à la liste
      if (this.questionJamaisPosee(i, fprime, OA, AB)) {
        // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(this.question)
        this.listeCorrections.push(this.correction)
        i++
      }
      cpt++
    }

    // On convertit les champs de texte générés en contenu
    listeQuestionsToContenu(this)
  }

  // On gère la réponse de l'élève (vérification du tracé des rayons)
  correctionInteractive = (i) => {
    const figure = this.figures[i]
    const resultat = []
    let feedback = ''

    if (this.sup === 1) {
      const divFeedback = document.querySelector(`#feedbackEx${this.numeroExercice}Q${i}`) // Ne pas changer le nom du FeedBack, il est écrit en dur, ailleurs.
      const { isValid, message } = verifSegment({ figure, point1: this.Binteractif, point2: this.Cinteractif, tolerance: 0.25, color: 'red' })
      resultat.push(isValid ? 'OK' : 'KO')
      if (message !== '') { feedback += message + `En ${texteEnCouleurEtGras('rouge', 'red')} : segment entre B et C` + '<br>' }

      const { isValid: isValid2, message: message2 } = verifDemiDroite({ figure, point1: this.Cinteractif, point2: this.Fprimeinteractif, tolerance: 0.25, color: 'red' })
      resultat.push(isValid2 ? 'OK' : 'KO')
      if (message2 !== '') { feedback += message2 + `En ${texteEnCouleurEtGras('rouge', 'red')} : demi-droite partant de C et passant par F'` + '<br>' }

      const { isValid: isValid3, message: message3 } = verifDemiDroite({ figure, point1: this.Binteractif, point2: this.Ointeractif, tolerance: 0.25, color: 'blue' })
      resultat.push(isValid3 ? 'OK' : 'KO')
      if (message3 !== '') { feedback += message3 + `En ${texteEnCouleurEtGras('bleu', 'blue')} : demi-droite partant de B passant O` + '<br>' }

      const { isValid: isValid4, message: message4 } = verifSegment({ figure, point1: this.Binteractif, point2: this.Dinteractif, tolerance: 0.25, color: 'green' })
      resultat.push(isValid4 ? 'OK' : 'KO')
      if (message4 !== '') { feedback += message4 + `En ${texteEnCouleurEtGras('vert', 'green')} : segment entre B et D` + '<br>' }

      const { isValid: isValid5, message: message5 } = verifDemiDroite({ figure, point1: this.Dinteractif, point2: this.Bprimeinteractif, tolerance: 0.25, color: 'green' })
      resultat.push(isValid5 ? 'OK' : 'KO')
      if (message5 !== '') { feedback += message5 + `En ${texteEnCouleurEtGras('vert', 'green')} : demi-droite partant de D passant par B'` + '<br>' }

      if (divFeedback) divFeedback.innerHTML = feedback
    }

    if (this.sup === 2 || this.sup === 4) {
      const divFeedback = document.querySelector(`#feedbackEx${this.numeroExercice}Q${i}`) // Ne pas changer le nom du FeedBack, il est écrit en dur, ailleurs.
      const { isValid, message } = verifSegment({ figure, point1: this.Binteractif, point2: this.Cinteractif, tolerance: 0.25 })
      resultat.push(isValid ? 'OK' : 'KO')
      if (message !== '') { feedback += message + " Rayon issu de B et parallèle à l'axe optique jusqu'à la lentille" + '<br>' }

      const { isValid: isValid2, message: message2 } = verifDemiDroite({ figure, point1: this.Cinteractif, point2: this.Fprimeinteractif, tolerance: 0.25 })
      resultat.push(isValid2 ? 'OK' : 'KO')
      if (message2 !== '') { feedback += message2 + ' Rayon issu de C passant par le foyer image' + '<br>' }

      const { isValid: isValid3, message: message3 } = verifDemiDroite({ figure, point1: this.Binteractif, point2: this.Ointeractif, tolerance: 0.25 })
      resultat.push(isValid3 ? 'OK' : 'KO')
      if (message3 !== '') { feedback += message3 + ' Rayon issu de B passant par le centre optique' + '<br>' }

      const { isValid: isValid4, message: message4 } = verifSegment({ figure, point1: this.Binteractif, point2: this.Dinteractif, tolerance: 0.25 })
      resultat.push(isValid4 ? 'OK' : 'KO')
      if (message4 !== '') { feedback += message4 + ' Rayon issu de B passant par le foyer objet jusqu\'à la lentille' + '<br>' }

      const { isValid: isValid5, message: message5 } = verifDemiDroite({ figure, point1: this.Dinteractif, point2: this.Bprimeinteractif, tolerance: 0.25 })
      resultat.push(isValid5 ? 'OK' : 'KO')
      if (message5 !== '') { feedback += message5 + " Rayon issu de D et parallèle à l'axe optique" + '<br>' }

      // Image
      const { isValid: isValid6, message: message6 } = verifSegment({ figure, point1: this.Aprimeinteractif, point2: this.Bprimeinteractif, tolerance: 1 })
      resultat.push(isValid6 ? 'OK' : 'KO')
      if (message6 !== '') { feedback += message6 + " Image A'B'" + '<br>' }

      if (divFeedback) divFeedback.innerHTML = feedback
    }

    if (this.sup === 3 || this.sup === 4) {
      let divFeedback = document.querySelector(`#resultatCheckEx${this.numeroExercice}Q${4 * i + 2}`)
      let reponse = document.querySelector(`#champTexteEx${this.numeroExercice}Q${4 * i + 2}`).value
      if (reponse != null) {
        if (this.autoCorrection[4 * i + 2] != null && this.autoCorrection[4 * i + 2].reponse != null) {
          if (approximatelyCompare(reponse, this.reponse, { tolerance: 0.25 }).isOk) {
            if (divFeedback) divFeedback.innerHTML = '😎'
            resultat.push('OK')
          } else {
            if (divFeedback) divFeedback.innerHTML = '☹️'
            resultat.push('KO')
          }
        }
      }

      console.log(`#resultatCheck${this.numeroExercice}Q${4 * i + 3}`)
      divFeedback = document.querySelector(`#resultatCheckEx${this.numeroExercice}Q${4 * i + 3}`)
      reponse = document.querySelector(`#champTexteEx${this.numeroExercice}Q${4 * i + 3}`).value
      if (reponse != null) {
        if (this.autoCorrection[4 * i + 3] != null && this.autoCorrection[4 * i + 3].reponse != null) {
          if (approximatelyCompare(reponse, this.reponse2, { tolerance: 0.25 }).isOk) {
            if (divFeedback) divFeedback.innerHTML = '😎'
            resultat.push('OK')
          } else {
            if (divFeedback) divFeedback.innerHTML = '☹️'
            resultat.push('KO')
          }
        }
      }

      divFeedback = document.querySelector(`#feedbackEx${this.numeroExercice}Q${4 * i + 4}R1`)
      reponse = document.querySelector(`#checkEx${this.numeroExercice}Q${4 * i + 4}R1`).checked ? 'oppose' : 'meme'
      if (reponse != null) {
        if (reponse === this.reponse3) {
          if (divFeedback) divFeedback.innerHTML = '😎'
          resultat.push('OK')
        } else {
          if (divFeedback) divFeedback.innerHTML = '☹️'
          resultat.push('KO')
        }
      }
    }

    return resultat
  }
}
