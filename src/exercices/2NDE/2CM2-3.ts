/**
 * 2CM2-3.ts
 * @author Régis Ferreira Da Silva
 * @author Thibault Giauffret
 * @date 2024-07-04
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare, fonctionComparaison } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { prenom } from '../../lib/outils/Personne'
import { ecritureScientifique, chiffresSignificatifs } from '../../lib/outils_phys/ecritures'

// à remplacer
export const titre = "Déterminer la concentration en masse d'une solution"
export const interactifReady = true
export const interactifType = 'mathLive'
// à remplacer
export const dateDePublication = '04/01/2025'
// à remplacer : pnpm getNewUuid pour récupérer l'UUID pour un nouvel exercice
export const uuid = 'e9549'
// à remplacer "test-2" par la référence dans le référentiel (disponible dans "src/json/allExercice.json")
export const refs = {
  'fr-fr': ['2CM2-3']
}

export default class ConcentrationMasse extends Exercice {
  constructor () {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 3, '1 : Novice\n2 :Confirmé\n3 : Expert']
  }

  nouvelleVersion () {
    this.listeQuestions = []
    this.listeCorrections = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['novice']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['confirme']
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['expert']
    }

    const listeTypeDeQuestions = combinaisonListes(
      typesDeQuestionsDisponibles,
      this.nbQuestions
    )

    for (let i = 0, cpt = 0, tgL; i < this.nbQuestions && cpt < 50;) {
      const Massesolute = randint(10, 50)
      const VolumemL = randint(100, 500)
      const VolumeL = VolumemL / 1000
      const tgmL = Massesolute / VolumemL
      tgL = tgmL * 1000
      switch (listeTypeDeQuestions[i]) {
        case 'novice':
          // On énonce la question
          this.question = `${prenom()} prépare une solution aqueuse contenant ${Massesolute} g de soluté et ${VolumemL} mL d'eau.<br><br>`
          this.question += '<b>a.</b> Rappeler la formule reliant la concentration en masse, $t$, la masse de soluté, $m$, et le volume, $V$.<br>'
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, 2 * i, 'inline largeur01', { texteAvant: '$t = $', texteApres: '' })
          //   On génère la réponse
          this.reponse = '$\\frac{m}{V}$'
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
          handleAnswers(this, 2 * i, { reponse: { value: this.reponse, compare: fonctionComparaison } })
          //   On écrit la correction
          this.correction = `<b>a.</b> La formule reliant la concentration en masse, la masse de soluté et le volume de solution est ${this.reponse}.`
          // fin question
          // On énonce la question
          this.question += '<br><b>b.</b> Calculer la concentration en masse, $t$, en g/mL avec 2 chiffres significatifs.<br>'
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, 2 * i + 1, 'inline largeur01', { texteAvant: '$t = $', texteApres: 'g/mL' })
          //   On génère la réponse
          this.reponse = Math.round(tgmL * 100) / 100
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
          handleAnswers(this, 2 * i + 1, { reponse: { value: this.reponse, compare: approximatelyCompare }, options: { tolerance: 0.1 } })
          //   On écrit la correction
          this.correction += `<br><b>b.</b> En effectuant l'application numérique à l'aide de la relation précédente, on obtient :
          
          $t = \\cfrac{${Massesolute}\\, \\text{g}}{${VolumemL}\\, \\text{mL}} = ${this.reponse}\\, \\text{g/mL} =
          ${ecritureScientifique(this.reponse, 2)}\\, \\text{g/mL}$`
          break
        case 'confirme':
          // On énonce la question
          this.question = `${prenom()} prépare une solution aqueuse contenant ${Massesolute} g de soluté et ${VolumemL} mL d'eau. Déterminer la concentration en masse, $t$, en g/mL avec 2 chiffres significatifs.<br><br>`
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$t = $', texteApres: 'g/mL' })
          //   On génère la réponse
          this.reponse = Math.round(tgmL * 100) / 100
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare }, options: { tolerance: 0.1 } })
          //   On écrit la correction
          this.correction = `La relation reliant la concentration en masse, la masse de soluté et le volume de solution est $t=\\cfrac{m}{V}$<br>
          
          En effectuant l'application numérique, on trouve la valeur numérique : $t = \\cfrac{${Massesolute}\\, \\text{g}}{${VolumemL}\\, \\text{mL}} = ${this.reponse}\\, \\text{g/mL} = ${ecritureScientifique(this.reponse, 2)}\\, \\text{g/mL}$`
          break
        case 'expert':
          // On énonce la question
          this.question = `${prenom()} prépare une solution aqueuse contenant ${Massesolute} g de soluté et ${VolumemL} mL d'eau. Déterminer la concentration en masse, $t$, en g/L avec 2 chiffres significatifs.<br><br>`
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$t = $', texteApres: 'g/L' })
          //   On génère la réponse
          this.reponse = Math.round(tgL * 100) / 100
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare }, options: { tolerance: 0.1 } })
          //   On écrit la correction
          this.correction = `La formule reliant la concentration en masse, la masse de soluté et le volume de solution est $t=\\cfrac{m}{V}$, <br> soit en remplaçant par les valeurs de l'énoncé puis en convertissant le volume en litre : <br><br>
          $t=\\cfrac{${Massesolute}\\, \\text{g}}{${VolumemL} \\, \\text{mL}}=\\cfrac{${Massesolute} \\, \\text{g}}{${VolumeL} \\, \\text{L}}$ <br><br>
          
          En effectuant l'application numérique, on trouve la valeur numérique : $t = ${this.reponse}\\, \\text{g/L} = ${ecritureScientifique(this.reponse, 2)}\\, \\text{g/L}$.`
          break
      }

      if (this.questionJamaisPosee(i, tgL!)) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(this.question!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(this.correction!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }
    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
