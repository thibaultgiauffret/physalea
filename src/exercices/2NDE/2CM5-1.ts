/**
 * 2CM5-1.ts
 * @author Thibault Giauffret
 * @date 2024-04-03
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare, fonctionComparaison } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { KeyboardType } from '../../lib/interactif/claviers/keyboard'
import { miseEnEvidence } from '../../lib/outils/embellissements'
import { context } from '../../modules/context.js'

export const titre = 'Déterminer la position de l’élément dans le tableau périodique'

export const uuid = 'b8c15'
export const ref = '2CM5-1'
export const refs = {
  'fr-fr': ['2CM5-1']
}
export const interactifReady = true
export const interactifType = 'mathLive'

export default class PosTableauPeriodique extends Exercice {
  constructor () {
    super()
    this.titre = titre
    this.consigne = ''
    this.nbQuestions = 3
    this.sup = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 4, '1 : Numéro de ligne\n2 :Numéro de colonne\n3 : Symbole de l\'élément\n4 : Position de l\'élément']
  }

  nouvelleVersion () {
    this.listeQuestions = []
    this.listeCorrections = []

    this.consigne = 'On souhaite déterminer la position de différents éléments dans la classification périodique simplifiée (3 premières lignes et 8 colonnes).'

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['typeE1']
      this.consigne = ''
      this.introduction = ''
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['typeE2']
      this.consigne = ''
      this.introduction = ''
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['typeE3']
      this.consigne = 'On souhaite déterminer la position de différents éléments dans la classification périodique simplifiée (3 premières lignes et 8 colonnes) suivante :'
      // // Temporaire !
      if (context.isHtml) {
        this.introduction = '<img src="assets/images/classification_periodique.png" style="width: calc(100% * {zoomFactor}" alt="énoncé" />'
      } else {
        this.introduction = '\\begin{center}\n' + '\\qrcode{https://physalea.fr/alea/assets/images/classification_periodique.png}\n' + '\\end{center}\n'
      }
    } else {
      this.consigne = ''
      this.introduction = ''
      typesDeQuestionsDisponibles = ['typeE4']
    }

    const listeTypeDeQuestions = combinaisonListes(
      typesDeQuestionsDisponibles,
      this.nbQuestions
    )

    let a; let b; let c; let d; let e = 0
    let col; let lig; let somme = ''
    let elem; let elemNom; let config = ''
    const elemList = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar']
    const elemListNoms = ['Hydrogène', 'Hélium', 'Lithium', 'Béryllium', 'Bore', 'Carbone', 'Azote', 'Oxygène', 'Fluor', 'Néon', 'Sodium', 'Magnésium', 'Aluminium', 'Silicium', 'Phosphore', 'Soufre', 'Chlore', 'Argon']

    for (let i = 0, texte, texteCorr, cpt = 0, n; i < this.nbQuestions && cpt < 50;) {
      switch (listeTypeDeQuestions[i]) {
        case 'typeE1':
          // Choix d'un élement (Z <= 18) au hasard
          n = randint(1, 18)
          texte = `Déterminer le numéro de ligne de l'élément de numéro atomique $Z = ${n}$ et de configuration électronique `
          // Génération de la configuration électronique sous la forme 1s^a 2s^b 2p^c...
          somme = ''
          if (n <= 2) {
            a = n
            config = `$1\\text{s}^${a}$`
            col = n
            lig = 1
          } else if (n <= 4) {
            a = 2
            b = n - a
            config = `$1\\text{s}^${a} 2\\text{s}^${b}$`
            col = n - a
            lig = 2
          } else if (n <= 10) {
            a = 2
            b = 2
            c = n - a - b
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c}$`
            col = n - a
            lig = 2
            somme = `${b} + ${c}`
          } else if (n <= 12) {
            a = 2
            b = 2
            c = 6
            d = n - a - b - c
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d}$`
            col = n - a - b - c
            lig = 3
          } else {
            a = 2
            b = 2
            c = 6
            d = 2
            e = n - a - b - c - d
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d} 3\\text{p}^${e}$`
            col = n - a - b - c
            lig = 3
            somme = `${d} + ${e}`
          }

          texte += `${config}`
          texte += '<br>'
          texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: 'Numéro de ligne : ', texteApres: '' })
          handleAnswers(this, i, { reponse: { value: lig, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

          texteCorr = `En analysant la configuration électronique, on constate que la couche de valence est la couche ${lig} (dernière couche remplie ou en cours de remplissage).<br><br>
           Or le numéro de la couche de valence correspond à la ligne dans la classification périodique. <br><br>
           L'élément se trouve donc dans la ligne $${miseEnEvidence(lig.toString())}$.`

          break
        case 'typeE2':
          // Choix d'un élement (Z <= 18) au hasard
          n = randint(1, 18)
          texte = `Déterminer le numéro de colonne de l'élément de numéro atomique $Z = ${n}$ et de configuration électronique `
          // Génération de la configuration électronique sous la forme 1s^a 2s^b 2p^c...
          somme = ''
          if (n <= 2) {
            a = n
            config = `$1\\text{s}^${a}$`
            col = n
            lig = 1
          } else if (n <= 4) {
            a = 2
            b = n - a
            config = `$1\\text{s}^${a} 2\\text{s}^${b}$`
            col = n - a
            lig = 2
          } else if (n <= 10) {
            a = 2
            b = 2
            c = n - a - b
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c}$`
            col = n - a
            lig = 2
            somme = `${b} + ${c}`
          } else if (n <= 12) {
            a = 2
            b = 2
            c = 6
            d = n - a - b - c
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d}$`
            col = n - a - b - c
            lig = 3
          } else {
            a = 2
            b = 2
            c = 6
            d = 2
            e = n - a - b - c - d
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d} 3\\text{p}^${e}$`
            col = n - a - b - c
            lig = 3
            somme = `${d} + ${e}`
          }
          texte += `${config}`
          texte += '<br>'
          texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: 'Numéro de colonne : ', texteApres: '' })
          handleAnswers(this, i, { reponse: { value: col, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

          texteCorr = `En analysant la configuration électronique, on constate que la couche de valence est la couche ${lig} (dernière couche remplie ou en cours de remplissage).<br><br>        
          Dans cette couche se trouvent `
          if (somme !== '') {
            texteCorr += `${somme} = ${col}`
          } else {
            texteCorr += `${col}`
          }
          texteCorr += ` électron(s) (appelés électron(s) de valence). <br><br>       
          Or le nombre d'électrons de valence correspond à la colonne dans la classification périodique simplifiée.<br><br>
          L'élément se trouve donc dans la colonne $${miseEnEvidence(col.toString())}$.`
          break
        case 'typeE3':
          // Choix d'un élement (Z <= 18) au hasard
          n = randint(1, 18)
          elem = elemList[n - 1]
          elemNom = elemListNoms[n - 1]
          texte = 'Déterminer le symbole de l\'élément de configuration électronique '
          // Génération de la configuration électronique sous la forme 1s^a 2s^b 2p^c...
          somme = ''
          if (n <= 2) {
            a = n
            config = `$1\\text{s}^${a}$`
            col = n
            lig = 1
          } else if (n <= 4) {
            a = 2
            b = n - a
            config = `$1\\text{s}^${a} 2\\text{s}^${b}$`
            col = n - a
            lig = 2
          } else if (n <= 10) {
            a = 2
            b = 2
            c = n - a - b
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c}$`
            col = n - a
            lig = 2
            somme = `${b} + ${c}`
          } else if (n <= 12) {
            a = 2
            b = 2
            c = 6
            d = n - a - b - c
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d}$`
            col = n - a - b - c
            lig = 3
          } else {
            a = 2
            b = 2
            c = 6
            d = 2
            e = n - a - b - c - d
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d} 3\\text{p}^${e}$`
            col = n - a - b - c
            lig = 3
            somme = `${d} + ${e}`
          }

          texte += `${config}`
          texte += '<br>'
          texte += ajouteChampTexteMathLive(this, i, 'inline largeur25 ' + KeyboardType.alphanumeric, { texteAvant: 'Symbole de l\'élément : ' })
          handleAnswers(this, i, { reponse: { value: elem, compare: fonctionComparaison, options: { texteSansCasse: true } } })

          texteCorr = `Le symbole de l'élément est $${miseEnEvidence(elem)}$ (${elemNom}).`

          break
        case 'typeE4':
          // Choix d'un élement (Z <= 18) au hasard
          n = randint(1, 18)
          texte = `Déterminer la position de l'élément de numéro atomique $Z = ${n}$ dans la classification périodique simplifiée.`
          // Génération de la configuration électronique sous la forme 1s^a 2s^b 2p^c...
          if (n <= 2) {
            a = n
            col = 1
            lig = 1
            config = `$1\\text{s}^${a}$`
          } else if (n <= 4) {
            a = 2
            b = n - a
            col = n - a
            lig = 2
            config = `$1\\text{s}^${a} 2\\text{s}^${b}$`
          } else if (n <= 10) {
            a = 2
            b = 2
            c = n - a - b
            col = n - a
            lig = 2
            somme = `${b} + ${c}`
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c}$`
          } else if (n <= 12) {
            a = 2
            b = 2
            c = 6
            d = n - a - b - c
            col = n - a - b - c
            lig = 3
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d}$`
          } else {
            a = 2
            b = 2
            c = 6
            d = 2
            e = n - a - b - c - d
            col = n - a - b - c
            lig = 3
            somme = `${d} + ${e}`
            config = `$1\\text{s}^${a} 2\\text{s}^${b} 2\\text{p}^${c} 3\\text{s}^${d} 3\\text{p}^${e}$`
          }
          // texte += `${config}`
          texte += '<br>'
          texte += ajouteChampTexteMathLive(this, 2 * i, 'inline largeur01', { texteAvant: 'Numéro de ligne : ', texteApres: '' })
          handleAnswers(this, 2 * i, { reponse: { value: lig, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
          texte += ajouteChampTexteMathLive(this, 2 * i + 1, 'inline largeur01', { texteAvant: "<span style='margin-left: 10px'>et numéro de colonne : </span>", texteApres: '' })
          handleAnswers(this, 2 * i + 1, { reponse: { value: col, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

          texteCorr = `Pour $Z = ${n}$, on a la configuration électronique suivante : `
          texteCorr += `${config}.<br><br>`
          texteCorr += `En analysant la configuration électronique, on constate que la couche de valence est la couche ${lig} (dernière couche remplie ou en cours de remplissage).<br><br>
            Or le numéro de la couche de valence correspond à la ligne dans la classification périodique. <br><br>
            L'élément se trouve donc dans la ligne $${miseEnEvidence(lig.toString())}$.<br><br>`
          texteCorr += 'Dans cette couche se trouvent '
          if (somme !== '') {
            texteCorr += `${somme} = ${col}`
          } else {
            texteCorr += `${col}`
          }
          texteCorr += ` électron(s) (appelés électron(s) de valence). <br><br>
            Or le nombre d'électrons de valence correspond à la colonne dans la classification périodique simplifiée.<br><br>
            L'élément se trouve donc dans la colonne $${miseEnEvidence(col.toString())}$.`
          break
      }

      if (this.questionJamaisPosee(i, n!)) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(texte!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(texteCorr!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }
    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
