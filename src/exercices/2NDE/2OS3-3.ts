import figureApigeom from '../../lib/figureApigeom'
import Figure from 'apigeom'
import { Droite, droite } from '../../lib/2d/droites.js'
import { TracePoint, point, tracePoint } from '../../lib/2d/points.js'
import { repere } from '../../lib/2d/reperes.js'
import { labelPoint, texteParPosition } from '../../lib/2d/textes'
import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { mathalea2d, colorToLatexOrHTML } from '../../modules/2dGeneralites.js'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { min, max } from 'mathjs'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'

export const titre = 'Utiliser la loi d\'Ohm'
export const interactifReady = true
export const interactifType = 'custom'

/**
* @author Thibault Giauffret
* 2OS3-23.js
*/
export const uuid = '8775d'
export const ref = '2OS3-3'
export const refs = {
  'fr-fr': ['2OS3-3'],
  'fr-ch': ['']
}
export default class LoiOhm extends Exercice {
  figures!: Figure[]
  coefficients!: [number, number][]
  constructor () {
    super()
    this.consigne = 'On souhaite étudier les propriétés d\'un conducteur ohmique. Pour cela, on relève les valeurs d\'intensité traversant le dipôle lorsque ce dernier est soumis à différentes valeurs de tension.'
    this.nbQuestions = 1 // On complète le nb de questions
    this.tailleDiaporama = 3
    this.sup = 1
    this.besoinFormulaireNumerique = ['Types de question ', 4, '1 : Calcul de valeur de résistance\n2 : Calcul de valeur de tension/intensité\n3 : Pente de la caractéristique \n4 : Représentation graphique U = f(I)\n5 : Représentation graphique I = f(U)']
  }

  nouvelleVersion (numeroExercice: number) {
    this.figures = []
    this.coefficients = []
    this.sup = parseInt(this.sup)
    this.listeQuestions = []
    this.listeCorrections = []
    let typesDeQuestionsDisponibles: (1 | 2 | 3 | 4 | 5)[] = []
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = [1]
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = [2]
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = [3]
    } else if (this.sup === 4) {
      typesDeQuestionsDisponibles = [4]
    } else if (this.sup === 5) {
      typesDeQuestionsDisponibles = [5]
    }
    let texte: string = ''
    let texteCorr: string = ''
    let a: number, b: number

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles, this.nbQuestions)
    const textO = texteParPosition('O', -0.5, -0.5, 0, 'black', 1)
    for (let i = 0, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      let xA: number, yA: number, xB: number, yB: number,
        droiteAB: Droite,
        cadre: { xMin: number, yMin: number, xMax: number, yMax: number },
        monRepere: unknown,
        tA: TracePoint, tB: TracePoint,
        lA: unknown, lB: unknown,
        cadreFenetreSvg: unknown,
        f: (x: number) => number
      texte = ''
      texteCorr = ''
      a = 0
      b = 0

      switch (listeTypeDeQuestions[i]) {
        case 1:
          {
            this.interactifType = 'mathLive'
            a = randint(0, 6, [0])
            b = 0
            this.coefficients[i] = [a, b]
            f = (x) => a * x + b

            const ligneX: number[] = []
            const ligneY: number[] = []
            const pas = randint(1, 4)
            for (let i = 0; i < 5; pas) {
              const x = i * pas
              const y = x * a + b
              ligneX[i] = x
              ligneY[i] = y
              i++
            }

            texte = 'Déterminer la valeur de résistance du conducteur ohmique associée aux valeurs suivantes :<br><br>'
            texte += `$\\def\\arraystretch{1.5}\\begin{array}{|l|c|c|c|c|c|}
            \\hline
            I (\\text{A}) & ${ligneX[0]} & ${ligneX[1]} & ${ligneX[2]} & ${ligneX[3]} & ${ligneX[4]} \\\\
            \\hline
            U (\\text{V}) & ${ligneY[0]} & ${ligneY[1]} & ${ligneY[2]} & ${ligneY[3]} & ${ligneY[4]} \\\\
            \\hline
            \\end{array}
            $<br><br>`

            texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$R = $', texteApres: ' $\\Omega$' })
            handleAnswers(this, i, { reponse: { value: a, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
            console.log(this)

            texteCorr = 'En utilisant le tableau de valeurs, on remarque une situation de proportionnalité entre les valeurs de tension et d\'intensité (ce qui est le cas pour les conducteurs ohmiques).<br>'
            texteCorr += 'On a par exemple :'
            texteCorr += `$\\cfrac{${ligneY[1]}}{${ligneX[1]}} = \\cfrac{${ligneY[2]}}{${ligneX[2]}} = \\cfrac{${ligneY[3]}}{${ligneX[3]}} = \\cfrac{${ligneY[4]}}{${ligneX[4]}} = ${a}$<br>`
            texteCorr += 'Or d\'après la loi d\'Ohm, on a : $U = R \\times I \\Leftrightarrow R = \\cfrac{U}{I}$<br>'
            texteCorr += `On en déduit alors que le coefficient de proportionnalité correspond à la valeur de la résistance du conducteur ohmique :  \\[R = ${a}\\ \\Omega\\]`
          }
          break
        case 2:
          {
            this.interactifType = 'mathLive'

            // On choisit aléatoirement les coefficients a et b
            a = randint(2, 8, [0])
            b = 0
            this.coefficients[i] = [a, b]
            f = (x) => a * x + b

            // Création des listes de valeurs
            const ligneX: number[] = []
            const ligneY: number[] = []

            for (let i = 0; i < 5; 1) {
              const x = randint(0, 10, ligneX)
              const y = x * a + b
              ligneX[i] = x
              ligneY[i] = y
              i++
            }

            // Tri des listes
            const [indices, ligneXSorted] = ligneX.slice().sort((a, b) => a - b).reduce((acc, x) => {
              const index = ligneX.indexOf(x)
              acc[0].push(index)
              acc[1].push(x)
              return acc
            }, [[], []] as [number[], number[]])
            const ligneYSorted = indices.map(i => ligneY[i].toString())

            // Passage en string
            const ligneXSorted2 = ligneXSorted.map(x => x.toString())
            const ligneYSorted2 = ligneYSorted.map(y => y.toString())

            // Enlever une valeur aléatoirement soit dans listeX, soit dans listeY
            const choix = randint(0, 1)
            const index = randint(0, ligneXSorted.length - 1)
            let reponse = 0
            let reponse2 = 0
            let exemple = ''
            let exemple2 = ''
            const grandeur = choix === 0 ? 'I' : 'U'
            const unite = choix === 0 ? 'A' : 'V'
            const unite2 = choix === 0 ? 'V' : 'A'
            if (choix === 0) {
              reponse = parseFloat(ligneXSorted2[index])
              reponse2 = parseFloat(ligneYSorted2[index])
              exemple = ligneXSorted2[(index + 1) % 5]
              exemple2 = ligneYSorted2[(index + 1) % 5]
              ligneXSorted2[index] = '?'
            } else {
              reponse = parseFloat(ligneYSorted2[index])
              reponse2 = parseFloat(ligneXSorted2[index])
              exemple = ligneYSorted2[(index + 1) % 5]
              exemple2 = ligneXSorted2[(index + 1) % 5]
              ligneYSorted2[index] = '?'
            }

            texte = 'Déterminer la valeur manquante dans le tableau suivant:<br><br>'
            texte += `$\\def\\arraystretch{1.5}\\begin{array}{|l|c|c|c|c|c|}
            \\hline
            I (\\text{A}) & ${ligneXSorted2[0]} & ${ligneXSorted2[1]} & ${ligneXSorted2[2]} & ${ligneXSorted2[3]} & ${ligneXSorted2[4]} \\\\
            \\hline
            U (\\text{V}) & ${ligneYSorted2[0]} & ${ligneYSorted2[1]} & ${ligneYSorted2[2]} & ${ligneYSorted2[3]} & ${ligneYSorted2[4]} \\\\
            \\hline
            \\end{array}
            $<br><br>`

            texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: `$${grandeur} = $`, texteApres: ` $\\text{${unite}}$` })
            handleAnswers(this, i, { reponse: { value: reponse, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })

            texteCorr = `On cherche la valeur de $${grandeur}$ associée à la valeur de ${reponse2} ${unite2}.<br>`
            texteCorr += 'Comme il s\'agit d\'un conducteur ohmique, on cherche le coefficient de proportionnalité entre les valeurs de tension et d\'intensité.<br>'
            texteCorr += 'On a par exemple :'
            if (choix === 0) {
              texteCorr += `$\\cfrac{${exemple}}{${exemple2}} = \\cfrac{1}{${a}}$<br>`
              texteCorr += `On en déduit que la valeur de $${grandeur}$ associée à la valeur de ${reponse2} ${unite2}  est :  \\[${grandeur} = ${reponse2} \\times \\cfrac{1}{${a}} =  ${reponse}\\ \\text{${unite}}\\]`
            } else {
              texteCorr += `$\\cfrac{${exemple}}{${exemple2}} = ${a}$<br>`
              texteCorr += `On en déduit que la valeur de $${grandeur}$ associée à la valeur de ${reponse2} ${unite2}  est :  \\[${grandeur} = ${reponse2} \\times ${a} =  ${reponse}\\ \\text{${unite}}\\]`
            }
          }
          break
        case 3:
          {
            this.interactifType = 'mathLive'

            // On choisit aléatoirement les coefficients a et b
            a = randint(0, 5, [0])
            b = 0
            this.coefficients[i] = [a, b]
            f = (x) => a * x + b

            // Tracé de la droite
            xA = 0
            yA = f(xA)
            xB = 3// Abscisse de B
            yB = f(xB)// Ordonnée de B

            const A = point(xA, yA, 'A')
            const B = point(xB, yB, 'B')
            droiteAB = droite(A, B)
            droiteAB.color = colorToLatexOrHTML('red')
            droiteAB.epaisseur = 2

            cadre = {
              xMin: min(0, xA - 1, xB - 1),
              yMin: min(0, yA - 1, yB - 1),
              xMax: max(5, xA + 1, xB + 1),
              yMax: max(5, yA + 1, yB + 1)
            }

            cadreFenetreSvg = {
              xmin: cadre.xMin,
              ymin: cadre.yMin,
              xmax: cadre.xMax,
              ymax: cadre.yMax,
              scale: 0.6
            }

            monRepere = repere(cadre)

            tA = tracePoint(A, 'red') // Variable qui trace les points avec une croix
            tB = tracePoint(B, 'red') // Variable qui trace les points avec une croix
            lA = labelPoint(A, 'red')// Variable qui trace les nom s A et B
            lB = labelPoint(B, 'red')// Variable qui trace les nom s A et B

            tA.taille = 5
            tA.epaisseur = 2
            tB.taille = 5
            tB.epaisseur = 2

            texte = 'On propose la caractéristique $U = f(I)$ d\'un conducteur ohmique :<br><br>'

            // @ts-expect-error mathalea2d n'est pas typé
            texte += mathalea2d(cadreFenetreSvg,
              lA, lB, monRepere, droiteAB, tA, tB, textO)

            texte += '<br>Déterminer graphiquement la valeur de la résistance du conducteur ohmique.<br><br>'

            texte += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$R = $', texteApres: ' $\\Omega$' })
            handleAnswers(this, i, { reponse: { value: a, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
            console.log(this)

            texteCorr = 'On remarque une situation de proportionnalité entre les valeurs de tension et d\'intensité (droite passant par l\'origine).<br><br>'
            texteCorr += 'D\'après la loi d\'Ohm, on a : $U = R \\times I$. Le coefficient directeur de la droite est donc $R$.<br>'
            texteCorr += 'En utilisant les coordonnées de deux points A et B sur la droite, on détermine le coefficient directeur de la caractéristique :<br><br>'

            texteCorr += `$a = \\cfrac{\\text{ordonnée de B} - \\text{ordonnée de A}}{\\text{abscisse de B} - \\text{abscisse de A}} = \\cfrac{${yB} - ${yA}}{${xB} - ${xA}} = \\cfrac{${yB - yA}}{${xB - xA}} = ${a}$<br><br>`

            texteCorr += 'On en déduit que la valeur de la résistance du conducteur ohmique est :  \\[R = a = ' + a + '\\ \\Omega\\]'
          }
          break
        case 4:
          {
            this.interactifType = 'custom'

            // On choisit aléatoirement les coefficients a et b
            a = randint(0, 6, [0])
            b = 0
            this.coefficients[i] = [a, b]
            f = (x) => a * x + b

            // Création des listes de valeurs
            const ligneX: number[] = []
            const ligneY: number[] = []
            const pas = randint(1, 4)
            for (let i = 0; i < 5; pas) {
              const x = i * pas
              const y = x * a + b
              ligneX[i] = x
              ligneY[i] = y
              i++
            }

            texte = `$\\def\\arraystretch{1.5}\\begin{array}{|l|c|c|c|c|c|}
            \\hline
            I (\\text{A}) & ${ligneX[0]} & ${ligneX[1]} & ${ligneX[2]} & ${ligneX[3]} & ${ligneX[4]} \\\\
            \\hline
            U (\\text{V}) & ${ligneY[0]} & ${ligneY[1]} & ${ligneY[2]} & ${ligneY[3]} & ${ligneY[4]} \\\\
            \\hline
            \\end{array}
            $<br><br>
            `
            texte += 'Déterminer la valeur de la résistance du conducteur ohmique puis tracer la représentation graphique de la caractéristique $U = f(I)$.'

            // Tracé de la droite
            xA = 0
            yA = f(xA)
            xB = randint(1, 2)// Abscisse de B
            yB = f(xB)// Ordonnée de B

            const A = point(xA, yA, 'A')
            const B = point(xB, yB, 'B')
            droiteAB = droite(A, B)
            droiteAB.color = colorToLatexOrHTML('red')
            droiteAB.epaisseur = 2

            cadre = {
              xMin: min(0, xA - 1, xB - 1),
              yMin: min(0, yA - 1, yB - 1),
              xMax: max(5, xA + 1, xB + 1),
              yMax: max(5, yA + 1, yB + 1)
            }

            cadreFenetreSvg = {
              xmin: cadre.xMin,
              ymin: cadre.yMin,
              xmax: cadre.xMax,
              ymax: cadre.yMax,
              scale: 0.6
            }

            monRepere = repere(cadre)

            tA = tracePoint(A, 'red') // Variable qui trace les points avec une croix
            tB = tracePoint(B, 'red') // Variable qui trace les points avec une croix
            lA = labelPoint(A, 'red')// Variable qui trace les nom s A et B
            lB = labelPoint(B, 'red')// Variable qui trace les nom s A et B

            tA.taille = 5
            tA.epaisseur = 2
            tB.taille = 5
            tB.epaisseur = 2

            texteCorr = `On commence par déterminer la valeur de la résistance du conducteur ohmique : $R = \\cfrac{U}{I} = \\cfrac{${ligneY[1]}}{${ligneX[1]}} = \\cfrac{${ligneY[2]}}{${ligneX[2]}} = \\cfrac{${ligneY[3]}}{${ligneX[3]}} = \\cfrac{${ligneY[4]}}{${ligneX[4]}} = ${a}\\ \\Omega$<br>`
            texteCorr += 'On peut maintenant tracer la représentation graphique de la caractéristique $U = f(I)$. D\'après la loi d\'Ohm, on a : $U = R \\times I$. Le coefficient directeur de la droite est donc $R$.<br>'
            texteCorr += `On place deux points A et B sur le graphique, puis on trace la droite passant par ces deux points : $I_A = ${xA}\\ \\text{A}$, $U_A = ${yA}\\ \\text{V}$ et $I_B = ${xB}\\ \\text{A}$, $U_B = ${a} \\times ${xB} = ${yB}\\ \\text{V}$<br>`

            // @ts-expect-error mathalea2d n'est pas typé
            texteCorr += mathalea2d(cadreFenetreSvg,
              lA, lB, monRepere, droiteAB, tA, tB, textO)
          }
          break
        case 5:
          {
            this.interactifType = 'custom'

            // On choisit aléatoirement les coefficients a et b
            a = 1 / randint(0, 6, [0])
            b = 0
            this.coefficients[i] = [a, b]
            f = (x) => a * x + b

            // Création des listes de valeurs
            const ligneX: number[] = []
            const ligneY: number[] = []
            const pas = randint(1, 4)
            for (let i = 0; i < 5; pas) {
              const x = i * pas
              const y = x * a + b
              ligneX[i] = Math.round(x * 100) / 100
              ligneY[i] = Math.round(y * 100) / 100
              i++
            }

            texte = `$\\def\\arraystretch{1.5}\\begin{array}{|l|c|c|c|c|c|}
            \\hline
            I (\\text{A}) & ${ligneY[0]} & ${ligneY[1]} & ${ligneY[2]} & ${ligneY[3]} & ${ligneY[4]} \\\\
            \\hline
            U (\\text{V}) & ${ligneX[0]} & ${ligneX[1]} & ${ligneX[2]} & ${ligneX[3]} & ${ligneX[4]} \\\\
            \\hline
            \\end{array}
            $<br><br>
            `
            texte += 'Déterminer la valeur de la résistance du conducteur ohmique puis tracer la représentation graphique de la caractéristique $I = f(U)$.'

            // Tracé de la droite
            xA = 0
            yA = f(xA)
            xB = 3// Abscisse de B
            yB = f(xB)// Ordonnée de B

            const A = point(xA, yA, 'A')
            const B = point(xB, yB, 'B')
            droiteAB = droite(A, B)
            droiteAB.color = colorToLatexOrHTML('red')
            droiteAB.epaisseur = 2

            cadre = {
              xMin: min(0, xA - 1, xB - 1),
              yMin: min(0, yA - 1, yB - 1),
              xMax: max(5, xA + 1, xB + 1),
              yMax: max(5, yA + 1, yB + 1)
            }

            cadreFenetreSvg = {
              xmin: cadre.xMin,
              ymin: cadre.yMin,
              xmax: cadre.xMax,
              ymax: cadre.yMax,
              scale: 0.6
            }

            monRepere = repere(cadre)

            tA = tracePoint(A, 'red') // Variable qui trace les points avec une croix
            tB = tracePoint(B, 'red') // Variable qui trace les points avec une croix
            lA = labelPoint(A, 'red')// Variable qui trace les nom s A et B
            lB = labelPoint(B, 'red')// Variable qui trace les nom s A et B

            tA.taille = 5
            tA.epaisseur = 2
            tB.taille = 5
            tB.epaisseur = 2

            texteCorr = `On commence par déterminer la valeur de la résistance du conducteur ohmique : $R = \\cfrac{U}{I} = \\cfrac{${ligneX[1]}}{${ligneY[1]}} = \\cfrac{${ligneX[2]}}{${ligneY[2]}} = \\cfrac{${ligneX[3]}}{${ligneY[3]}} = \\cfrac{${ligneX[4]}}{${ligneY[4]}} = ${1 / a}\\ \\Omega$<br>`
            texteCorr += 'On peut maintenant tracer la représentation graphique de la caractéristique $I = f(U)$. En utilisant la loi d\'Ohm à nouveau, on a : $I = \\cfrac{1}{R} \\times U$. Le coefficient directeur de la droite est donc $\\cfrac{1}{R}$.<br>'
            texteCorr += `On place deux points A et B sur le graphique, puis on trace la droite passant par ces deux points : $U_A = ${xA}\\ \\text{V}$, $I_A = ${yA}\\ \\text{A}$ et $U_B = ${xB}\\ \\text{V}$, $I_B = ${a} \\times ${xB} = ${Math.round(yB * 100) / 100}\\ \\text{A}$<br>`
            // @ts-expect-error mathalea2d n'est pas typé
            texteCorr += mathalea2d(cadreFenetreSvg,
              lA, lB, monRepere, droiteAB, tA, tB, textO)
          }
          break
      }

      // Création de la figure interactive
      if (this.interactif && (this.sup === 4 || this.sup === 5)) {
        const figure = new Figure({ xMin: -2, yMin: -2, width: 330, height: 330, scale: 0.5 })
        this.figures[i] = figure
        figure.setToolbar({ tools: ['POINT', 'LINE', 'DRAG', 'REMOVE'], position: 'top' })
        figure.create('Grid')
        figure.options.color = 'blue'
        figure.options.thickness = 2
        figure.snapGrid = true
        figure.dx = 0.5
        figure.dy = 0.5
        const idApigeom = `apigeomEx${numeroExercice}F${i}`
        texte += figureApigeom({ exercice: this, idApigeom, figure, question: i })
        if (figure.ui) figure.ui.send('LINE')
      }

      if (this.questionJamaisPosee(i, a, b)) {
        // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(texte)
        this.listeCorrections.push(texteCorr)
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }

  correctionInteractive = (i?: number) => {
    if (i === undefined) return 'KO'
    let result: 'OK' | 'KO' = 'KO'
    const figure = this.figures[i]
    if (this.answers == null) this.answers = {}
    this.answers[`#apigeomEx${this.numeroExercice}F${i}`] = figure.json
    figure.isDynamic = false
    figure.divButtons.style.display = 'none'
    figure.divUserMessage.style.display = 'none'
    const lines = [...figure.elements.values()].filter(e => e.type.includes('Line'))
    const [a, b] = this.coefficients[i]
    const point1 = { x: 0, y: b }
    const point2 = { x: 1, y: a + b }
    const { isValid } = figure.checkLine({ point1, point2 })
    const divFeedback = document.querySelector(`#feedbackEx${this.numeroExercice}Q${i}`)
    if (divFeedback != null) {
      if (isValid && lines.length === 1) {
        divFeedback.innerHTML = '😎'
        result = 'OK'
      } else {
        const p = document.createElement('p')
        p.innerText = '☹️'
        if (lines.length === 0) {
          p.innerHTML += ' Aucune droite n\'a été tracée.'
        } else if (lines.length > 1) {
          p.innerHTML += ' Il ne faut tracer qu\'une seule droite.'
        }
        divFeedback.insertBefore(p, divFeedback.firstChild)
      }
    }
    return result
  }
}
