/**
 * 2OS2-9 : Lentille mince convergente
 * @author Thibault Giauffret
 * @author Régis Ferreira Da Silva
 */
import Exercice from '../Exercice'
// Géométrie dynamique
import { Point, point, tracePoint } from '../../lib/2d/points.js'
import { grille } from '../../lib/2d/reperes.js'
import { DemiDroite, demiDroite, segment, Segment } from '../../lib/2d/segmentsVecteurs.js'
import { labelPoint } from '../../lib/2d/textes'
import { mathalea2d } from '../../modules/2dGeneralites.js'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { prenom } from '../../lib/outils/Personne'

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { chiffresSignificatifs } from '../../lib/outils_phys/ecritures'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { createList } from '../../lib/format/lists.js'

export const titre = 'Définir et déterminer géométriquement un grandissement.'
export const interactifReady = true
export const interactifType = 'mathlive'
export const dateDePublication = '20/11/2024'
export const uuid = 'be581'
export const refs = {
  'fr-fr': ['2OS2-9']
}

// Début de l'exercice
export default class Grandissement extends Exercice {
  echelle: number | undefined
  A: Point | undefined
  B: Point | undefined
  C: Point | undefined
  D: Point | undefined
  F: Point | undefined
  Fprime: Point | undefined
  L: Point | undefined
  Aprime: Point | undefined
  Bprime: Point | undefined
  O: Point | undefined
  delta: Point | undefined
  AxeOptique: Segment | undefined
  AxeOptiqueAlt: Segment | undefined
  ABsegment: Segment | undefined
  ABsegmentAlt: Segment | undefined
  Lsegment: Segment | undefined
  LsegmentAlt: Segment | undefined
  BBprime: Segment | undefined
  BBprimeAlt: Segment | undefined
  AAprime: Segment | undefined
  AAprimeAlt: Segment | undefined
  AprimeBprimeSegment: Segment | undefined
  AprimeBprimeSegmentAlt: Segment | undefined
  BC: Segment | undefined
  BCAlt: Segment | undefined
  BD: Segment | undefined
  BDAlt: Segment | undefined
  DBprime: Segment | undefined
  DBprimeAlt: DemiDroite | undefined
  CFprime: DemiDroite | undefined
  CFprimeAlt: DemiDroite | undefined
  CBprimeAlt: DemiDroite | undefined
  To: Point | undefined
  Tf: Point | undefined
  Tc: Point | undefined

  constructor () {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.sup2 = false
    this.sup3 = false
    this.exoCustomResultat = true
    this.besoinFormulaireNumerique = ['Choix des questions', 4, 'À partir des valeurs de AB et A\'B\'\nÀ partir des valeurs de OA et OA\'\nLecture graphique\nDétermination de la taille de l\'image (Thalès)']
  }

  nouvelleVersion () {
    this.figures = []
    this.listeQuestions = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['AB_ABprime']
      this.besoinFormulaire2CaseACocher = false
      this.besoinFormulaire3CaseACocher = false
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['OA_OAprime']
      this.besoinFormulaire2CaseACocher = false
      this.besoinFormulaire3CaseACocher = ['Afficher les triangles semblables (Thalès)']
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['graphique_echelle']
      this.besoinFormulaire2CaseACocher = ['Échelle simple (1 carreau = 1 cm)']
      this.besoinFormulaire3CaseACocher = false
    } else {
      typesDeQuestionsDisponibles = ['taille_image_thales']
      this.besoinFormulaire2CaseACocher = false
      this.besoinFormulaire3CaseACocher = ['Afficher les triangles semblables (Thalès)']
    }

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles || [], this.nbQuestions)

    // On boucle pour générer les questions. cpt est le nombre maximal d'essai pour obtenir des questions différentes.
    for (let i = 0, cpt = 0, fprime, OA, AB; i < this.nbQuestions && cpt < 50;) {
      // On calcule les grandeurs (O est le centre de la lentille, A est l'objet, Aprime est l'image)
      // La figure est centrée en O et est comprise entre -10 et 10 en x
      fprime = randint(30, 50) / 10 // Focale comprise entre 2 et 4
      OA = randint(70, 80) / 10 // Objet à gauche de la lentille
      AB = randint(20, 60) / 10 // Taille de l'objet (fixe pour l'instant, à changer plus tard)
      let OAprime = 1 / (1 / fprime + 1 / -OA) // Relation de conjugaison pour déterminer la position de l'image
      let AprimeBprime = AB * OAprime / OA // Taille de l'image
      let gamma = AprimeBprime / AB // Grandissement
      if (this.sup === 3) {
        // Redéfinition des valeurs pour la question
        fprime = randint(3, 5) // Focale comprise entre 2 et 4
        OA = 9 // Objet à gauche de la lentille
        AB = randint(2, 8) // Taille de l'objet (fixe pour l'instant, à changer plus tard)
        OAprime = 1 / (1 / fprime + 1 / -OA)
        AprimeBprime = AB * OAprime / OA // Taille de l'image
        gamma = OAprime / OA
      }
      this.echelle = randint(2, 6) // Echelle de la figure
      const Xmax = Math.round(OAprime + 7)
      const Xmin = -10
      const Ymax = Math.round(Math.max(AB, AprimeBprime) + 3)
      const Ymin = -Ymax

      // -------------------------------------------------
      //   Figure(s) d'énoncé
      // -------------------------------------------------
      const objetsEnonce = []
      const objetsEnonce2 = []

      // On crée les points
      this.L = point(0, Ymin + 1, '(L)', 'below')
      this.delta = point(Xmax - 1.5, 0, 'Axe optique', 'above left')
      this.A = point(-OA, 0, 'A', 'below left')
      this.B = point(-OA, this.A.y + AB, 'B', 'above left')
      this.Bprime = point(OAprime, -AprimeBprime, 'B\'', 'below left')
      this.O = point(0, 0, 'O', 'above right')
      this.F = point(-fprime, 0, 'F', 'above left')
      this.Fprime = point(fprime, 0, 'F\'', 'above right')

      // On ajoute les libellés de certains points
      this.To = tracePoint(this.A, this.B)
      this.To.style = 'x'
      this.To.epaisseur = 1
      this.To.tailleTikz = 0.3
      this.Tf = tracePoint(this.F, this.Fprime)
      this.Tf.style = '|'
      this.Tf.epaisseur = 2
      this.Tf.taille = 4
      this.Tf.tailleTikz = 0.3

      // On crée les segments et droites
      this.AxeOptique = segment(point(Xmin, 0), point(Xmax, 0), 'black')
      this.AxeOptique.epaisseur = 1.5
      this.AxeOptique.pointilles = '2'

      this.ABsegment = segment(this.A, this.B, 'black')
      this.ABsegment.epaisseur = 2
      this.Lsegment = segment(0, Ymin + 1, 0, Ymax - 1, 'black', '<->')
      this.Lsegment.epaisseur = 2

      // On crée les points et segments pour les rayons lumineux (correction)
      this.C = point(0, this.B.y, 'C', 'above left')
      this.BC = segment(this.B, this.C, 'red')
      this.BC.epaisseur = 1.5
      this.CFprime = demiDroite(this.C, this.Bprime, 'red')
      this.CFprime.epaisseur = 1.5
      this.D = point(0, -AprimeBprime, 'D', 'below right')
      this.Aprime = point(OAprime, 0, 'A\'', 'above right')

      this.BD = segment(this.B, this.D, 'green')
      this.BD.epaisseur = 1.5
      this.DBprime = demiDroite(this.D, this.Bprime, 'green')
      this.DBprime.epaisseur = 1.5
      this.BBprime = demiDroite(this.B, this.Bprime, 'blue')
      this.BBprime.epaisseur = 1.5
      this.AprimeBprimeSegment = segment(this.Aprime, this.Bprime, 'black')
      this.AprimeBprimeSegment.epaisseur = 2

      this.Tc = tracePoint(this.C, this.D, this.Aprime, this.Bprime)

      // On ajoute les objets à la liste utilisée pour générer la figure d'énoncé complète
      objetsEnonce.push(labelPoint(this.A, this.B, this.O, this.F, this.L, this.delta, this.Fprime, this.C, this.D, this.Aprime, this.Bprime), this.To, this.Tf, this.Tc, this.BC, this.CFprime, this.BD, this.DBprime, this.BBprime, this.AprimeBprimeSegment, this.ABsegment, this.Lsegment, this.AxeOptique)
      // Figure d'énoncé simple (Thalès)

      this.AxeOptiqueAlt = segment(point(Xmin, 0), point(Xmax, 0), 'lightgray')
      this.AxeOptiqueAlt.epaisseur = 1.5
      this.AxeOptiqueAlt.pointilles = 2
      this.LsegmentAlt = segment(0, Ymin + 1, 0, Ymax - 1, 'lightgray', '<->')
      this.LsegmentAlt.epaisseur = 2
      this.BBprimeAlt = segment(this.B, this.Bprime, 'black')
      this.BBprimeAlt.epaisseur = 1.5
      this.AAprimeAlt = segment(this.A, this.Aprime, 'black')
      this.AAprimeAlt.epaisseur = 1.5
      this.BCAlt = segment(this.B, this.C, 'lightgray')
      this.BCAlt.epaisseur = 1
      this.CBprimeAlt = demiDroite(this.C, this.Bprime, 'lightgray')
      this.CBprimeAlt.epaisseur = 1
      this.BDAlt = segment(this.B, this.D, 'lightgray')
      this.BDAlt.epaisseur = 1
      this.DBprimeAlt = demiDroite(this.D, this.Bprime, 'lightgray')
      this.DBprimeAlt.epaisseur = 1

      objetsEnonce2.push(labelPoint(this.A, this.B, this.O, this.Aprime, this.Bprime, this.delta), this.ABsegment, this.AprimeBprimeSegment, this.LsegmentAlt, this.BBprimeAlt, this.AxeOptiqueAlt, this.BCAlt, this.CBprimeAlt, this.BDAlt, this.DBprimeAlt, this.AAprimeAlt)
      // Grille
      let sc
      const g = grille(Xmin, Ymin, Xmax, Ymax, 'gray', 0.7)

      objetsEnonce.push(g)

      // -------------------------------------------------
      //   Énoncé
      // -------------------------------------------------
      this.question = ''
      switch (listeTypeDeQuestions[i]) {
        case 'AB_ABprime': {
          if (this.sup2) {
            this.echelle = 1
          }
          this.question = `Lors d'une séance de TP d'optique, ${prenom()} forme l\'image A\'B\' de l\'objet lumineux AB par une lentille convergente.<br> Les valeurs relevées sont :<br><br>`
          const list = createList({
            items: ['$AB = ' + chiffresSignificatifs(AB, 2) + '\\, \\text{cm}$', '$A\'B\' = ' + chiffresSignificatifs(AprimeBprime, 2) + ' \\ \\text{cm}$'],
            style: 'fleches'
          })
          this.question += list
          this.question += '<br>Déterminer le grandissement $\\gamma$ de cette lentille.'
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '<br><br>$\\gamma  =$ ', texteApres: '' })
          handleAnswers(this, i, { reponse: { value: gamma, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
          break
        }
        case 'OA_OAprime': {
          this.question = `Lors d'une séance de TP d'optique, ${prenom()} forme l\'image A\'B\' de l\'objet lumineux AB par une lentille convergente.<br> Les valeurs relevées sont :<br><br>`
          const list2 = createList({
            items: ['$OA = ' + chiffresSignificatifs(OA, 2) + '\\ \\text{cm}$', '$OA\' = ' + chiffresSignificatifs(OAprime, 2) + ' \\ \\text{cm}$'],
            style: 'fleches'
          })
          this.question += list2
          if (this.sup3) {
          // On crée la figure
            this.question += '<br>' + mathalea2d(
              {
                xmin: Xmin,
                ymin: Ymin,
                xmax: Xmax,
                ymax: Ymax,
                pixelsParCm: 20,
                scale: sc
              },
              objetsEnonce2
            )
          }
          this.question += '<br>Déterminer le grandissement $\\gamma$ de cette lentille.'
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '<br><br>$\\gamma  =$ ', texteApres: '' })
          handleAnswers(this, i, { reponse: { value: gamma, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
          break
        }
        case 'graphique_echelle': {
          this.question = `Lors d'une séance de TP d'optique, ${prenom()} forme l\'image A\'B\' de l\'objet lumineux AB par une lentille convergente.`
          if (this.sup2) {
            this.echelle = 1
          }
          this.question += '<br>Déterminer géométriquement le grandissement $\\gamma$.'
          this.question += '<br><br><i>Sur la représentation graphique ci-dessous, un carreau a une largeur de ' + this.echelle + ' cm.</i><br>'
          this.question += '<br>' + mathalea2d(
            {
              xmin: Xmin,
              ymin: Ymin,
              xmax: Xmax,
              ymax: Ymax,
              pixelsParCm: 20,
              scale: sc
            },
            objetsEnonce
          )
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '<br><br>$\\gamma  =$ ', texteApres: '' })
          handleAnswers(this, i, { reponse: { value: gamma, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
          break
        }
        case 'taille_image_thales': {
          this.question = `Lors d'une séance de TP d'optique, ${prenom()} forme l\'image A\'B\' de l\'objet lumineux AB par une lentille convergente. <br>Les valeurs relevées sont :<br><br>`
          const list3 = createList({
            items: ['$AB = ' + chiffresSignificatifs(AB, 2) + '\\ \\text{cm}$', '$OA = ' + chiffresSignificatifs(OA, 2) + '\\ \\text{cm}$', '$OA\' = ' + chiffresSignificatifs(OAprime, 2) + ' \\ \\text{cm}$'],
            style: 'fleches'
          })
          this.question += list3
          if (this.sup3) {
            // On crée la figure
            this.question += '<br>' + mathalea2d(
              {
                xmin: Xmin,
                ymin: Ymin,
                xmax: Xmax,
                ymax: Ymax,
                pixelsParCm: 20,
                scale: sc
              },
              objetsEnonce2
            )
          }
          this.question += '<br>Déterminer la taille de l\'image A\'B\' de l\'objet lumineux AB.'
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '<br><br>$A\'B\'  =$ ', texteApres: ' cm' })
          handleAnswers(this, i, { reponse: { value: AprimeBprime, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
          break
        }
      }

      // -------------------------------------------------
      //   Correction
      // -------------------------------------------------

      // On ajoute les points et les segments à la liste utilisée pour générer la figure de correction

      this.correction = ''
      if (this.sup === 1) {
        this.correction += 'Le grandissement $\\gamma$ correspond au rapport de la taille de l\'image par la taille de l\'objet lumineux. <br> Cela correspond à la formule mathématique suivante  :<br><br>'
        this.correction += '$\\gamma = \\cfrac{A\'B\'}{AB}$<br>'
        this.correction += '<br>En remplaçant par les valeurs numériques (nombres), on obtient :<br>'
        this.correction += '$\\gamma = \\cfrac{' + chiffresSignificatifs(AprimeBprime, 2) + '\\,\\text{cm}}{' + chiffresSignificatifs(AB, 2) + '\\,\\text{cm}} = ' + chiffresSignificatifs(gamma, 2) + '$'
      } else if (this.sup === 2) {
        this.correction += 'Le grandissement $\\gamma$ correspond au rapport de la taille de l\'image par la taille de l\'objet lumineux. <br> Cela correspond à la formule mathématique suivante  :<br><br>'
        this.correction += '$\\gamma = \\cfrac{A\'B\'}{AB}$<br>'
        this.correction += '<br>Or, les valeurs de $A\'B\'$ et $AB$ ne sont pas données dans l\'énoncé. <br><br>'
        this.correction += 'D\'après le théorème de Thalès appliqué aux triangles semblables $OAB$ et $OA\'B\'$, on a :<br><br>'
        this.correction += '<br>' + mathalea2d(
          {
            xmin: Xmin,
            ymin: Ymin,
            xmax: Xmax,
            ymax: Ymax,
            pixelsParCm: 20,
            scale: sc
          },
          objetsEnonce2
        )
        this.correction += '$\\cfrac{A\'B\'}{AB} = \\cfrac{OA\'}{OA}$'
        this.correction += '<br><br>D\'où :<br>'
        this.correction += '$\\gamma =\\cfrac{OA\'}{OA}$'
        this.correction += '<br><br>En remplaçant par les valeurs numériques (nombres), on obtient :<br>'
        this.correction += '$\\gamma = \\cfrac{' + chiffresSignificatifs(OAprime, 2) + '\\,\\text{cm}}{' + chiffresSignificatifs(OA, 2) + '\\,\\text{cm}} = ' + chiffresSignificatifs(gamma, 2) + '$'
      } else if (this.sup === 3) {
        this.correction += 'Le grandissement $\\gamma$ correspond au rapport de la taille de l\'image par la taille de l\'objet lumineux. <br> Cela correspond à la formule mathématique suivante  :<br><br>'
        this.correction += '$\\gamma = \\cfrac{A\'B\'}{AB}$<br><br>'
        this.correction += `Déterminons les valeurs de $A'B'$ et $AB$ en utilisant de l'échelle donnée dans l'énoncé. On mesure graphiquement ${chiffresSignificatifs(AprimeBprime, 2)} carreaux entre $A'$ et $B'$.<br>
       On peut établir le tableau de proportionnalité suivant :<br><br>

        $
        \\def\\arraystretch{1.5}
        \\begin{array}{|c|c|}
        \\hline
        \\text{sur l\'écran} & \\text{en réalité} \\\\ \\hline
        1 \\text{ carreau} & ${chiffresSignificatifs(this.echelle, 2)}\\text{ cm} \\\\
        \\hline
        ${chiffresSignificatifs(AprimeBprime, 2)} \\text{ carreau(x)} & A'B' \\\\
        \\hline
        \\end{array}$
        <br><br>
         En effectuant un produit en croix : $A'B' = \\cfrac{${chiffresSignificatifs(AprimeBprime, 2)} \\, \\text{carreaux} \\times ${chiffresSignificatifs(this.echelle, 2)} \\,\\text{cm} }{1 \\,\\text{carreau} } = ${chiffresSignificatifs(AprimeBprime * this.echelle, 2)} \\, \\text{cm}$.<br>
        
        En faisant de même pour $AB$, on obtient : $AB = ${chiffresSignificatifs(AB * this.echelle, 2)} \\, \\text{cm}$.<br>`
        this.correction += '<br>En remplaçant par les valeurs dans l\'expression littérale du grandissement, on obtient :<br>'
        this.correction += '$\\gamma = \\cfrac{' + chiffresSignificatifs(AprimeBprime * this.echelle, 2) + '\\,\\text{cm}}{' + chiffresSignificatifs(AB * this.echelle, 2) + '\\,\\text{cm}} = ' + chiffresSignificatifs(gamma, 2) + '$'
      } else {
        this.correction += 'Pour déterminer la taille de l\'image A\'B\', on peut utiliser le théorème de Thalès appliqué aux triangles semblables $OAB$ et $OA\'B\'$.<br>'
        this.correction += 'On a :<br>'
        this.correction += '$\\cfrac{A\'B\'}{AB} = \\cfrac{OA\'}{OA}$<br>'
        this.correction += 'En multipliant chaque membre de l\'égalité précédente par $AB$, on obtient $A\'B\' = \\cfrac{OA\'}{OA}\\times AB$<br>'
        this.correction += '<br>En remplaçant par les valeurs numériques (nombres), on obtient :<br>'
        this.correction += '$A\'B\' = \\cfrac{OA\'}{OA} \\times AB$<br>'
        this.correction += '$A\'B\' = \\cfrac{' + chiffresSignificatifs(OAprime, 2) + '\\,\\text{cm}}{' + chiffresSignificatifs(OA, 2) + '\\, \\text{cm}} \\times ' + chiffresSignificatifs(AB, 2) + ' \\, \\text{cm} = ' + chiffresSignificatifs(AprimeBprime, 2) + '\\ \\text{cm}$'
      }

      //   Si l'exercice généré est unique (i.e. la valeur de this.reponse n'est jamais sortie), alors on l'ajoute à la liste
      if (this.questionJamaisPosee(i, fprime, OA, AB)) {
        // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(this.question)
        this.listeCorrections.push(this.correction)
        i++
      }
      cpt++
    }

    // On convertit les champs de texte générés en contenu
    listeQuestionsToContenu(this)
  }
}
