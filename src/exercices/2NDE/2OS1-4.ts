/**
 * 2OS1-4.ts
 * @author Régis Ferreira Da Silva
 * @author Thibault Giauffret
 * @date 2024-10-18
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { chiffresSignificatifs, ecritureScientifique } from '../../lib/outils_phys/ecritures'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { prenom } from '../../lib/outils/Personne'
import { fixeBordures, mathalea2d } from '../../modules/2dGeneralites.js'
import { courbe } from '../../lib/2d/courbes.js'
import { repere } from '../../lib/2d/reperes.js'

// à remplacer
export const titre = 'Déterminer la période et la fréquence d’un signal sonore notamment à partir de sa représentation temporelle'
export const interactifReady = true
export const interactifType = 'mathLive'
// à remplacer
export const dateDePublication = '31/10/2024'
// à remplacer : pnpm getNewUuid pour récupérer l'UUID pour un nouvel exercice
export const uuid = 'fe5fa'
// à remplacer "test-2" par la référence dans le référentiel (disponible dans "src/json/allExercice.json")
export const refs = {
  'fr-fr': ['2OS1-4']
}

export default class Periodefrequence extends Exercice {
  listeTaches: string[]

  constructor () {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.sup2 = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 3, "1 : Novice (Pas de conversion d'unité)\n2 :Confirmé (Conversion d'unité)\n3 : Expert"]
    this.besoinFormulaire2Numerique = ['\n\nTâche', 2, '1 : Mesure de période\n2 : Calcul de fréquence']
    this.listeTaches = []
  }

  nouvelleVersion () {
    this.listeQuestions = []
    this.listeTaches = []
    this.listeCorrections = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['novice']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['confirme']
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['expert']
      // Cacher le formulaire besoinFormulaire2Numerique ???
    }

    let sousChoix
    if (this.sup2 === 1) {
      sousChoix = ['periode'] // pour choisir aléatoirement des questions dans chaque catégorie
    } else if (this.sup2 === 2) {
      sousChoix = ['frequence']
    }

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles || [], this.nbQuestions)
    const listeTaches = combinaisonListes(sousChoix || [], this.nbQuestions)

    for (let i = 0, cpt = 0, Tms; i < this.nbQuestions && cpt < 50;) {
      Tms = randint(10, 99) / 10 // Période en ms
      const Tn = randint(10, 99) / 10 // Période en s (novice)
      const T = Tms / 1000 // Période en s
      const fn = 1 / Tn // Fréquence en Hz (novice)
      const f = 1 / T // Fréquence en Hz
      const fg = 1 / T / 1000 // Fréquence en Hz graphique
      const A = randint(1, 10) // Amplitude
      const phase = Math.random() * 2 * Math.PI // Phase
      let F: (x: number) => number
      const nbcs = 2 // Nombre de chiffres significatifs

      // ------------------------
      // Préparation du graphique
      // ------------------------
      // Limites du graphique
      const fixedWidth = 10 // Nombre de graduations
      const fixedHeight = 10 // Nombre de graduations
      const Xmin = -1
      const Xmax = fixedWidth
      const Ymin = -A - 1
      const Ymax = A + 1

      // Détermination des valeurs de graduation (ticks)
      // const xTickDistance = Math.round((approxXmax - Xmin) / fixedWidth)
      const yTickDistance = (Ymax - Ymin) / fixedHeight
      // const Xmax = xTickDistance * fixedWidth

      switch (listeTypeDeQuestions[i]) {
        case 'novice':
          switch (listeTaches[i]) {
            case 'periode':
              this.question = `En TP, ${prenom()} a effectué l'acquisition d'un signal sonore dont la représentation graphique est la suivante :<br>`

              // Tirage au sort de la fonction et initialisation de la courbe
              F = this.fonctionAleatoire(A, fn, 0)
              this.question += this.creerGraphique({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F })

              // On énonce la question
              this.question += 'Par lecture graphique, mesurer la période, $T$, de ce signal arrondie à la dixième de seconde près.<br><br>'
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$T = $', texteApres: 's' })
              //   On génère la réponse
              this.reponse = Tn
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
              handleAnswers(this, i, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare, options: { tolerance: 1e-2 } } })// options: { tolerance: 1e-1 }
              //   On écrit la correction
              this.correction = 'On commence par identifier un motif élémentaire (par exemple ici en rouge) :<br>'
              this.correction += this.creerGraphique({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F, xLegende: 't (s)', afficheMotif: Tn })
              this.correction += `La période correspondant à la durée d'un motif élémentaire, on en déduit la mesure de la période : $T=${this.reponse}\\ \\text{s}$.`
              // fin question
              break
            case 'frequence':
              this.question = `En TP, ${prenom()} a effectué l'acquisition d'un signal sonore dont la période est de $T =${Tn}$ s.<br>`
              // On énonce la question
              this.question += 'En déduire la fréquence de ce signal arrondie au centième de Hertz.<br><br>'
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$f = $', texteApres: 'Hz' })
              //   On génère la réponse
              this.reponse = Math.round(fn * 100) / 100
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de f à la valeur attendue
              handleAnswers(this, i, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare, options: { tolerance: 1e-3 } } })// Math.round n'y fait rien
              // options: { tolerance: 1e-2 }
              //   On écrit la correction
              this.correction = `La situation rencontrée peut être ramenée à une situation de proportionnalité : <br><br>
              $\\def\\arraystretch{1.5}
              \\begin{array}{|c|c|}
              \\hline
              \\text{Nombre de motifs observés} & \\text{Durée} \\\\
              \\hline
              1 & ${Tn}\\ \\text{s}\\\\
              \\hline
              f&1\\ \\text{s} \\\\
              \\hline
              \\end{array}
              $<br><br>
              
              Soit en effectuant le produit en croix, $f\\times ${Tn}\\ \\text{s}=1$. <br>
              Ce qui revient à $f=\\cfrac{1}{${Tn}\\ \\text{s}}=${chiffresSignificatifs(fn, nbcs)}\\ \\text{Hz}$.<br>              
              La fréquence de ce signal est donc $f= ${chiffresSignificatifs(fn, nbcs)}\\ \\text{Hz}$.`
              // fin question
              break
          }
          break
        case 'confirme':
          switch (listeTaches[i]) {
            case 'periode':
              this.question = `En TP, ${prenom()} a effectué l'acquisition d'un signal sonore dont la représentation graphique est la suivante :<br> `// axe des abscisses en ms
              // On énonce la question
              // Tirage au sort de la fonction et initialisation de la courbe
              F = this.fonctionAleatoire(A, fg, 0)
              this.question += this.creerGraphique({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F, xLegende: 't (ms)' })
              this.question += 'Par lecture graphique, mesurer puis convertir la période de ce signal au dixième de millième de seconde près.<br><br>'
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$T = $', texteApres: 's' })
              //   On génère la réponse
              this.reponse = Math.round(T * 10000) / 10000
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de Tms
              handleAnswers(this, i, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare, options: { tolerance: 1e-5 } } })// options: { tolerance: 1e-1 }
              //   On écrit la correction
              this.correction = 'On commence par identifier un motif élémentaire (par exemple ici en rouge) :<br>'
              this.correction += this.creerGraphique({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F, xLegende: 't (ms)', afficheMotif: Tms })
              this.correction += `La période correspondant à la durée d'un motif élémentaire, on en déduit la mesure de la période : $T=${chiffresSignificatifs(Tms, nbcs)}\\ \\text{ms}$ soit $T=${ecritureScientifique(T, nbcs)}\\ \\text{s}$.`
              // fin question
              break
            case 'frequence':
              this.question = `En TP, ${prenom()} a effectué l'acquisition d'un signal sonore dont la période est de $T =${chiffresSignificatifs(Tms, nbcs)}$ ms.<br>`
              // On énonce la question
              this.question += 'En déduire la fréquence de ce signal arrondie à la dizaine de Hertz.<br><br>'
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$f = $', texteApres: 'Hz' })
              //   On génère la réponse
              this.reponse = Math.round(f / 10) * 10
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de f à la valeur attendue
              handleAnswers(this, i, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare } })// Math.round n'y fait rien
              // options: { tolerance: 1e-2 }
              //   On écrit la correction
              this.correction = `Afin de déterminer la fréquence, on utilise la formule $f=\\cfrac{1}{T}$. <br>
              En effectuant l'application numérique, on trouve $f=\\cfrac{1}{${chiffresSignificatifs(Tms, nbcs)}\\ \\text{ms}}=\\cfrac{1}{${chiffresSignificatifs(T, nbcs)}\\ \\text{s}}=${ecritureScientifique(f, nbcs)}\\ \\text{Hz}$.`
              // fin question
              break
          }
          break
        case 'expert':
          this.question = `En TP, ${prenom()} a effectué l'acquisition d'un signal sonore dont la représentation graphique est la suivante :<br>`
          // On énonce la question
          // Tirage au sort de la fonction et initialisation de la courbe
          F = this.fonctionAleatoire(A, fg, phase)
          this.question += this.creerGraphique({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F, xLegende: 't (ms)' })
          this.question += 'Déterminer la fréquence de ce signal sonore arrondie à la dizaine de Hertz.<br><br>'
          //   On y ajoute le champ de réponse
          this.question += ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: '$f = $', texteApres: 'Hz' })
          //   On génère la réponse
          this.reponse = Math.round(f / 10) * 10
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
          handleAnswers(this, i, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare } })// options: { tolerance: 1e-1 }
          //   On écrit la correction
          this.correction = 'On commence par identifier un motif élémentaire (par exemple ici en rouge) :<br>'
          this.correction += this.creerGraphique({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F, xLegende: 't (ms)', afficheMotif: Tms })
          this.correction += `La période correspondant à la durée d'un motif élémentaire, on en déduit la mesure de la période : $T= ${chiffresSignificatifs(Tms, nbcs)}\\ \\text{ms}$ soit $T=${chiffresSignificatifs(T, nbcs)}\\ \\text{s}$. <br> 
          Afin de déterminer la fréquence, on utilise la formule $f=\\cfrac{1}{T}$. <br>
          En effectuant l'application numérique, on trouve $f=\\cfrac{1}{${chiffresSignificatifs(Tms, nbcs)}\\ \\text{ms}}=\\cfrac{1}{${chiffresSignificatifs(T, nbcs)}\\ \\text{s}}=${ecritureScientifique(f, nbcs)}\\ \\text{Hz}$.`
          // fin question
          break
      }

      if (this.questionJamaisPosee(i, T!)) { // Si la question n'a jamais été posée, on en créé une autre, tgL ne peut pas prendre la même valeur
        this.listeQuestions.push(this.question!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(this.correction!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }
    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }

  // Sélection aléatoire entre cinq fonctions
  fonctionAleatoire (A: number, f: number, phase: number) {
    const randomIndex = Math.floor(Math.random() * 5)
    switch (randomIndex) {
      case 0: {
        return (x: number) => A * Math.sin(2 * Math.PI * f * x + phase)
      }
      case 1: {
        return (x: number) => A / 2 * Math.sin(2 * Math.PI * f * x + phase) + A / 4 * Math.sin(2 * Math.PI * 2 * f * x + phase)
      }
      case 2: {
        return (x: number) => A / 3 * Math.sin(2 * Math.PI * f * x + phase) + A / 4 * Math.sin(2 * Math.PI * 2 * f * x + phase) + A / 6 * Math.sin(2 * Math.PI * 3 * f * x + phase)
      }
      case 3: {
        return (x: number) => A * Math.cos(2 * Math.PI * f * x + phase)
      }
      case 4: {
        return (x: number) => A / 2 * Math.cos(2 * Math.PI * f * x + phase) + A / 4 * Math.cos(2 * Math.PI * 3 * f * x + phase)
      }
      default: {
        return (x: number) => A * Math.sin(2 * Math.PI * f * x + phase)
      }
    }
  }

  // Fonction pour créer le graphique
  creerGraphique ({ Xmin, Xmax, Ymin, Ymax, yTickDistance, F, xLegende = 't (s)', yLegende = 'Amplitude  (U.A.)', afficheMotif }: { Xmin: number, Xmax: number, Ymin: number, Ymax: number, yTickDistance: number, F: (x: number) => number, xLegende?: string, yLegende?: string, afficheMotif?: number }) {
    // Graphique
    const r = repere({
      xMin: Xmin,
      xMax: Xmax,
      xUnite: 1,
      xThickDistance: 1, // Chez MathAlea, ils appellent ça "thick" mais c'est "tick"...
      xLegende,
      yMin: Ymin,
      yMax: Ymax,
      yUnite: 1 / yTickDistance,
      yThickDistance: yTickDistance, // Chez MathAlea, ils appellent ça "thick" mais c'est "tick"...
      yLegende,
      xLegendePosition: [1.12 * Xmax, -0.5],
      yLegendePosition: [1.7, Ymax * (1 / yTickDistance) + 0.5],
      grilleSecondaire: true,
      xLabelListe: Array.from({ length: Xmax - Xmin + 1 }, (_, i) => i),
      axesEpaisseur: 1.5
    })
    const courbe2 = courbe(F, { repere: r, xMin: Xmin, xMax: Xmax, color: 'blue', epaisseur: 1.2, step: 0.01 })
    if (afficheMotif) {
      const motif = courbe(F, { repere: r, xMin: 0, xMax: afficheMotif, color: 'red', epaisseur: 1.2, step: 0.01 })
      return mathalea2d(Object.assign({
        pixelsParCm: 35,
        scale: 1,
        style: 'display: block'
      }, fixeBordures(r, {
        rxmin: 6
      }
      )), r, courbe2, motif)
    } else {
      // Création du graphique
      return mathalea2d(Object.assign({
        pixelsParCm: 35,
        scale: 1,
        style: 'display: block'
      }, fixeBordures(r, {
        rxmin: 6
      })), r, courbe2)
    }
  }
}
