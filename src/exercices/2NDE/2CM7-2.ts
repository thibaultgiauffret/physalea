/**
 * 2CM7-2.ts
 * @author Régis Ferreira Da Silva
 * @author Thibault Giauffret
 * @date 2025-01-10
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare, fonctionComparaison } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { prenom } from '../../lib/outils/Personne'
import { ecritureScientifique, chiffresSignificatifs } from '../../lib/outils_phys/ecritures'
//import { m } from 'vitest/dist/reporters-yx5ZTtEV.js'

// à remplacer
export const titre = "Déterminer le nombre d’entités et la quantité de matière (en mol) d’une espèce dans une masse d’échantillon."
export const interactifReady = true
export const interactifType = 'mathLive'
// à remplacer
export const dateDePublication = '28/01/2025'
// à remplacer : pnpm getNewUuid pour récupérer l'UUID pour un nouvel exercice
export const uuid = 'e91e5'
// à remplacer "test-2" par la référence dans le référentiel (disponible dans "src/json/allExercice.json")
export const refs = {
  'fr-fr': ['2CM7-2']
}

export default class QuantiteMatiere extends Exercice {
  listeTaches: string[]

  constructor() {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.sup2 = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 3, "1 : Novice (avec questions intermédiaires)\n2 :Confirmé (sans question intermédiaire)"]
    this.besoinFormulaire2Numerique = ['\n\nTâche', 2, '1 : Nombre entité\n2 : Quantité de matière']
    this.listeTaches = []
  }

  nouvelleVersion() {
    this.listeQuestions = []
    this.listeTaches = []
    this.listeCorrections = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['novice']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['confirme']
    }

    let sousChoix
    if (this.sup2 === 1) {
      sousChoix = ['N'] // pour choisir aléatoirement des questions dans chaque catégorie
    } else if (this.sup2 === 2) {
      sousChoix = ['n']
    }

    const listeTypeDeQuestions = combinaisonListes(
      typesDeQuestionsDisponibles || [],
      this.nbQuestions
    )
    const listeTaches = combinaisonListes(sousChoix || [], this.nbQuestions)

    for (let i = 0, cpt = 0, m; i < this.nbQuestions && cpt < 50;) {
      m = randint(200, 500)
      const nElement = randint(0, 9)// choix de l'élément chimique
      const ElementList = ['C', 'H', 'O', 'Na', 'Cu', 'S', 'Cl', 'Fe', 'N']//liste symbole des atomes
      const NomElementList = ['carbone', 'hydrogène', 'oxygène', 'sodium', 'cuivre', 'soufre', 'chlore', 'fer', 'azote'] //liste nom des atomes
      const masseElementList = [1.99e-23, 1.66e-24, 2.66e-23, 3.82e-23, 1.05e-22, 5.33e-23, 5.90e-23, 9.27e-23, 2.33e-23] //liste masses atome
      const massemolaireElementList = [12.0, 1.0, 16.0, 23.0, 63.5, 32.1, 35.5, 55.8, 14]//liste masses molaires
      const N = m / masseElementList[nElement]
      const n = m / massemolaireElementList[nElement]
      const nbcs = 2 // Nombre de chiffres significatifs
      const tolerance = Math.pow(-nbcs)

      switch (listeTypeDeQuestions[i]) {
        case 'novice':
          switch (listeTaches[i]) {
            case 'N':
              // On énonce la question
              this.question = `${prenom()} dispose d'un échantillon de masse ${m} grammes de ${NomElementList[nElement]}.<br><br>`
              this.question += `<b>a.</b> Rappeler la formule reliant la masse d'une entité, $m_{\\text{entité}}$, la masse de l'échantillon, $m_{\\text{échantillon}}$, et le nombre d'entité, $N$.<br>`
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, 2 * i, 'inline largeur01', { texteAvant: '$N = $', texteApres: '<br><br>' })
              //   On génère la réponse
              this.reponse = `$\\dfrac{1\\times m_{\\text{échantillon}}}{m_{\\text{entité}}}`
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
              handleAnswers(this, 2 * i, { reponse: { value: this.reponse, compare: fonctionComparaison } })
              //   On écrit la correction
              this.correction = `<b>a.</b> La situation rencontrée peut être ramenée à une situation de proportionnalité : <br><br>
              $\\def\\arraystretch{1.5}
              \\begin{array}{|c|c|}
              \\hline
              \\text{Nombre d'entités} & \\text{Masse} \\\\
              \\hline
              1 & m_{\\text{entité}}\\\\
              \\hline
              N&m_{\\text{échantillon}} \\\\
              \\hline
              \\end{array}
              $<br><br>

              En effectuant un produit en croix, nous pouvons écrire l'égalité suivante :<br><br>

              $N\\times m_{\\text{entité}}=1\\times m_{\\text{échantillon}}$ <br><br>

              Pour connaître l'expression de $N$, il suffit de diviser l'égalité précédente par $m_{\\text{entité}}$ :<br><br>

              $\\dfrac{N\\times m_{\\text{entité}}}{m_{\\text{entité}}}=\\dfrac{1\\times m_{\\text{échantillon}}}{m_{\\text{entité}}}$ <br><br>

              Ce qui revient à écrire l'égalité :<br><br>

              $N=\\dfrac{m_{\\text{échantillon}}}{m_{\\text{entité}}}$<br><br>
              `
              // fin question
              // On énonce la question
              this.question += `<b>b.</b> Sachant qu'un atome de ${NomElementList[nElement]} a une masse de $${ecritureScientifique(masseElementList[nElement])}$ g, en déduire le nombre d'atomes de ${NomElementList[nElement]}, $N_{\\text{${ElementList[nElement]}}}$, dans cet échantillon .<br>`
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, 2 * i + 1, 'inline largeur01', { texteAvant: `$N_{\\text{${ElementList[nElement]}}} = $`, texteApres: `atomes de ${NomElementList[nElement]}.` })
              //   On génère la réponse
              this.reponse = N
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
              handleAnswers(this, 2 * i + 1, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare }, options: { tolerance: tolerance } })
              //   On écrit la correction
              this.correction += `<b>b.</b> En effectuant l'application numérique à l'aide de la relation précédente, on obtient : <br><br>

              $N_{\\text{${ElementList[nElement]}}}=\\dfrac{${m} \\,\\text{g}}{${ecritureScientifique(masseElementList[nElement])}\\,\\text{g}}=${ecritureScientifique(N, nbcs)}$<br>
              L'échantillon contient donc $${ecritureScientifique(N, nbcs)}$ atomes de ${NomElementList[nElement]}.`
              break
            case 'n':
              // On énonce la question
              this.question = `${prenom()} dispose d'un échantillon de masse ${m} grammes de ${NomElementList[nElement]}.<br><br>`
              this.question += `<b>a.</b> Rappeler la formule reliant la masse d'une mole d'entité, $m_{\\text{mole}}$, la masse de l'échantillon, $m_{\\text{échantillon}}$, et la quantité de matière de cette entité, $n$.<br>`
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, 2 * i, 'inline largeur01', { texteAvant: '$n = $', texteApres: '<br><br>' })
              //   On génère la réponse
              this.reponse = `$\\dfrac{1\\times m_{\\text{échantillon}}}{m_{\\text{entité}}}`
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
              handleAnswers(this, 2 * i, { reponse: { value: this.reponse, compare: fonctionComparaison } })
              //   On écrit la correction
              this.correction = `<b>a.</b> La situation rencontrée peut être ramenée à une situation de proportionnalité : <br><br>
              $\\def\\arraystretch{1.5}
              \\begin{array}{|c|c|}
              \\hline
              \\text{Quantité de matière} & \\text{Masse} \\\\
              \\hline
              1\\,\\text{mol}& m_{\\text{mole}}\\\\
              \\hline
              n&m_{\\text{échantillon}} \\\\
              \\hline
              \\end{array}
              $<br><br>
              En effectuant un produit en croix, nous pouvons écrire l'égalité suivante :<br><br>

              $N\\times m_{\\text{mole}}=1 \\text{mol} \\times m_{\\text{échantillon}}$ <br><br>

              Pour connaître l'expression de $n$, il suffit de diviser l'égalité précédente par $m_{\\text{mole}}$ :<br><br>

              $\\dfrac{N\\times m_{\\text{mole}}}{m_{\\text{entité}}}=\\dfrac{1\\text{mol}\\times m_{\\text{échantillon}}}{m_{\\text{mole}}}$ <br><br>

              Ce qui revient à écrire l'égalité :<br><br>

              $N=\\dfrac{m_{\\text{échantillon}}}{m_{\\text{mole}}}\\times 1\\text{mol}$<br><br>
              `
              // fin question
              // On énonce la question
              this.question += `<b>b.</b> Sachant qu'une mole d'atomes de ${NomElementList[nElement]} a une masse de ${massemolaireElementList[nElement]} g, en déduire la quantité de matière d'atomes de ${NomElementList[nElement]}, $n_{\\text{${ElementList[nElement]}}}$, dans cet échantillon .<br>`
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, 2 * i + 1, 'inline largeur01', { texteAvant: `$n_{\\text{${ElementList[nElement]}}} = $`, texteApres: `moles de ${NomElementList[nElement]}.` })
              //   On génère la réponse
              this.reponse = n
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
              handleAnswers(this, 2 * i + 1, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare }, options: { tolerance: tolerance } })
              //   On écrit la correction
              this.correction += `<b>b.</b> En effectuant l'application numérique à l'aide de la relation précédente, on obtient : <br><br>

              $N_{\\text{${ElementList[nElement]}}}=\\dfrac{${m} \\,\\text{g}}{${massemolaireElementList[nElement]} \\,\\text{g}}\\times \\text{1 mol}=${ecritureScientifique(n, nbcs)}\\,\\text{mol}$<br><br>

              L'échantillon contient donc $${ecritureScientifique(n, nbcs)}$ moles d'atomes de ${NomElementList[nElement]}.`
              break
          }
          break
        case 'confirme':
          switch (listeTaches[i]) {
            case 'N':
              // On énonce la question
              this.question = `${prenom()} dispose d'un échantillon de masse ${m} grammes de ${NomElementList[nElement]}.<br><br>`
              // On énonce la question
              this.question += `Sachant qu'un atome de ${NomElementList[nElement]} a une masse de $${ecritureScientifique(masseElementList[nElement])}$ g, en déduire le nombre d'atomes de ${NomElementList[nElement]}, $N_{\\text{${ElementList[nElement]}}}$, dans cet échantillon .<br>`
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, 2 * i + 1, 'inline largeur01', { texteAvant: `$N_{\\text{${ElementList[nElement]}}} = $`, texteApres: `atomes de ${NomElementList[nElement]}.` })
              //   On génère la réponse
              this.reponse = N
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
              handleAnswers(this, 2 * i + 1, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare }, options: { tolerance: tolerance } })
              //   On écrit la correction
              this.correction = `En effectuant l'application numérique à l'aide de la relation :  $N=\\frac{m_{\\text{échantillon}}}{m_{\\text{entité}}}$, on obtient : <br><br>
              $N_{\\text{${ElementList[nElement]}}}=\\dfrac{${m} \\,\\text{g}}{${ecritureScientifique(masseElementList[nElement])} \\,\\text{g}}=${ecritureScientifique(N, nbcs)}$<br>
              L'échantillon contient donc $${ecritureScientifique(N, nbcs)}$ atomes de ${NomElementList[nElement]}.`
              break
            case 'n':
              // On énonce la question
              this.question = `${prenom()} dispose d'un échantillon de masse ${m} grammes de ${NomElementList[nElement]}.<br><br>`
              // On énonce la question
              this.question += `Sachant qu'une mole d'atomes de ${NomElementList[nElement]} a une masse de ${massemolaireElementList[nElement]} g, en déduire la quantité de matière d'atomes de ${NomElementList[nElement]}, $n_{\\text{${ElementList[nElement]}}}$, dans cet échantillon .<br>`
              //   On y ajoute le champ de réponse
              this.question += ajouteChampTexteMathLive(this, 2 * i + 1, 'inline largeur01', { texteAvant: `$N_{\\text{${ElementList[nElement]}}} = $`, texteApres: `moles de ${NomElementList[nElement]}.` })
              //   On génère la réponse
              this.reponse = n
              //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur donnée avec une tolérance
              handleAnswers(this, 2 * i + 1, { reponse: { value: this.reponse.toString(), compare: approximatelyCompare }, options: { tolerance: tolerance } })
              //   On écrit la correction
              this.correction = `En effectuant l'application numérique à l'aide de la relation : $n=\\frac{1\\times m_{\\text{échantillon}}}{m_{\\text{entité}}}$, on obtient : <br><br>
              $n_{\\text{${ElementList[nElement]}}}=\\dfrac{${m} \\,\\text{g}}{${massemolaireElementList[nElement]} \\,\\text{g}}\\times \\text{1 mol}=${ecritureScientifique(n, nbcs)}\\,\\text{mol}$<br>
              L'échantillon contient donc $${ecritureScientifique(n, nbcs)}$ moles d'atomes de ${NomElementList[nElement]}.`
              break
          }
      }

      if (this.questionJamaisPosee(i, m!)) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(this.question!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(this.correction!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }
    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
