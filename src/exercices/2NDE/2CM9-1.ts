import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
export const titre = 'Modéliser, à partir de données expérimentales, une transformation par une réaction, établir l’équation de réaction associée et l’ajuster.'
export const interactifReady = true
export const interactifType = 'mathLive'

/**
* @author Thibault Giauffret
* 2CM9-1.ts
*/

export const uuid = 'e59b4'
export const ref = '2CM9-1'
export const refs = {
  'fr-fr': ['2CM9-1'],
  'fr-ch': ['']
}

export default class equationReaction extends Exercice {
  constructor () {
    super()
    this.consigne = ''
    this.nbQuestions = 1
    this.tailleDiaporama = 3
    this.sup = 1
    this.besoinFormulaireNumerique = ['Types de question ', 1, '1 : Alcanes (1 inconnue)\n2 : Alcools (1 inconnue)\n3 : Alcools ou alcanes (2 inconnues)']
  }

  nouvelleVersion () {
    this.sup = parseInt(this.sup)
    this.listeQuestions = []
    this.listeCorrections = []
    let typesDeQuestionsDisponibles: (1 | 2 | 3)[] = []
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = [1]
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = [2]
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = [3]
    }
    // else if (this.sup === 4) {
    //   typesDeQuestionsDisponibles = [4]
    // }
    //  else if (this.sup === 5) {
    //   typesDeQuestionsDisponibles = [5]
    // }
    let texte: string = ''
    let values: { texte: string, texteCorr: string, isInteger: boolean } = { texte: '', texteCorr: '', isInteger: false }
    let texteCorr: string = ''
    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles, this.nbQuestions)

    for (let i = 0, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      switch (listeTypeDeQuestions[i]) {
        case 1:
          values = this.transfoAlcane(i, true)
          if (values.isInteger) {
            texte = values.texte
            texteCorr = values.texteCorr
          } else {
            continue
          }
          break
        case 2:
          values = this.transfoAlcool(i)
          texte = values.texte
          texteCorr = values.texteCorr
          break
        case 3:
          if (Math.random() < 0.5) {
            values = this.transfoAlcane(i, false, 2)
          } else {
            values = this.transfoAlcool(i, 2)
          }
          texte = values.texte
          texteCorr = values.texteCorr
          break
        // case 4:
        //   if (Math.random() < 0.5) {
        //     values = this.transfoAlcane(i, false, 4)
        //   } else {
        //     values = this.transfoAlcool(i, 4)
        //   }
        //   texte = values.texte
        //   texteCorr = values.texteCorr
        //   break
      }

      if (this.questionJamaisPosee(i, texte)) {
        // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(texte)
        this.listeCorrections.push(texteCorr)
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }

  transfoAlcane (i: number, int = false, j = 1) {
    const nomAlcane = ['du méthane', 'de l\'éthane', 'du propane', 'du butane', 'du pentane', 'de l\'hexane', 'de l\'heptane', 'de l\'octane']
    const coeffAlcane = randint(1, 3)
    const x = randint(1, 8)
    const y = 2 * x + 2
    const coeffO2 = ((3 * x + 1) / 2) * coeffAlcane
    const coeffCO2 = x * coeffAlcane
    const coeffH2O = (x + 1) * coeffAlcane
    const coeffsNum = [coeffAlcane, coeffO2, coeffCO2, coeffH2O]

    if (int) {
      if (coeffsNum[1] % 1 !== 0 || coeffsNum[2] % 1 !== 0 || coeffsNum[3] % 1 !== 0) {
        return { texte: '', texteCorr: '', isInteger: false }
      }
    }

    let a = ''
    let b = ''
    let c = ''
    let d = ''

    a = coeffsNum[0].toString()
    if (coeffsNum[1] % 1 === 0) {
      b = coeffsNum[1].toString()
    } else {
      b = `\\frac{${(3 * x + 1) * coeffAlcane}}{2}`
    }
    c = coeffsNum[2].toString()
    d = coeffsNum[3].toString()
    const coeffs =
      [
        {
          id: 'a',
          value: coeffsNum[0],
          text: a,
          result: a,
          hidden: false
        },
        {
          id: 'b',
          value: coeffsNum[1],
          text: b,
          result: b,
          hidden: false
        },
        {
          id: 'c',
          value: coeffsNum[2],
          text: c,
          result: c,
          hidden: false
        },
        {
          id: 'd',
          value: coeffsNum[3],
          text: d,
          result: d,
          hidden: false
        }
      ]
    const resultats = []

    let count = 0
    while (count < j) {
      const r = randint(0, coeffs.length - 1, resultats)
      coeffs[r].hidden = true
      coeffs[r].text = coeffs[r].id
      resultats.push(r)
      count++
    }

    const coeff = [coeffs[0].text, coeffs[1].text, coeffs[2].text, coeffs[3].text]
    for (let k = 0; k < coeff.length; k++) {
      if (coeff[k] === '1') {
        coeff[k] = ''
      }
    }
    let equation = ''
    if (x === 1) {
      equation = `\\[${coeff[0]}\\ \\text{CH}_{${y}} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    } else {
      equation = `\\[${coeff[0]}\\ \\text{C}_${x}\\text{H}_{${y}} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    }

    const alcane = nomAlcane[x - 1]

    let texte = 'Soit l\'équation de combustion complète ' + alcane + ' : '
    texte += equation
    texte += '<br>Déterminer le (ou les) nombre(s) stoechiométrique(s) manquant(s) :<br><br>'
    let resultsCounter = 0
    for (let k = 0; k < coeffs.length; k++) {
      if (coeffs[k].hidden) {
        handleAnswers(this, j * i + resultsCounter, { reponse: { value: coeffs[k].value, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
        texte += ajouteChampTexteMathLive(this, j * i + resultsCounter, 'inline largeur01 ', { texteAvant: '$' + coeffs[k].id + ' = $' })
        texte += '<br><br>'

        resultsCounter++
      }
    }

    let texteCorr = ''

    if (j === 1) {
      const index = resultats[0]
      const resultat = coeffs[index].result
      const lettre = coeffs[index].id
      if (index === 0 || index === 2) {
        texteCorr = `Dans les réactifs, seule la molécule de $\\text{C}_${x}\\text{H}_{${y}}$ contient du carbone (par paquet de ${x}). Dans les produits, le dioxyde de carbone $\\text{CO}_2$ contient du carbone (par paquet de 1).<br><br>Afin d'équilibrer l'équation bilan, il est nécéssaire qu'il y ait autant d'élément carbone ($\\text{C}$) dans les réactifs que dans les produits. On peut traduire cela par l'équation suivante :<br><br>
              $${coeffs[0].text} \\times ${x} = ${coeffs[2].text} \\times 1$<br><br>
              soit :<br><br>
              $${lettre} = ${resultat}$`
      } else if (index === 1) {
        texteCorr = `Dans les réactifs, seules les molécules de $\\text{O}_2$ contiennent de l'oxygène (par paquet de 2). Dans les produits, le dioxyde de carbone $\\text{CO}_2$ contient de l'oxygène (par paquet de 2) et l'eau $\\text{H}_2\\text{O}$ contient de l'oxygène (par paquet de 1).<br><br>Afin d'équilibrer l'équation bilan, il est nécéssaire qu'il y ait autant d'élément oxygène ($\\text{O}$) dans les réactifs que dans les produits. On peut traduire cela par l'équation suivante :<br><br>
              $${coeffs[0].text} \\times 0 + ${coeffs[1].text} \\times 2 = ${coeffs[2].text} \\times 2 + ${coeffs[3].text} \\times 1$<br><br>
              soit :<br><br>
              $${lettre} = ${resultat}$`
      } else if (index === 3) {
        texteCorr = `Dans les réactifs, seule la molécule de $\\text{C}_${x}\\text{H}_{${y}}$ contient de l'hydrogène (par paquet de ${y}). Dans les produits, l'eau $\\text{H}_2\\text{O}$ contient de l'hydrogène (par paquet de 2).<br><br>Afin d'équilibrer l'équation bilan, il est nécéssaire qu'il y ait autant d'élément hydrogène ($\\text{H}$) dans les réactifs que dans les produits. On peut traduire cela par l'équation suivante :<br><br>
              $${coeffs[0].text} \\times ${y} = ${coeffs[3].text} \\times 2$<br><br>
              soit :<br><br>
              $${lettre} = ${resultat}$`
      }
    } else {
      for (let k = 0; k < coeffs.length; k++) {
        if (coeffs[k].hidden) {
          texteCorr += `$${coeffs[k].id} = ${coeffs[k].result}$<br>`
        }
      }
    }
    return { texte, texteCorr, isInteger: true }
  }

  transfoAlcool (i: number, j = 1) {
    const nomAlcool = ['du méthanol', 'de l\'éthanol', 'du propanol', 'du butanol', 'du pentanol', 'de l\'hexanol', 'de l\'heptanol', 'de l\'octanol']
    const coeffAlcool = randint(1, 3)
    const x = randint(1, 8)
    const y = 2 * x + 2
    const coeffO2 = (3 * x / 2) * coeffAlcool
    const coeffCO2 = x * coeffAlcool
    const coeffH2O = (x + 1) * coeffAlcool

    let a = ''
    let b = ''
    let c = ''
    let d = ''

    a = coeffAlcool.toString()
    if (coeffO2 % 1 === 0) {
      b = coeffO2.toString()
    } else {
      b = `\\frac{${(3 * x) * coeffAlcool}}{${2}}`
    }
    c = coeffCO2.toString()
    d = coeffH2O.toString()

    const coeffs =
      [
        {
          id: 'a',
          value: coeffAlcool,
          text: a,
          result: a,
          hidden: false
        },
        {
          id: 'b',
          value: coeffO2,
          text: b,
          result: b,
          hidden: false
        },
        {
          id: 'c',
          value: coeffCO2,
          text: c,
          result: c,
          hidden: false
        },
        {
          id: 'd',
          value: coeffH2O,
          text: d,
          result: d,
          hidden: false
        }
      ]

    const resultats = []

    let count = 0
    while (count < j) {
      const r = randint(0, coeffs.length - 1, resultats)
      coeffs[r].hidden = true
      coeffs[r].text = coeffs[r].id
      resultats.push(r)
      count++
    }

    let equation = ''

    const coeff = [coeffs[0].text, coeffs[1].text, coeffs[2].text, coeffs[3].text]
    for (let k = 0; k < coeff.length; k++) {
      if (coeff[k] === '1') {
        coeff[k] = ''
      }
    }
    if (x === 1) {
      equation = `\\[${coeff[0]}\\ \\text{C}_{${y}}\\text{H}_{${y}}\\text{O} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    } else {
      equation = `\\[${coeff[0]}\\ \\text{C}_{${x}}\\text{H}_{${y}}\\text{O} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    }

    const alcool = nomAlcool[x - 1]
    let texte = 'Soit l\'équation de combustion complète ' + alcool + ' : '
    texte += equation
    texte += '<br>Déterminer le (ou les) nombre(s) stoechiométrique(s) manquant(s) :<br><br>'

    let resultsCounter = 0
    for (let k = 0; k < coeffs.length; k++) {
      if (coeffs[k].hidden) {
        texte += ajouteChampTexteMathLive(this, j * i + resultsCounter, 'inline largeur01 ', { texteAvant: '$' + coeffs[k].id + ' = $' })
        texte += '<br><br>'
        handleAnswers(this, j * i + resultsCounter, { reponse: { value: coeffs[k].value, compare: approximatelyCompare, options: { tolerance: 1e-1 } } })
        resultsCounter++
      }
    }

    let texteCorr = ''
    if (j === 1) {
      const index = resultats[0]
      const resultat = coeffs[index].result
      const lettre = coeffs[index].id
      if (index === 0 || index === 2) {
        texteCorr = `Dans les réactifs, seule la molécule de $\\text{C}_{${x}}\\text{H}_{${y}}\\text{O}$ contient du carbone (par paquet de ${x}). Dans les produits, le dioxyde de carbone $\\text{CO}_2$ contient du carbone (par paquet de 1).<br><br>Afin d'équilibrer l'équation bilan, il est nécéssaire qu'il y ait autant d'élément carbone ($\\text{C}$) dans les réactifs que dans les produits. On peut traduire cela par l'équation suivante :<br><br>
                $${coeffs[0].text} \\times ${x} = ${coeffs[2].text} \\times 1$<br><br>
                soit :<br><br>
                $${lettre} = ${resultat}$`
      } else if (index === 1) {
        texteCorr = `Dans les réactifs, la molécule de $\\text{C}_{${x}}\\text{H}_{${y}}\\text{O}$ contient de l'oxygène (par paquet de 1) et les molécules de $\\text{O}_2$ contiennent de l'oxygène (par paquet de 2). Dans les produits, le dioxyde de carbone $\\text{CO}_2$ contient de l'oxygène (par paquet de 2) et l'eau $\\text{H}_2\\text{O}$ contient de l'oxygène (par paquet de 1).<br><br>Afin d'équilibrer l'équation bilan, il est nécéssaire qu'il y ait autant d'élément oxygène ($\\text{O}$) dans les réactifs que dans les produits. On peut traduire cela par l'équation suivante :<br><br>
                $${coeffs[0].text} \\times 1 + ${coeffs[1].text} \\times 2 = ${coeffs[2].text} \\times 2 + ${coeffs[3].text} \\times 1$<br><br>
                soit :<br><br>
                $${lettre} = ${resultat}$`
      } else if (index === 3) {
        texteCorr = `Dans les réactifs, seule la molécule de $\\text{C}_{${x}}\\text{H}_{${y}}\\text{O}$ contient de l'hydrogène (par paquet de ${y}). Dans les produits, l'eau $\\text{H}_2\\text{O}$ contient de l'hydrogène (par paquet de 2).<br><br>Afin d'équilibrer l'équation bilan, il est nécéssaire qu'il y ait autant d'élément hydrogène ($\\text{H}$) dans les réactifs que dans les produits. On peut traduire cela par l'équation suivante :<br><br>
                $${coeffs[0].text} \\times ${y} = ${coeffs[3].text} \\times 2$<br><br>
                soit :<br><br>
                $${lettre} = ${resultat}$`
      }
    } else if (j === 2 || j === 3) {
      for (let k = 0; k < coeffs.length; k++) {
        if (coeffs[k].hidden) {
          texteCorr += `$${coeffs[k].id} = ${coeffs[k].result}$<br>`
        }
      }
    }
    return { texte, texteCorr, isInteger: true }
  }
}
