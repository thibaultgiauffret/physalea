/**
 * DS-REA3-1.ts
 * @author Régis Ferreira Da Silva
 * @date 2024-12-18
 */

import { combinaisonListes } from '../../lib/outils/arrayOutils'
import Exercice from '../Exercice'
import { listeQuestionsToContenu, randint } from '../../modules/outils.js'
import { chiffresSignificatifs, ecritureScientifique } from '../../lib/outils_phys/ecritures'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { approximatelyCompare } from '../../lib/interactif/comparisonFunctions'
import { handleAnswers } from '../../lib/interactif/gestionInteractif'
import { prenom } from '../../lib/outils/Personne'

// à remplacer
export const titre = 'Convertir des unités.'
export const interactifReady = true
export const interactifType = 'mathLive'
// à remplacer
export const dateDePublication = '18/12/2024'
// à remplacer : pnpm getNewUuid pour récupérer l'UUID pour un nouvel exercice
export const uuid = '1dd27'
// à remplacer "test-2" par la référence dans le référentiel (disponible dans "src/json/allExercice.json")
export const refs = {
  'fr-fr': ['DS-REA3-1']
}

export default class Conversions extends Exercice {
  constructor () {
    super()
    this.nbQuestions = 1
    this.sup = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 3, '1 : Conversion grandeur simple (novice)\n 2 : Conversion grandeur composée  (confirmé)\n 3 : Conversion grandeur composée  (expert)']
    // On énonce la question
    this.introduction = `Aide ${prenom()} à réaliser la (ou les) conversion(s) d'unité suivante(s) : <br>`
  }

  nouvelleVersion () {
    this.listeQuestions = []
    this.listeCorrections = []

    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['novice']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['confirme']
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['expert']
    }

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles, this.nbQuestions)

    for (let i = 0, N, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      // variables conversions simples
      N = randint(200, 500)
      const nUnite = randint(0, 6)// choix de l'unité
      const unitesList = ['m', 'g', 'L', 'A', 'V', 'W', 'J']
      const unitesConvertieList = ['déci', 'centi', 'milli', 'déca', 'hecto', 'kilo']
      const nSousUnite = randint(1, 5)// choix de la sous-unité
      const symbolesUniteConvertieList = ['d', 'c', 'm', 'da', 'h', 'k']
      const explicitationUniteConvertieList = ['un dixième', 'un centième', 'un millième', 'une dizaine', 'une centaine', 'un millier']
      const facteursConversionUniteList = ['10 fois plus grand', '100 fois plus grand', '1000 fois plus grand', '10 fois plus petit', '100 fois plus petit', '1000 fois plus petit']
      const NExpressionConvertiList = [`${N}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}\\times 10`, `${N}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}\\times 100`, `${N}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}\\times 1000`, `\\cfrac{${N}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}}{10}`, `\\cfrac{${N}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}}{100}`, `\\cfrac{${N}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}}{1000}`]
      const NConvertiList = [N * 10, N * 100, N * 1000, N / 10, N / 100, N / 1000]
      // variables conversions vitesse
      const Vkmh = randint(25, 120)
      const Vms = Vkmh / 3.6

      const nbcs = 3

      switch (listeTypeDeQuestions[i]) {
        case 'novice':

          if (this.interactif) {
            //   On y ajoute le champ de réponse interactif
            this.question = ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: `$${N}\\, \\text{${unitesList[nUnite]}} =$`, texteApres: `$\\text{ ${symbolesUniteConvertieList[nSousUnite]}}\\text{${unitesList[nUnite]}}$` })
          } else {
            this.question = `$${N}\\, \\text{${unitesList[nUnite]}} = ... \\, \\text{ ${symbolesUniteConvertieList[nSousUnite]}}\\text{${unitesList[nUnite]}}$
            `
          }

          //   On génère la réponse
          this.reponse = NConvertiList[nSousUnite]
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare } })
          //   On écrit la correction
          this.correction = `Pour réaliser la conversion, il suffit de savoir que 1 ${unitesConvertieList[nSousUnite]} correspond à ${explicitationUniteConvertieList[nSousUnite]} d'une unité. <br> 
                    Une unité est donc ${facteursConversionUniteList[nSousUnite]} qu'un ${unitesConvertieList[nSousUnite]} . <br>
                    Par conséquent $${N}\\, \\text{${unitesList[nUnite]}} = ${NExpressionConvertiList[nSousUnite]}=${NConvertiList[nSousUnite]}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}$.<br><br>
                    En tenant compte des chiffres significatifs, on peut écrire que $${N}\\, \\text{${unitesList[nUnite]}} = ${ecritureScientifique(NConvertiList[nSousUnite], nbcs)}\\, \\text{${symbolesUniteConvertieList[nSousUnite]}${unitesList[nUnite]}}$.
                    `
          // fin question
          break
        case 'confirme':
          if (this.interactif) {
            //   On y ajoute le champ de réponse interactif
            this.question = ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: `$${chiffresSignificatifs(Vkmh, nbcs)}\\, \\text{km/h} =$`, texteApres: '$\\text{m/s}$' })
          } else {
            this.question = `$${chiffresSignificatifs(Vkmh, nbcs)}\\, \\text{km/h} = ... \\, \\text{m/s}$
            `
          }
          //   On génère la réponse
          this.reponse = Vms
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare } })
          //   On écrit la correction
          this.correction = `Il s'agit de faire ici une double conversion : les kilomètres en mètres et les heures en seconde. <br>
                    <ul>
                    <li>1 kilomètre correspond à 1000 mètres,</li>
                    <li>1 heure correspond à 3 600 secondes ($60\\times 60\\,\\text{s}$).</li>
                    </ul>
                    Ramenons-nous à la formule de la vitesse en faisant figurer les unités :<br><br>
                    $v=\\cfrac{\\text{distance parcourue}}{\\text{durée du trajet}}=\\cfrac{${chiffresSignificatifs(Vkmh, nbcs)}\\,\\text{km}}{1\\, \\text{h}}=\\cfrac{${chiffresSignificatifs(Vkmh, nbcs)}\\times\\text{1 km}}{1\\, \\text{h}}$<br><br>
                    Il s'agit maintenant de reporter les conversions d'unité directement dans la formule comme suit :<br><br>
                    $v=\\cfrac{${chiffresSignificatifs(Vkmh, nbcs)}\\times\\text{1 km}}{\\text{1\\, \\text{h}}}=\\cfrac{${chiffresSignificatifs(Vkmh, nbcs)}\\times\\text{1 000 m}}{\\text{3600 s}}=${chiffresSignificatifs(Vms, nbcs)} \\,\\text{m/s}$<br><br>
                    On peut donc écrire que $v=${chiffresSignificatifs(Vkmh, nbcs)} \\,\\text{km/h} =${chiffresSignificatifs(Vms, nbcs)} \\,\\text{m/s}$
                    `
          // fin question
          break
        case 'expert':
          if (this.interactif) {
            //   On y ajoute le champ de réponse interactif
            this.question = ajouteChampTexteMathLive(this, i, 'inline largeur01', { texteAvant: `$${chiffresSignificatifs(Vms, nbcs)}\\, \\text{m/s} =$`, texteApres: '$\\text{km/h}$' })
          } else {
            this.question = `$${chiffresSignificatifs(Vms, nbcs)}\\, \\text{m/s} = ... \\, \\text{km/h}$
            `
          }
          //   On génère la réponse
          this.reponse = Vkmh
          //   On gère la réponse. Ici, on utilise la fonction approximatelyCompare pour vérifier la valeur de T
          handleAnswers(this, i, { reponse: { value: this.reponse, compare: approximatelyCompare } })
          //   On écrit la correction
          this.correction = `Il s'agit de faire ici une double conversion : les mètres en kilomètres et les secondes en heures. <br>
                    <ul>
                    <li>Comme 1 kilomètre correspond à 1000 mètre alors 1 mètre correspond donc à $\\cfrac{1}{1000}\\,\\text{km}$.</li>
                    <li>Comme 1 heure correspond à 3 600 s alors 1 seconde correspond donc à $\\cfrac{1}{3600}\\,\\text{h}$. <br>
                    Le raisonnement précédent peut se ramener à une situation de proportionnalité : <br><br>

                    $\\def\\arraystretch{1.5}
                    \\begin{array}{|c|c|}
                    \\hline
                    1  \\text{h} & 3 600 \\text{s}\\\\
                    \\hline
                    \\cfrac{1  \\text{h}}{3 600} &1\\ \\text{s} \\\\
                    \\hline
                    \\end{array}
                    $<br><br>
                    
                    </li>
                    </ul>
                    Ramenons-nous à la formule de la vitesse en faisant figurer les unités :<br><br>
                    $v=\\cfrac{\\text{distance parcourue}}{\\text{durée du trajet}}=\\cfrac{${chiffresSignificatifs(Vms, nbcs)}\\,\\text{m}}{1\\, \\text{s}}=\\cfrac{${chiffresSignificatifs(Vms, nbcs)}\\times\\text{1 m}}{1\\, \\text{s}}$<br><br>
                    Il s'agit maintenant de reporter les conversions d'unité directement dans la formule comme suit :<br><br>
                    $v=\\cfrac{${chiffresSignificatifs(Vms, nbcs)}\\times\\text{1 m}}{\\text{1 s}}=\\cfrac{\\cfrac{${chiffresSignificatifs(Vms, nbcs)}\\times\\text{1 km}}{\\text{1 000}}}{\\cfrac{\\text{1 h}}{3 600}}=${chiffresSignificatifs(Vkmh, nbcs)} \\,\\text{km/h}$<br><br>
                    On peut donc écrire que $v=${chiffresSignificatifs(Vms, nbcs)} \\,\\text{m/s} =${chiffresSignificatifs(Vkmh, nbcs)} \\,\\text{km/h}$
                    `
          // fin question
          break
      }

      if (this.questionJamaisPosee(i, N!)) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(this.question!) // Sinon on enregistre la question dans listeQuestions
        this.listeCorrections.push(this.correction!) // On fait pareil pour la correction
        i++ // On passe à la question suivante
      }
      cpt++ // Sinon on incrémente le compteur d'essai pour avoir une question nouvelle
    }

    listeQuestionsToContenu(this) // La liste de question et la liste de la correction
  }
}
