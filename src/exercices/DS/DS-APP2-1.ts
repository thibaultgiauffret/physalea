/**
 * DS-APP2-1.ts
 * @author Régis Ferreira Da Silva
 * @date 2025-01-22
 */

import Exercice from '../Exercice'
import { combinaisonListes } from '../../lib/outils/arrayOutils'
import { listeQuestionsToContenu, randint } from '../../modules/outils'
import { propositionsQcm } from '../../lib/interactif/qcm'

// à remplacer
export const titre = 'Lire une consigne.'
export const interactifReady = true
export const interactifType = 'qcm'
// à remplacer
export const dateDePublication = '22/01/2025'

// à remplacer : pnpm getNewUuid pour récupérer l'UUID pour un nouvel exercice
export const uuid = 'bc9d8'

// remplacer "qcm-typeB" par la référence dans le référentiel (disponible dans "src/json/allExercice.json")
export const refs = {
  'fr-fr': ['DS-APP2-1']
}

export default class LectureConsigne extends Exercice {
  constructor () {
    super()
    this.nbQuestions = 2
    this.sup = 1
    this.besoinFormulaireNumerique = ['Choix des questions', 3, '1 : Novice(consigne)\n2 :Confirmé(question)\n3 : Expert']
  }

  nouvelleVersion () {
    let typesDeQuestionsDisponibles
    if (this.sup === 1) {
      typesDeQuestionsDisponibles = ['novice']
    } else if (this.sup === 2) {
      typesDeQuestionsDisponibles = ['confirme']
    } else if (this.sup === 3) {
      typesDeQuestionsDisponibles = ['expert']
    }

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles || [], this.nbQuestions)

    // On génère autant de questions que nécessaire (en évitant d'avoir deux fois la valeur de a dans cet exemple)
    for (let i = 0, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      let monQcm

      // On tire au sort le valeur aléatoire (à venir)
      // a = randint(1, 5) // numéro du verbe d'action tiré au sort
      // const VerbesactionConsignesList = ['Calculer', 'Déterminer', 'Identifier', 'Indiquer', 'Interpréter']// élaborer, indiquer,identifier
      // const SignificationVerbesactionList = ['fournir une valeur numérique (nombre + unité) d’une grandeur à partir d’une expression littérale.', 'donner la valeur d’une grandeur ou une caractéristique en utilisant une information d’ordre théorique (lois, relation, représentation graphique, propriété...).', 'reconnaı̂tre et nommer précisément.', 'reconnaı̂tre et nommer précisément.', 'utiliser un résultat ou une idée (ou plusieurs) qui vien(ne)t d’être formulés(s) pour produire une nouvelle idée ou obtenir un nouveau résultat.']// même signification pour identifier et indiquer, définition du groupe pégase
      // const ReformulationSignificationVerbesactionList = ['faire un calcul à partir d\'une formule', 'faire un calcul à partir d\'une formule. La formule est obligatoire', 'donner la valeur trouvée dans l\'énoncé', 'donner la valeur trouvée dans l\'énoncé', 'analyser le résultat obtenu et proposer une explication']

      // const ObjetConsignesCalculs = ['la masse de soluté', 'le volume de solution mère', 'le volume de solution fille', 'l\'angle d\'incidence', 'l\'angle de réfraction', 'l\'indice de réfraction du milieu 1']
      // const b = randint(1, 7)// numéro de l'objet de la consigne

      const consignes = [
        {
          enonce: 'Calculer la masse de soluté.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Calculer le volume de solution mère à prélever.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Calculer la norme du vecteur vitesse au point $M_5$.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Calculer le volume de solution fille préparée.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Calculer l\'indice de réfraction du milieu incident.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Calculer l\'indice de réfraction du milieu où se propage le rayon réfracté.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Calculer le grossissement du montage optique réalisé.',
          verbe: 'Calculer',
          reponse: ['calc', 'phrase','valeur'],
          correction: ''
        },
        {
          enonce: 'Déterminer la masse de soluté.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer le volume de solution mère à prélever.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer la norme du vecteur vitesse au point $M_5$.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer le volume de solution fille préparée.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer l\'indice de réfraction du milieu incident.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer l\'indice de réfraction du milieu où se propage le rayon réfracté.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer le grossissement du montage optique réalisé',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Déterminer la vitesse de propagation de l\'onde sonore dans l\'eau.',
          verbe: 'Déterminer',
          reponse: ['form', 'calc', 'phrase','valeur'],
          correction: 'Il s\'agit ici d\'utiliser la formule puis de calculer la valeur puis de conclure avec une phrase.'
        },
        {
          enonce: 'Identifier les réactifs de la réaction chimique.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Identifier le gaz formé lors de cette transformation.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Identifier les dipôles présents dans ce circuit.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Interpréter la valeur du courant électrique obtenue.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Interpréter la valeur de la tension électrique obtenue.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Interpréter la valeur de la concentration en masse obtenue.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Interpréter la valeur de la valeur de la masse volumique obtenue.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Interpréter la valeur de la valeur de la température de solidification obtenue.',
          verbe: 'Justifier',
          reponse: ['justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Élaborer le protocole expérimental permettant de préparer une solution de concentration en masse en soluté $t=0.50\\text{g/L}$.',
          verbe: 'Élaborer',
          reponse: ['listeP'],
          correction: ''
        },
        {
          enonce: 'Élaborer le protocole expérimental permettant de tracer la caractéristique du dipôle fourni.',
          verbe: 'Élaborer',
          reponse: ['listeP'],
          correction: ''
        },
        {
          enonce: 'Élaborer le protocole expérimental permettant de mesurer la température d\'ébullition du liquide à votre disposition.',
          verbe: 'Élaborer',
          reponse: ['listeP'],
          correction: ''
        },
        {
          enonce: 'Élaborer le protocole expérimental permettant de mesurer la température de solification du liquide à votre disposition.',
          verbe: 'Élaborer',
          reponse: ['listeP'],
          correction: ''
        },
        {
          enonce: 'Élaborer le protocole expérimental permettant de mesurer la masse volumique du liquide à votre disposition.',
          verbe: 'Élaborer',
          reponse: ['listeP'],
          correction: ''
        },
        {
          enonce: 'Élaborer la liste du matériel permettant de préparer une solution de concentration en masse en soluté $t=0.50\\text{g/L}$.',
          verbe: 'Élaborer',
          reponse: ['listeM'],
          correction: ''
        },
        {
          enonce: 'Élaborer la liste du matériel permettant de tracer la caractéristique du dipôle fourni.',
          verbe: 'Élaborer',
          reponse: ['listeM'],
          correction: ''
        },
        {
          enonce: 'Élaborer la liste du matériel permettant de mesurer la température d\'ébullition du liquide à votre disposition.',
          verbe: 'Élaborer',
          reponse: ['listeM'],
          correction: ''
        },
        {
          enonce: 'Élaborer la liste du matériel permettant de mesurer la température de solification du liquide à votre disposition.',
          verbe: 'Élaborer',
          reponse: ['listeM'],
          correction: ''
        },
        {
          enonce: 'Élaborer la liste du matériel permettant de mesurer la masse volumique du liquide à votre disposition.',
          verbe: 'Élaborer',
          reponse: ['listeM'],
          correction: ''
        },
        {
          enonce: 'Est-ce que le ballon passera au dessus du mur?',
          verbe: 'Est-ce que',
          reponse: ['valid', 'justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Est-ce que la solution contient de l\'eau?',
          verbe: 'Est-ce que',
          reponse: ['valid', 'justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Est-ce que la solution préparée peut être appliquée sur la peau?',
          verbe: 'Est-ce que',
          reponse: ['valid', 'justif', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Quelle est la masse de soluté?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'phrase', 'valeur'],
          correction: ''
        },
        {
          enonce: 'Quel est le volume de solution mère à prélever?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'phrase', 'valeur'],
          correction: ''
        },
        {
          enonce: 'Quelle est la norme du vecteur vitesse au point $M_5$?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'ph?uuid=463db&id=2CM8-5&n=1&d=10&s=3&cd=1&alea=KEc7&uuid=b8c16&id=2OS3-1&n=1&d=10&s=3&s2=2&cd=1&alea=bGkp&uuid=8775d&id=2OS3-3&alea=wG4Qrase'],
          correction: ''
        },
        {
          enonce: 'Quel est le volume de solution fille préparée?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Quel est l\'indice de réfraction du milieu incident?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Quel est l\'indice de réfraction du milieu où se propage le rayon réfracté?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'phrase'],
          correction: ''
        },
        {
          enonce: 'Quel est le grossissement du montage optique réalisé?',
          verbe: 'Quel(le) est',
          reponse: ['calc', 'phrase'],
          correction: ''
        }
      ]

      const reponses = [
        {
          id: 'calc',
          texte: 'Un calcul',
          statut: false,
          explicitation: 'Un calcul consiste à fournir une valeur numérique (nombre + unité) d’une grandeur à partir d’une expression littérale (formule).'
        },
        {
          id: 'valeur',
          texte: 'Une valeur',
          statut: false,
          explicitation: 'La valeur numérique contient le nombre et l\'unité éventuelle de la grandeur attendue.'
        },
        {
          id: 'form',
          texte: 'Une formule',
          statut: false,
          explicitation: 'La formule du cours doit être réécrite et manipuler pour exprimer la grandeur demandée.'
        },
        {
          id: 'justif',
          texte: 'Une justification',
          statut: false,
          explicitation: 'Une justification consiste à expliquer un résultat à partir de connaissances et/ou de l\'énoncé.'
        },
        {
          id: 'phrase',
          texte: 'Une phrase',
          statut: false,
          explicitation: 'Une phrase permet de rassembler les informations collectées dans l\'ensemble a réponse.'
        },
        {
          id: 'schem',
          texte: 'Un schéma',
          statut: false,
          explicitation: 'Un schéma reprenant les codes de représentation du domaine est attendu.'
        },
        {
          id: 'valid',
          texte: 'Une réponse de type vrai/faux ou oui/non',
          statut: false,
          explicitation: 'Une réponse courte à la consigne ou à la question suffit.'
        },
        {
          id: 'listeP',
          texte: 'Une liste des étapes',
          statut: false,
          explicitation: 'Une liste chronologique des actions expérimentales à réaliser pour réaliser l\'expérience.'
        },
        {
          id: 'listeM',
          texte: 'Une liste du matériel',
          statut: false,
          explicitation: 'Une liste d\'objets nécessaires pour réaliser l\'expérience.'
        }
      ]

      const a = randint(0, consignes.length - 1)
      const consigne = consignes[a]
      let correction = consigne.correction + '<br />'
      for (let j = 0; j <= consigne.reponse.length - 1; j++) {
        reponses.find(o => o.id === consigne.reponse[j])!.statut = true
        correction += reponses.find(o => o.id === consigne.reponse[j])!.explicitation + '<br />'
      }

      console.log(reponses)

      switch (listeTypeDeQuestions[i]) {
        case 'novice':
          // On énonce la question
          this.question = `Cocher la(es) les proposition(s) de réponse qui indique le type d’information demandé par la question : <br><br>
                   <b>${consigne.enonce}</b>
                    `

          // On génère la correction pour le mode interactif
          this.autoCorrection[i] = {
            enonce: this.question,
            options: { vertical: true },
            propositions: reponses
          }
          // On génère le propositions de réponses
          monQcm = propositionsQcm(this, i)
          // On ajoute les champs de réponse à l'énoncé de la question
          this.question = this.question + monQcm.texte

          // On énonce la correction
          this.correction = correction

          break
      }

      if (this.questionJamaisPosee(i, a!)) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions.push(this.question!)
        this.listeCorrections.push(this.correction!)
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }
}
