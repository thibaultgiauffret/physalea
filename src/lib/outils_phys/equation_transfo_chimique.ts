import { randint } from '../../modules/outils.js'

/**
 * equationCombustionAlcane
 * @param i
 * @param int
 * @returns {equation, alcane, especes, coeffs}
 * @description Une fonction qui génère une équation de combustion d'un alcane.
 */
export function equationCombustionAlcane (i: number, int = false) {
  // On définit les noms des alcanes disponibles
  const nomAlcane = ['du méthane', 'de l\'éthane', 'du propane', 'du butane', 'du pentane', 'de l\'hexane', 'de l\'heptane', 'de l\'octane']

  // On tire au sort les coefficients de l'équation
  const coeffAlcane = randint(1, 3)
  const x = randint(1, 8)
  const y = 2 * x + 2
  const coeffO2 = ((3 * x + 1) / 2) * coeffAlcane
  const coeffCO2 = x * coeffAlcane
  const coeffH2O = (x + 1) * coeffAlcane
  const coeffsNum = [coeffAlcane, coeffO2, coeffCO2, coeffH2O]

  //   Si on veut des coefficients entiers, on retourne une équation vide si les coefficients tirés au sort ne sont pas entiers
  if (int) {
    if (coeffsNum[1] % 1 !== 0 || coeffsNum[2] % 1 !== 0 || coeffsNum[3] % 1 !== 0) {
      return { equation: '', alcane: '', coeffs: [] }
    }
  }

  //   On construit la liste des coefficients (texte et valeur)
  let a = ''
  let b = ''
  let c = ''
  let d = ''

  a = coeffsNum[0].toString()
  if (coeffsNum[1] % 1 === 0) {
    b = coeffsNum[1].toString()
  } else {
    b = `\\frac{${(3 * x + 1) * coeffAlcane}}{2}`
  }
  c = coeffsNum[2].toString()
  d = coeffsNum[3].toString()
  //   Les coeffs possèdent un id, une valeur, un texte, un résultat et un booléen pour savoir s'ils sont cachés ou non (dans le cas où on voudrait en retrouver un à partir de l'équation, 2CM9-1 par exemple)
  const coeffs =
      [
        {
          id: 'a',
          value: coeffsNum[0],
          text: a,
          result: a,
          hidden: false
        },
        {
          id: 'b',
          value: coeffsNum[1],
          text: b,
          result: b,
          hidden: false
        },
        {
          id: 'c',
          value: coeffsNum[2],
          text: c,
          result: c,
          hidden: false
        },
        {
          id: 'd',
          value: coeffsNum[3],
          text: d,
          result: d,
          hidden: false
        }
      ]

  //   Si un coeff vaut 1, on remplace son texte par une chaîne vide
  const coeff = [coeffs[0].text, coeffs[1].text, coeffs[2].text, coeffs[3].text]
  for (let k = 0; k < coeff.length; k++) {
    if (coeff[k] === '1') {
      coeff[k] = ''
    }
  }

  //   On construit l'équation
  let equation = ''
  let especes: string[] = []
  //   Avec le cas particulier du méthane
  if (x === 1) {
    equation = `\\[${coeff[0]}\\ \\text{CH}_{${y}} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    especes = ['\\text{CH}_{4}', '\\text{O}_2', '\\text{CO}_2', '\\text{H}_2\\text{O}']
  } else {
    equation = `\\[${coeff[0]}\\ \\text{C}_${x}\\text{H}_{${y}} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    especes = [`\\text{C}_{${x}}\\text{H}_{${y}}`, '\\text{O}_2', '\\text{CO}_2', '\\text{H}_2\\text{O}']
  }

  //   On récupère le nom de l'alcane
  const alcane = nomAlcane[x - 1]

  //   On retourne l'équation, le nom de l'alcane, les espèces, les espèces simples et les coefficients
  return { equation, alcane, especes, coeffs }
}

export function equationCombustionAlcool (i: number, int = false) {
  const nomAlcool = ['du méthanol', 'de l\'éthanol', 'du propanol', 'du butanol', 'du pentanol', 'de l\'hexanol', 'de l\'heptanol', 'de l\'octanol']
  const coeffAlcool = randint(1, 3)
  const x = randint(1, 8)
  const y = 2 * x + 2
  const coeffO2 = (3 * x / 2) * coeffAlcool
  const coeffCO2 = x * coeffAlcool
  const coeffH2O = (x + 1) * coeffAlcool

  const coeffsNum = [coeffAlcool, coeffO2, coeffCO2, coeffH2O]

  if (int) {
    if (coeffsNum[1] % 1 !== 0 || coeffsNum[2] % 1 !== 0 || coeffsNum[3] % 1 !== 0) {
      return { equation: '', alcool: '', coeffs: [] }
    }
  }

  let a = ''
  let b = ''
  let c = ''
  let d = ''

  a = coeffAlcool.toString()
  if (coeffO2 % 1 === 0) {
    b = coeffO2.toString()
  } else {
    b = `\\frac{${(3 * x) * coeffAlcool}}{${2}}`
  }
  c = coeffCO2.toString()
  d = coeffH2O.toString()

  const coeffs =
    [
      {
        id: 'a',
        value: coeffAlcool,
        text: a,
        result: a,
        hidden: false
      },
      {
        id: 'b',
        value: coeffO2,
        text: b,
        result: b,
        hidden: false
      },
      {
        id: 'c',
        value: coeffCO2,
        text: c,
        result: c,
        hidden: false
      },
      {
        id: 'd',
        value: coeffH2O,
        text: d,
        result: d,
        hidden: false
      }
    ]

  const coeff = [coeffs[0].text, coeffs[1].text, coeffs[2].text, coeffs[3].text]
  for (let k = 0; k < coeff.length; k++) {
    if (coeff[k] === '1') {
      coeff[k] = ''
    }
  }

  let equation = ''
  let especes: string[] = []
  if (x === 1) {
    equation = `\\[${coeff[0]}\\ \\text{C}_{${y}}\\text{H}_{${y}}\\text{O} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    especes = [`\\text{C}_{${y}}\\text{H}_{${y}}\\text{O}`, '\\text{O}_2', '\\text{CO}_2', '\\text{H}_2\\text{O}']
  } else {
    equation = `\\[${coeff[0]}\\ \\text{C}_{${x}}\\text{H}_{${y}}\\text{O} + ${coeff[1]}\\ \\text{O}_2 \\rightarrow ${coeff[2]}\\ \\text{CO}_2 + ${coeff[3]}\\ \\text{H}_2\\text{O}\\]`
    especes = [`\\text{C}_{${x}}\\text{H}_{${y}}\\text{O}`, '\\text{O}_2', '\\text{CO}_2', '\\text{H}_2\\text{O}']
  }

  const alcool = nomAlcool[x - 1]

  return { equation, alcool, especes, coeffs }
}
