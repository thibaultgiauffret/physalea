// A simple circuit class for electrical circuits
// For html, the circuit is generated as an svg component
// For latex, the circuit is generated in a circuitikz environment
import { context } from '../../modules/context.js'

export function createSimpleCircuit (components = [{ id: 0, type: '', pos: 'top', voltage: 0 }]) {
  if (context.isHtml) {
    const rectOffsetLeft = 200
    const rectOffsetTop = 100
    // Draw as svg
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')

    // Add the voltage arrow next to the resistor
    // Def marker for the arrow
    const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
    const marker = arrowMarker('fill-red-500 dark:fill-red-400')
    defs.appendChild(marker)
    svg.appendChild(defs)

    // Draw the main rectangle
    const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect.setAttribute('x', rectOffsetLeft.toString())
    rect.setAttribute('y', rectOffsetTop.toString())
    rect.setAttribute('width', '200')
    rect.setAttribute('height', '150')
    rect.setAttribute('class', 'stroke-current dark:stroke-current')
    rect.setAttribute('stroke-width', '2')
    rect.setAttribute('fill', 'transparent')
    svg.appendChild(rect)

    // Add intensity arrow

    // For each component in the json, draw the corresponding component
    for (let i = 0; i < components.length; i++) {
      const component = { id: 0, type: '', pos: 'top', voltage: 0, coord: { x: 0, y: 0 }, voltageLabel: '', voltagePos: { x: 0, y: 0 }, voltageLabelPos: { x: 0, y: 0, anchor: '' }, voltageAngle: 0, angle: 0 }
      // Handle position (top, bottom, left, right)
      if (components[i].pos === 'top') {
        component.coord = { x: 100 + rectOffsetLeft, y: rectOffsetTop }
        component.voltagePos = { x: 0, y: -30 }
        component.voltageLabelPos = { x: 0, y: -50, anchor: 'middle' }
        component.voltageAngle = 180
        component.angle = 0
      } else if (components[i].pos === 'bottom') {
        component.coord = { x: 100 + rectOffsetLeft, y: 150 + rectOffsetTop }
        component.voltagePos = { x: 0, y: 30 }
        component.voltageLabelPos = { x: 0, y: 50, anchor: 'middle' }
        component.voltageAngle = 0
        component.angle = 0
      } else if (components[i].pos === 'left') {
        component.coord = { x: rectOffsetLeft, y: 75 + rectOffsetTop }
        component.angle = 90
        component.voltagePos = { x: -30, y: 0 }
        component.voltageLabelPos = { x: -40, y: 0, anchor: 'end' }
        component.voltageAngle = 90
      } else if (components[i].pos === 'right') {
        component.coord = { x: 200 + rectOffsetLeft, y: 75 + rectOffsetTop }
        component.angle = 90
        component.voltagePos = { x: 30, y: 0 }
        component.voltageLabelPos = { x: 40, y: 0, anchor: 'start' }
        component.voltageAngle = 270
      }
      component.id = i + 1
      component.voltage = components[i].voltage

      if (components[i].voltage === 0) {
        component.voltageLabel = '?'
      } else {
        component.voltageLabel = component.voltage.toString()
      }

      let g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
      if (components[i].type === 'resistor') {
        g = drawResistorSVG(component)
      } else if (components[i].type === 'generator') {
        g = drawGeneratorSVG(component)
      } else if (components[i].type === 'lamp') {
        g = drawLampSVG(component)
      }
      svg.appendChild(g)
    }

    // Reduce the svg to its content
    svg.setAttribute('viewBox', '00 30 620 300')

    return svg.outerHTML
  } else {
    return drawCircuitLaTeX(components, [])
  }
}

export function createComplexCircuit (components = [{ id: 0, type: '', pos: 'top', voltage: 0 }], intensities = [{ id: 0, pos: 'left', value: 0 }]) {
  const newIntensities = [] as { id: number, pos: string, value: number, coord: { x: number, y: number }, coordLaTeX: [string, string], x: number, y: number, angle: number }[]
  let displayVoltage = true
  if (intensities.length !== 0) {
    displayVoltage = false
    for (let i = 0; i < intensities.length; i++) {
      newIntensities.push({ id: 0, pos: intensities[i].pos, value: intensities[i].value, coord: { x: 0, y: 0 }, coordLaTeX: ['', ''], x: 0, y: 0, angle: 0 })
    }
  }
  // Circuit with multiple branches (2 nodes)
  if (context.isHtml) {
    const rectOffsetLeft = 200
    const rectOffsetTop = 100
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
    const marker = arrowMarker('fill-red-500 dark:fill-red-400')
    defs.appendChild(marker)
    svg.appendChild(defs)

    // Draw the main rectangle
    const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect.setAttribute('x', rectOffsetLeft.toString())
    rect.setAttribute('y', rectOffsetTop.toString())
    rect.setAttribute('width', '150')
    rect.setAttribute('height', '150')
    rect.setAttribute('class', 'stroke-current dark:stroke-current')
    rect.setAttribute('stroke-width', '2')
    rect.setAttribute('fill', 'transparent')
    svg.appendChild(rect)

    // Draw the second rectangle
    const rect2 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect2.setAttribute('x', (150 + rectOffsetLeft).toString())
    rect2.setAttribute('y', rectOffsetTop.toString())
    rect2.setAttribute('width', '150')
    rect2.setAttribute('height', '150')
    rect2.setAttribute('class', 'stroke-current dark:stroke-current')
    rect2.setAttribute('stroke-width', '2')
    rect2.setAttribute('fill', 'transparent')
    svg.appendChild(rect2)

    // For each component in the json, draw the corresponding component
    for (let i = 0; i < components.length; i++) {
      const component = { id: 0, type: '', pos: 'top', voltage: 0, coord: { x: 0, y: 0 }, voltageLabel: '', voltagePos: { x: 0, y: 0 }, voltageLabelPos: { x: 0, y: 0, anchor: '' }, voltageAngle: 0, angle: 0 }
      // Handle position (top, bottom, left, right)
      if (components[i].pos === 'top') {
        component.coord = { x: 75 + rectOffsetLeft, y: rectOffsetTop }
        component.voltagePos = { x: 0, y: -30 }
        component.voltageLabelPos = { x: 0, y: -50, anchor: 'middle' }
        component.voltageAngle = 180
        component.angle = 0
      } else if (components[i].pos === 'bottom') {
        component.coord = { x: 75 + rectOffsetLeft, y: 150 + rectOffsetTop }
        component.voltagePos = { x: 0, y: 30 }
        component.voltageLabelPos = { x: 0, y: 50, anchor: 'middle' }
        component.voltageAngle = 0
        component.angle = 0
      } else if (components[i].pos === 'left') {
        component.coord = { x: rectOffsetLeft, y: 75 + rectOffsetTop }
        component.angle = 90
        component.voltagePos = { x: -30, y: 0 }
        component.voltageLabelPos = { x: -40, y: 0, anchor: 'end' }
        component.voltageAngle = 90
      } else if (components[i].pos === 'right') {
        component.coord = { x: 150 + rectOffsetLeft, y: 75 + rectOffsetTop }
        component.angle = 90
        component.voltagePos = { x: 30, y: 0 }
        component.voltageLabelPos = { x: 40, y: 0, anchor: 'start' }
        component.voltageAngle = 270
      } else if (components[i].pos === 'top2') {
        component.coord = { x: 225 + rectOffsetLeft, y: rectOffsetTop }
        component.voltagePos = { x: 0, y: -30 }
        component.voltageLabelPos = { x: 0, y: -50, anchor: 'middle' }
        component.voltageAngle = 180
        component.angle = 0
      } else if (components[i].pos === 'bottom2') {
        component.coord = { x: 225 + rectOffsetLeft, y: 150 + rectOffsetTop }
        component.voltagePos = { x: 0, y: 30 }
        component.voltageLabelPos = { x: 0, y: 50, anchor: 'middle' }
        component.voltageAngle = 0
        component.angle = 0
      } else if (components[i].pos === 'right2') {
        component.coord = { x: 300 + rectOffsetLeft, y: 75 + rectOffsetTop }
        component.angle = 90
        component.voltagePos = { x: 30, y: 0 }
        component.voltageLabelPos = { x: 40, y: 0, anchor: 'start' }
        component.voltageAngle = 270
      }
      component.id = i + 1
      component.voltage = components[i].voltage

      if (components[i].voltage === 0) {
        component.voltageLabel = '?'
      } else {
        component.voltageLabel = component.voltage.toString()
      }

      let g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
      if (components[i].type === 'resistor') {
        g = drawResistorSVG(component, displayVoltage)
      } else if (components[i].type === 'generator') {
        g = drawGeneratorSVG(component, displayVoltage)
      } else if (components[i].type === 'lamp') {
        g = drawLampSVG(component, displayVoltage)
      }
      svg.appendChild(g)
    }

    // Add the three intensity arrows
    for (let i = 0; i < intensities.length; i++) {
      const intensity = { id: 0, pos: 'left', coordLaTeX: ['', ''], value: 0, x: 0, y: 0, angle: 0 }
      if (newIntensities[i].pos === 'left') {
        intensity.x = 120 + rectOffsetLeft
        intensity.y = rectOffsetTop
        intensity.angle = 270
      } else if (newIntensities[i].pos === 'bottom') {
        intensity.x = 150 + rectOffsetLeft
        intensity.y = 30 + rectOffsetTop
        intensity.angle = 0
      } else if (newIntensities[i].pos === 'right') {
        intensity.x = 180 + rectOffsetLeft
        intensity.y = rectOffsetTop
        intensity.angle = 270
      }
      intensity.id = i + 1
      intensity.value = newIntensities[i].value
      intensity.pos = newIntensities[i].pos

      svg.appendChild(intensityArrow(intensity))
    }

    // Reduce the svg to its content
    svg.setAttribute('viewBox', '00 30 720 300')
    return svg.outerHTML
  } else {
    return drawCircuitLaTeX(components, newIntensities)
  }
}

function arrowMarker (classe = 'stroke-current dark:stroke-current'): SVGElement {
  const marker = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
  marker.setAttribute('id', 'head')
  marker.setAttribute('markerWidth', '3')
  marker.setAttribute('markerHeight', '4')
  marker.setAttribute('refX', '0.1')
  marker.setAttribute('refY', '2')
  const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
  path.setAttribute('d', 'M0,0 V4 L2,2 Z')
  path.setAttribute('class', classe)
  marker.appendChild(path)

  return marker
}

function intensityArrow (intensity = { id: 0, pos: 'left', value: 0, x: 0, y: 0, angle: 0 }): SVGElement {
  // Two lines for the intensity arrow
  const g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
  const line1 = document.createElementNS('http://www.w3.org/2000/svg', 'line')
  line1.setAttribute('x1', (intensity.x - 10).toString())
  line1.setAttribute('y1', (intensity.y - 10).toString())
  line1.setAttribute('x2', (intensity.x).toString())
  line1.setAttribute('y2', (intensity.y).toString())
  line1.setAttribute('class', 'stroke-green-500 dark:stroke-green-400')
  line1.setAttribute('stroke-width', '2')
  line1.setAttribute('transform-origin', (intensity.x).toString() + ' ' + (intensity.y).toString())
  line1.setAttribute('transform', 'rotate(' + intensity.angle + ')')
  g.appendChild(line1)

  const line2 = document.createElementNS('http://www.w3.org/2000/svg', 'line')
  line2.setAttribute('x1', (intensity.x).toString())
  line2.setAttribute('y1', (intensity.y).toString())
  line2.setAttribute('x2', (intensity.x + 10).toString())
  line2.setAttribute('y2', (intensity.y - 10).toString())
  line2.setAttribute('class', 'stroke-green-500 dark:stroke-green-400')
  line2.setAttribute('stroke-width', '2')
  line2.setAttribute('transform-origin', (intensity.x).toString() + ' ' + (intensity.y).toString())
  line2.setAttribute('transform', 'rotate(' + intensity.angle + ')')
  g.appendChild(line2)

  // Add text with the intensity value
  const text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
  if (intensity.pos === 'left') {
    text.setAttribute('transform-origin', (intensity.x).toString() + ' ' + (intensity.y).toString())
    text.setAttribute('transform', 'translate(' + (intensity.x + 15).toString() + ', ' + (intensity.y - 35).toString() + ')')
    text.setAttribute('text-anchor', 'end')
  } else if (intensity.pos === 'right') {
    text.setAttribute('transform-origin', (intensity.x).toString() + ' ' + (intensity.y).toString())
    text.setAttribute('transform', 'translate(' + (intensity.x - 15).toString() + ', ' + (intensity.y - 35).toString() + ')')
    text.setAttribute('text-anchor', 'start')
  } else if (intensity.pos === 'bottom') {
    text.setAttribute('transform-origin', (intensity.x).toString() + ' ' + (intensity.y).toString())
    text.setAttribute('transform', 'translate(' + (intensity.x - 10).toString() + ', ' + (intensity.y).toString() + ')')
    text.setAttribute('text-anchor', 'end')
  }
  text.setAttribute('class', 'fill-green-500 dark:fill-green-400')
  text.setAttribute('dominant-baseline', 'middle')
  const sub = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
  sub.setAttribute('dy', '7')
  sub.setAttribute('font-size', '0.8em')
  sub.textContent = intensity.id.toString()
  const tspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
  tspan.textContent = 'I'

  tspan.appendChild(sub)

  const end = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
  end.setAttribute('dy', '-7')
  if (intensity.value !== null) {
    end.textContent = ' = ' + intensity.value.toString() + ' mA'
  } else {
    end.textContent = ' = ? mA'
  }
  tspan.appendChild(end)

  text.appendChild(tspan)
  g.appendChild(text)

  return g
}

function drawResistorSVG (component = { id: 0, angle: 0, pos: '', coord: { x: 0, y: 0 }, voltage: 0, voltagePos: { x: 0, y: 0 }, voltageAngle: 0, voltageLabel: '', voltageLabelPos: { x: 0, y: 0, anchor: 'middle' } }, displayVoltage = true) {
  const id = component.id.toString()
  const angle = component.angle
  const coord = component.coord
  // const voltage = component.voltage
  const voltagePos = component.voltagePos
  const voltageAngle = component.voltageAngle
  const voltageLabelPos = component.voltageLabelPos
  const voltageLabel = component.voltageLabel

  const g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
  const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')

  rect.setAttribute('x', (coord.x).toString())
  rect.setAttribute('y', (coord.y).toString())

  rect.setAttribute('transform-origin', (coord.x + 20).toString() + ' ' + (coord.y + 10).toString())
  rect.setAttribute('transform', 'translate(-20, -10) rotate(' + angle + ')')

  rect.setAttribute('width', '40')
  rect.setAttribute('height', '20')
  rect.setAttribute('class', 'stroke-current dark:stroke-current fill-white dark:fill-gray-800')
  rect.setAttribute('stroke-width', '2')
  rect.setAttribute('fill', 'white')
  g.appendChild(rect)

  if (displayVoltage) {
    // Create the line
    const arrowLine = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    arrowLine.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
    arrowLine.setAttribute('transform', 'translate(' + voltagePos.x + ', ' + voltagePos.y + ') rotate(' + voltageAngle + ')')

    arrowLine.setAttribute('id', 'arrowLine')
    arrowLine.setAttribute('x1', (coord.x - 20).toString())
    arrowLine.setAttribute('y1', (coord.y).toString())
    arrowLine.setAttribute('x2', (coord.x + 20).toString())
    arrowLine.setAttribute('y2', (coord.y).toString())
    arrowLine.setAttribute('class', 'stroke-red-500 dark:stroke-red-400')
    arrowLine.setAttribute('stroke-width', '2')
    arrowLine.setAttribute('marker-end', 'url(#head)')
    g.appendChild(arrowLine)

    // Add label
    const text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    text.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
    text.setAttribute('transform', 'translate(' + voltageLabelPos.x + ', ' + voltageLabelPos.y + ')')

    text.setAttribute('x', (coord.x).toString())
    text.setAttribute('y', (coord.y).toString())
    text.setAttribute('class', 'fill-red-500 dark:fill-red-400')
    text.setAttribute('text-anchor', voltageLabelPos.anchor)
    text.setAttribute('dominant-baseline', 'middle')
    const sub = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    sub.setAttribute('dy', '7')
    sub.setAttribute('font-size', '0.8em')
    sub.textContent = id
    const tspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    tspan.textContent = 'U'
    tspan.appendChild(sub)
    const end = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    end.setAttribute('dy', '-7')
    end.textContent = ' = ' + voltageLabel + ' V'
    tspan.appendChild(end)

    text.appendChild(tspan)

    g.appendChild(text)
  }

  return g
}

function drawGeneratorSVG (component = { id: 0, angle: 0, pos: '', coord: { x: 0, y: 0 }, voltage: 0, voltageLabel: '', voltagePos: { x: 0, y: 0 }, voltageAngle: 0, voltageLabelPos: { x: 0, y: 0, anchor: 'middle' } }, displayVoltage = true) {
  const id = component.id.toString()
  const angle = component.angle
  const coord = component.coord
  // const voltage = component.voltage
  const voltagePos = component.voltagePos
  const voltageAngle = component.voltageAngle + 180
  const voltageLabelPos = component.voltageLabelPos
  const voltageLabel = component.voltageLabel

  // Draw a generator with a circle and two lines
  const g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
  const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
  circle.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
  circle.setAttribute('transform', 'rotate(' + angle + ')')

  // Add the voltage arrow next to the generator
  if (displayVoltage) {
  // Create the line
    const arrowLine = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    arrowLine.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
    arrowLine.setAttribute('transform', 'translate(' + voltagePos.x + ', ' + voltagePos.y + ') rotate(' + voltageAngle + ')')
    arrowLine.setAttribute('id', 'arrowLine')
    arrowLine.setAttribute('x1', (coord.x - 20).toString())
    arrowLine.setAttribute('y1', (coord.y).toString())
    arrowLine.setAttribute('x2', (coord.x + 20).toString())
    arrowLine.setAttribute('y2', (coord.y).toString())
    arrowLine.setAttribute('class', 'stroke-red-500 dark:stroke-red-400')
    arrowLine.setAttribute('stroke-width', '2')
    arrowLine.setAttribute('marker-end', 'url(#head)')
    g.appendChild(arrowLine)

    // Add label
    const text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    text.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
    text.setAttribute('transform', 'translate(' + voltageLabelPos.x + ', ' + voltageLabelPos.y + ')')
    text.setAttribute('x', (coord.x).toString())
    text.setAttribute('y', (coord.y).toString())
    text.setAttribute('class', 'fill-red-500 dark:fill-red-400')
    text.setAttribute('text-anchor', voltageLabelPos.anchor)
    text.setAttribute('dominant-baseline', 'middle')
    const sub = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    sub.setAttribute('dy', '7')
    sub.setAttribute('font-size', '0.8em')
    sub.textContent = id
    const tspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    tspan.textContent = 'U'
    tspan.appendChild(sub)
    const end = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    end.setAttribute('dy', '-7')
    end.textContent = ' = ' + voltageLabel + ' V'
    tspan.appendChild(end)
    text.appendChild(tspan)
    g.appendChild(text)
  }

  circle.setAttribute('cx', (coord.x).toString())
  circle.setAttribute('cy', (coord.y).toString())
  circle.setAttribute('r', '20')
  circle.setAttribute('class', 'stroke-current dark:stroke-current')
  circle.setAttribute('stroke-width', '2')
  circle.setAttribute('fill', 'transparent')
  g.appendChild(circle)
  return g
}

function drawLampSVG (component = { id: 0, angle: 0, pos: '', coord: { x: 0, y: 0 }, voltage: 0, voltageLabel: '', voltagePos: { x: 0, y: 0 }, voltageAngle: 0, voltageLabelPos: { x: 0, y: 0, anchor: 'middle' } }, displayVoltage = true) {
  const id = component.id.toString()
  const angle = component.angle
  const coord = component.coord
  // const voltage = component.voltage
  const voltagePos = component.voltagePos
  const voltageAngle = component.voltageAngle
  const voltageLabelPos = component.voltageLabelPos
  const voltageLabel = component.voltageLabel

  const g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
  const circ = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
  circ.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
  circ.setAttribute('transform', 'rotate(' + angle + ')')
  circ.setAttribute('cx', (coord.x).toString())
  circ.setAttribute('cy', (coord.y).toString())
  circ.setAttribute('r', '20')
  circ.setAttribute('class', 'stroke-current dark:stroke-current fill-white dark:fill-gray-800')
  circ.setAttribute('stroke-width', '2')
  g.appendChild(circ)

  // Draw the X in the circle
  const line1 = document.createElementNS('http://www.w3.org/2000/svg', 'line')
  line1.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
  line1.setAttribute('transform', 'translate(0, 0) rotate(90)')
  line1.setAttribute('x1', (coord.x - 15).toString())
  line1.setAttribute('y1', (coord.y - 15).toString())
  line1.setAttribute('x2', (coord.x + 15).toString())
  line1.setAttribute('y2', (coord.y + 15).toString())
  line1.setAttribute('class', 'stroke-current dark:stroke-current')
  line1.setAttribute('stroke-width', '2')
  g.appendChild(line1)

  const line2 = document.createElementNS('http://www.w3.org/2000/svg', 'line')
  line2.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
  line2.setAttribute('transform', 'translate(0, 0) rotate(-90)')
  line2.setAttribute('x1', (coord.x - 15).toString())
  line2.setAttribute('y1', (coord.y + 15).toString())
  line2.setAttribute('x2', (coord.x + 15).toString())
  line2.setAttribute('y2', (coord.y - 15).toString())
  line2.setAttribute('class', 'stroke-current dark:stroke-current')
  line2.setAttribute('stroke-width', '2')
  g.appendChild(line2)

  if (displayVoltage) {
  // Create the line
    const arrowLine = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    arrowLine.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
    arrowLine.setAttribute('transform', 'translate(' + voltagePos.x + ', ' + voltagePos.y + ') rotate(' + voltageAngle + ')')
    arrowLine.setAttribute('id', 'arrowLine')
    arrowLine.setAttribute('x1', (coord.x - 20).toString())
    arrowLine.setAttribute('y1', (coord.y).toString())
    arrowLine.setAttribute('x2', (coord.x + 20).toString())
    arrowLine.setAttribute('y2', (coord.y).toString())
    arrowLine.setAttribute('class', 'stroke-red-500 dark:stroke-red-400')
    arrowLine.setAttribute('stroke-width', '2')
    arrowLine.setAttribute('marker-end', 'url(#head)')
    g.appendChild(arrowLine)

    // Add label
    const text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    text.setAttribute('transform-origin', (coord.x).toString() + ' ' + (coord.y).toString())
    text.setAttribute('transform', 'translate(' + voltageLabelPos.x + ', ' + voltageLabelPos.y + ')')
    text.setAttribute('x', (coord.x).toString())
    text.setAttribute('y', (coord.y).toString())
    text.setAttribute('class', 'fill-red-500 dark:fill-red-400')
    text.setAttribute('text-anchor', voltageLabelPos.anchor)
    text.setAttribute('dominant-baseline', 'middle')
    const sub = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    sub.setAttribute('dy', '7')
    sub.setAttribute('font-size', '0.8em')
    sub.textContent = id
    const tspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    tspan.textContent = 'U'
    tspan.appendChild(sub)
    const end = document.createElementNS('http://www.w3.org/2000/svg', 'tspan')
    end.setAttribute('dy', '-7')
    end.textContent = ' = ' + voltageLabel + ' V'
    tspan.appendChild(end)
    text.appendChild(tspan)
    g.appendChild(text)
  }

  return g
}

function drawCircuitLaTeX (components = [{ id: 0, type: '', pos: 'top', voltage: 0 }], intensities = [{ id: 0, pos: 'left', coord: { x: 0, y: 0 }, coordLaTeX: ['', ''], value: 0, x: 0, y: 0, angle: 0 }]) {
  // Generate the circuit in latex
  let latex = `
\\begin{center}
\\begin{circuitikz}`
  for (let i = 0; i < components.length; i++) {
    const component = { id: 0, type: '', pos: '', voltage: 0, coord: { a: '', b: '' }, voltageLabel: '' }
    if (components[i].pos === 'top') {
      component.coord.a = '(0,3)'
      component.coord.b = '(3,3)'
    } else if (components[i].pos === 'bottom') {
      component.coord.a = '(3,0)'
      component.coord.b = '(0,0)'
    } else if (components[i].pos === 'left') {
      component.coord.a = '(0,0)'
      component.coord.b = '(0,3)'
    } else if (components[i].pos === 'right') {
      component.coord.a = '(3,3)'
      component.coord.b = '(3,0)'
    } else if (components[i].pos === 'top2') {
      component.coord.a = '(3,3)'
      component.coord.b = '(6,3)'
    } else if (components[i].pos === 'bottom2') {
      component.coord.a = '(6,0)'
      component.coord.b = '(3,0)'
    } else if (components[i].pos === 'right2') {
      component.coord.a = '(6,3)'
      component.coord.b = '(6,0)'
    }
    component.id = i + 1
    component.voltage = components[i].voltage

    if (components[i].voltage !== 0) {
      component.voltageLabel = `\\qty{${component.voltage}}{\\volt}`

      if (components[i].type === 'resistor') {
        latex += `
        \\draw ` + component.coord.a + ` to[R, v^<=\\mbox{$U_${component.id}=${component.voltageLabel}$}] ` + component.coord.b + ';'
      } else if (components[i].type === 'generator') {
        latex += `
        \\draw ` + component.coord.a + ` to[V, v=\\mbox{$U_${component.id}= ${component.voltageLabel}$}] ` + component.coord.b + ';'
      } else if (components[i].type === 'lamp') {
        latex += `
        \\draw ` + component.coord.a + ` to[lamp, v^<=\\mbox{$U_${component.id}= ${component.voltageLabel}$}] ` + component.coord.b + ';'
      } else {
        // Add a wire
        latex += `
        \\draw ` + component.coord.a + ' to ' + component.coord.b + ';'
      }
    } else {
      if (components[i].type === 'resistor') {
        latex += `
        \\draw ` + component.coord.a + ' to[R] ' + component.coord.b + ';'
      } else if (components[i].type === 'generator') {
        latex += `
        \\draw ` + component.coord.a + ' to[V] ' + component.coord.b + ';'
      } else if (components[i].type === 'lamp') {
        latex += `
        \\draw ` + component.coord.a + ' to[lamp] ' + component.coord.b + ';'
      } else {
        // Add a wire
        latex += `
        \\draw ` + component.coord.a + ' to ' + component.coord.b + ';'
      }
      component.voltageLabel = '\\ ?\\ \\si{\\volt}'
    }
  }

  // Not the best way to handle intensities, but I'm kinda tired
  let intensitiesData = 'avec : '
  if (intensities.length !== 0) {
    const intensity = { id: 0, pos: 'left', value: 0, x: 0, y: 0, angle: 0, coord: { a: '', b: '' } }
    for (let i = 0; i < intensities.length; i++) {
      if (intensities[i].pos === 'left') {
        intensity.coord.a = '(2.3,3)'
        intensity.coord.b = '(3,3)'
      } else if (intensities[i].pos === 'bottom') {
        intensity.coord.a = '(3,3)'
        intensity.coord.b = '(3,2.3)'
      } else if (intensities[i].pos === 'right') {
        intensity.coord.a = '(3,3)'
        intensity.coord.b = '(3.8,3)'
      }
      intensity.id = i + 1
      intensity.x = intensities[i].x
      intensity.y = intensities[i].y
      intensity.value = intensities[i].value

      latex += `
\\draw ` + intensity.coord.a + ` to[short, i=\\mbox{$I_${intensity.id}$}] ` + intensity.coord.b + ';'
      if (intensity.value === 0) {
        intensitiesData += `$I_${intensity.id} = ? \\si{\\milli\\ampere}$, `
      } else {
        intensitiesData += `$I_${intensity.id} = ${intensity.value} \\si{\\milli\\ampere}$, `
      }
    }
  }
  latex += '\n\\end{circuitikz}'

  if (intensities.length !== 0) {
    intensitiesData = intensitiesData.slice(0, -2)
    latex += '\n\n' + intensitiesData + '\n'
  }

  latex += '\n\\end{center}'
  return latex
}
