import Decimal from 'decimal.js'

/**
 * ecritureScientifique
 * Renvoie l'écriture scientifique d'un nombre
 * @param a Nombre à convertir
 * @param b Nombre de significatifs
 * @Example
 * ecritureScientifique(0.1234) renvoie 1.23\\times 10^{-1}
 * ecritureScientifique(1.23e-4) renvoie 1.23\\times 10^{-4}
 * ecritureScientifique(1.23e4) renvoie 1.23\\times 10^{4}
 * @author Thibault Giauffret
 */

export function ecritureScientifique (a: number | Decimal, b: number = 0, latex: boolean = true): string {
  if (typeof a === 'string') {
    window.notify('ecritureScientifique() n\'accepte pas les chaînes de caractère.', { argument: a })
    a = Number(a)
  }
  if (a instanceof Decimal) {
    const [mantisse, exposant] = a.toExponential().split('e')
    let newMantisse = parseFloat(mantisse)
    let newMantisseText = newMantisse.toString()
    if (b !== 0) {
      newMantisse = Math.round(newMantisse * 10 ** (b - 1)) / 10 ** (b - 1)
      newMantisseText = newMantisse.toPrecision(b)
    }
    if (latex) {
      let newExposant = exposant
      if (exposant[0] === '+') {
        newExposant = exposant.slice(1)
      }
      return `${newMantisseText}\\times 10^{${newExposant}}`
    } else {
      return `${newMantisse}e${exposant}`
    }
  } else if (typeof a === 'number') {
    const [mantisse, exposant] = a.toExponential().split('e')
    let newMantisse = parseFloat(mantisse)
    let newMantisseText = newMantisse.toString()
    if (b !== 0) {
      newMantisse = Math.round(newMantisse * 10 ** (b - 1)) / 10 ** (b - 1)
      newMantisseText = newMantisse.toPrecision(b)
    }

    if (latex) {
      // If the exponent is positive, remove the '+' sign
      let newExposant = exposant
      if (exposant[0] === '+') {
        newExposant = exposant.slice(1)
      }
      return `${newMantisseText}\\times 10^{${newExposant}}`
    } else {
      return `${newMantisse}e${exposant}`
    }
  } else {
    window.notify('ecritureScientifique : type de valeur non prise en compte', { a })
    return ''
  }
}

/**
 * chiffresSignificatifs
 * Renvoie une valeur avec le nombre de chiffres significatifs souhaité
 * @Example
 * chiffresSignificatifs(0.1234,2) devient 0.12
 * @author Thibault Giauffret
 */

export function chiffresSignificatifs (value: number, cs: number) {
  const scientificValue = ecritureScientifique(value, cs, false)
  const decimalValue = new Decimal(scientificValue)
  const fixedValue = decimalValue.toPrecision(cs)
  return fixedValue
}
