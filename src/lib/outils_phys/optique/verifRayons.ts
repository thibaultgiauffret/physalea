import Figure from 'apigeom'
import { type Coords, equationLine } from 'apigeom/src/elements/calculus/Coords'
import type Segment from 'apigeom/src/elements/lines/Segment'
import type Ray from 'apigeom/src/elements/lines/Ray'
import { round } from 'apigeom/src/lib/format'

/**
 *
 * @param param0 Vérifie si un segment est tracé
 * Attention ! Si A, B, C, D sont alignés dans cet ordre et [AD] est tracé, alors [BC] sera compté comme tracé
 * @returns
 */
export function verifSegment ({
  figure,
  point1,
  point2,
  color,
  tolerance = 0.1
}: {
  figure: Figure
  point1: Coords
  point2: Coords
  color?: string
  tolerance?:number
}): { isValid: boolean; message: string } {
  console.log('verifSegment')
  console.log({ point1, point2, color, tolerance })
  let segments = [...figure.elements.values()].filter((e) =>
    e.type.includes('Segment')
  ) as Segment[]
  if (segments.length === 0) {
    return {
      isValid: false,
      message: '❌'
    }
  }
  if (color !== undefined) {
    segments = segments.filter((e) => e.color === color)
  }
  if (segments.length === 0) {
    return {
      isValid: false,
      message: '❌'
    }
  }
  console.log({ point1, point2 })
  console.log(segments)
  const [a, b, c] = equationLine(point1, point2)
  console.log({ a, b, c })
  const segmentsCoincident = segments.filter((segment) => {
    const [a2, b2, c2] = segment.equation
    return areCoincident(a, b, c, a2, b2, c2, tolerance)
  })
  if (segmentsCoincident.length === 0) {
    return { isValid: false, message: '❌' }
  }
  for (const segment of segmentsCoincident) {
    if (
      (approximatelyEqual(segment.point1.x, point1.x, tolerance) &&
        approximatelyEqual(segment.point1.y, point1.y, tolerance) &&
        approximatelyEqual(segment.point2.x, point2.x, tolerance) &&
        approximatelyEqual(segment.point2.y, point2.y, tolerance)) ||
      (approximatelyEqual(segment.point2.x, point1.x, tolerance) &&
        approximatelyEqual(segment.point2.y, point1.y, tolerance) &&
        approximatelyEqual(segment.point1.x, point2.x, tolerance) &&
        approximatelyEqual(segment.point1.y, point2.y, tolerance))
    ) {
      return { isValid: true, message: '✅' }
    }
    // // Si [point1, point2] est inclus dans le segment
    // if (
    //   segment.point1.x <= point1.x &&
    //   point1.x <= segment.point2.x &&
    //   segment.point1.y <= point1.y &&
    //   point1.y <= segment.point2.y &&
    //   segment.point1.x <= point2.x &&
    //   point2.x <= segment.point2.x &&
    //   segment.point1.y <= point2.y &&
    //   point2.y <= segment.point2.y
    // ) {
    //   return { isValid: true, message: 'Un segment plus grand existe.' }
    // }
  }
  //   if (color !== undefined) {
  //     segments = [...figure.elements.values()].filter((e) =>
  //       e.type.includes('Segment')
  //     ) as Segment[]
  //     for (const segment of segmentsCoincident) {
  //       if (
  //         (approximatelyEqual(segment.point1.x, point1.x, tolerance) &&
  //           approximatelyEqual(segment.point1.y, point1.y, tolerance) &&
  //           approximatelyEqual(segment.point2.x, point2.x, tolerance) &&
  //           approximatelyEqual(segment.point2.y, point2.y, tolerance)) ||
  //         (approximatelyEqual(segment.point2.x, point1.x, tolerance) &&
  //           approximatelyEqual(segment.point2.y, point1.y, tolerance) &&
  //           approximatelyEqual(segment.point1.x, point2.x, tolerance) &&
  //           approximatelyEqual(segment.point1.y, point2.y, tolerance))
  //       ) {
  //         return {
  //           isValid: false,
  //           message: '❌'
  //         }
  //       }
  //     }
  //   }
  return {
    isValid: false,
    message: '❌'
  }
}

function approximatelyEqual (value1: number, value2: number, tolerance: number): boolean {
  console.log(`Comparing ${value1} and ${value2} with tolerance ${tolerance}`)
  return Math.abs(value1 - value2) <= tolerance
}

function areCoincident (
  a1: number,
  b1: number,
  c1: number,
  a2: number,
  b2: number,
  c2: number,
  tolerance = 0.1
): boolean {
  // Cas des droites verticales
  if (b1 === 0 && b2 === 0) {
    return approximatelyEqual(c1 / a1, c2 / a2, tolerance)
  }
  const slope1 = b1 !== 0 ? -a1 / b1 : Number.POSITIVE_INFINITY
  const slope2 = b2 !== 0 ? -a2 / b2 : Number.POSITIVE_INFINITY

  const intercept1 = b1 !== 0 ? -c1 / b1 : Number.NaN
  const intercept2 = b2 !== 0 ? -c2 / b2 : Number.NaN
  return (
    approximatelyEqual(slope1, slope2, tolerance) &&
    approximatelyEqual(intercept1, intercept2, tolerance)
    // &&
    // approximatelyEqual(c1, c2, tolerance * 3)
  )
}

export function verifDemiDroite ({
  figure,
  point1,
  point2,
  color,
  tolerance = 10
}: { figure: Figure; point1: Coords; point2: Coords; color?: string; tolerance?: number }): {
    isValid: boolean
    message: string
  } {
  let rays = [...figure.elements.values()].filter(
    (e) => e.type === 'Ray'
  ) as Ray[]
  const [a, b, c] = equationLine(point1, point2)
  if (rays.length === 0) {
    console.log('No ray')
    return {
      isValid: false,
      message: '❌'
    }
  }
  if (color !== undefined) {
    rays = rays.filter((e) => e.color === color)
    if (rays.length === 0) {
      return {
        isValid: false,
        message:
              '❌'
      }
    }
  }

  for (const ray of rays) {
    const [a2, b2, c2] = ray.equation
    if (
      areCoincident(a, b, c, a2, b2, c2, tolerance)
    ) {
      const directionRay = {
        x: round(ray.point2.x - ray.point1.x),
        y: round(ray.point2.y - ray.point1.y)
      }
      const directionCheck = {
        x: round(point2.x - point1.x),
        y: round(point2.y - point1.y)
      }
      console.log({ directionRay, directionCheck })
      if (
        directionRay.x * directionCheck.x >= 0 &&
          directionRay.y * directionCheck.y >= 0
      ) {
        return { isValid: true, message: '✅' }
      }
    }
  }

  // if (color !== undefined) {
  //   rays = [...figure.elements.values()].filter(
  //     (e) => e.type === 'Ray'
  //   ) as Ray[]
  //   for (const ray of rays) {
  //     const [a2, b2, c2] = ray.equation
  //     if (
  //       areCoincident(a, b, c, a2, b2, c2, tolerance) &&
  //           approximatelyEqual(ray.point1.x, point1.x, tolerance) &&
  //           approximatelyEqual(ray.point1.y, point1.y, tolerance)
  //     ) {
  //       const directionRay = {
  //         x: round(ray.point2.x - ray.point1.x),
  //         y: round(ray.point2.y - ray.point1.y)
  //       }
  //       const directionCheck = {
  //         x: round(point2.x - point1.x),
  //         y: round(point2.y - point1.y)
  //       }
  //       if (
  //         directionRay.x * directionCheck.x >= 0 &&
  //             directionRay.y * directionCheck.y >= 0
  //       ) {
  //         return {
  //           isValid: false,
  //           message:
  //                 '❌'
  //         }
  //       }
  //     }
  //   }
  // }

  return {
    isValid: false,
    message: '❌'
  }
}
