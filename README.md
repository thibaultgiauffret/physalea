# PhysALÉA

PhysALÉA est un générateur d'exercices de Physique-Chimie. Il est basé sur MathALÉA, un générateur d'exercices de mathématiques créé par CoopMaths (https://coopmaths.fr/alea).

**🚧 ATTENTION :** Ce projet est en cours de développement. Il n'est pas encore prêt pour une utilisation en production.


## 🖥 Tester en local

Les instructions détaillées pour l'installation de PhysALÉA sont disponibles sur le wiki de MathALÉA :
[https://forge.apps.education.fr/coopmaths/mathalea/-/wikis/home](https://forge.apps.education.fr/coopmaths/mathalea/-/wikis/home)

Il suffit de remplacer la commande :

 `git clone git@forge.apps.education.fr:coopmaths/mathalea.git` 
 
 du wiki par :

 `git clone https://forge.apps.education.fr/thibaultgiauffret/physalea.git`.

**TL;DR** (uniquement pour les plus expérimentés) :
- Installer NodeJS et NPM
- Installer pnpm : `npm install -g pnpm@8`
- Installer git : [https://git-scm.com/downloads](https://git-scm.com/downloads)
- Cloner le dépôt : `git clone https://forge.apps.education.fr/thibaultgiauffret/physalea.git`
- Installer les dépendances : `npm install`
- Lancer le serveur local de développement : `npm start`

## 🛠 Contribuer

PhysALÉA est un projet sous licence libre GNU AGPL v3, basé sur MathALÉA. Vous pouvez contribuer au projet en proposant des améliorations ou des corrections de bugs [juste là](https://forge.apps.education.fr/thibaultgiauffret/physalea/-/issues).

Si vous souhaitez contribuer au code, vous pouvez proposer des requêtes de fusion [ici](https://forge.apps.education.fr/thibaultgiauffret/physalea/-/merge_requests).

Pour faire vivre ce projet, il faut à la fois des contributeurs "codeurs" mais aussi des "éditeurs" et des "testeurs" ! Si vous souhaitez aider, n'hésitez pas à me contacter [ici](https://ensciences.fr/contact.php).

## ⚖ Licence

PhysALÉA est sous licence GNU AGPL v3. Vous pouvez consulter le texte complet de la licence [ici](https://www.gnu.org/licenses/agpl-3.0.html).